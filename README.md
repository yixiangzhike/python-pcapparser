# python-pcapparser

#### 介绍
用于解析tcpdump/dumpcap工具生成的pcap/pcapng文件的小工具。

#### 软件架构
LINUX


#### 安装教程

1.  下载源码: https://gitee.com/yixiangzhike/python-pcapparser
```
    git clone https://gitee.com/yixiangzhike/python-pcapparser.git
```
2.  安装python-pcapparser源码文件到python3库文件目录site-packages
```
    python3 setup.py build install
```
    或者
```
    python3 setup.py build install --single-version-externally-managed --root=/
```
3.  命令程序pcap-parse将安装到二进制PATH目录下

#### 使用说明

1.  获取帮助信息
```
    pcap-parse -h/--help
```
2.  默认解析所有数据报信息
```
    pcap-parse xx.pcap
```
3.  可以指定解析具体文件中某一行数据报信息
```
    pcap-parse -n 12 xx.pcap
```
4.  可以指定解析结果数据输出到文件
```
    pcap-parse -o outfile xx.pcap                                                                                         
```

#### 使用示例
```
$ pcap-parse tests/full.pcap -H
 -----------------------------------------------------------------------------------
 |   Pcap File Header   |  Link Type: 1-Ethernet, and Linux loopback devices 
 -----------------------------------------------------------------------------------
 | Magic    | Major | Minor | Thiszone | Sigfigs  | Snap Length | Link Type |      |
 -----------------------------------------------------------------------------------
 | D4C3B2A1 | 0002  | 0004  | 00000000 | 00000000 | 00040000    | 00000001  | HEX  |
 -----------------------------------------------------------------------------------
 | D4C3B2A1 | 2     | 4     | 0        | 0        | 262144      | 1         | DEC  |
 -----------------------------------------------------------------------------------

$ pcap-parse tests/full.pcap -n 2
 -----------------------------------------------------------------------------------
 |   Pcap File Header   |  Link Type: 1-Ethernet, and Linux loopback devices 
 -----------------------------------------------------------------------------------
 | Magic    | Major | Minor | Thiszone | Sigfigs  | Snap Length | Link Type |      |
 -----------------------------------------------------------------------------------
 | D4C3B2A1 | 0002  | 0004  | 00000000 | 00000000 | 00040000    | 00000001  | HEX  |
 -----------------------------------------------------------------------------------
 | D4C3B2A1 | 2     | 4     | 0        | 0        | 262144      | 1         | DEC  |
 -----------------------------------------------------------------------------------
 |>>>     Packet Header [1]     |
 --------------------------------------------------------------------
 | Timestamp(s)        | Timestamp(us) | Cap Len  | Length   |      |
 --------------------------------------------------------------------
 | 64D6DB5F            | 000266CE      | 0000002A | 0000002A | HEX  |
 --------------------------------------------------------------------
 | 2023-08-12 09:07:43 | 157390        | 42       | 42       | DEC  |
 --------------------------------------------------------------------
    |>>>    Ethernet Frame Header     |
    -------------------------------------------------------
    | D.MAC             | S.MAC             | Ether Type |
    -------------------------------------------------------
    | FF:FF:FF:FF:FF:FF | 28:73:8D:41:EE:D4 | 0806       |
    -------------------------------------------------------
        |>>>   ARP Frame Header   | ARP Request |
        ------------------------------------------------------------------------------------------------------
        |HWType|PtoType|HWSize|PtoSize|OP|S.MAC            |S.IP           |D.MAC            |D.IP           |
        ------------------------------------------------------------------------------------------------------
        |0001  |0800   |6     |4      |1 |28:73:8D:41:EE:D4|192.168.1.11   |FF:FF:FF:FF:FF:FF|192.168.1.31   |
        ------------------------------------------------------------------------------------------------------
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
