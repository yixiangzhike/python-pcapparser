#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""
from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.utils import format_ip2s,convert_hex2ascii

class VRRPMessage(object):
    """
    VRRPv2 Message format:
    -------------------------------------------------------------------------------------------------------
    |Version|Type|VRID |Priority|Count IP Addrs|Auth Type|Adver Int|Checksum|IPvx Address(es)|Auth Data1/2|
    -------------------------------------------------------------------------------------------------------
    |  4b   | 4b |1byte|  1byte |    1byte     |  1byte  |   1byte | 2bytes |    n*4/16bytes |    2*8bytes|
    -------------------------------------------------------------------------------------------------------

    VRRPv3 Message format:
    --------------------------------------------------------------------------------------------
    |Version|Type|VRID |Priority|Count IPvx Addr| Rsvd |Max Adver Int|Checksum|IPvx Address(es)|
    --------------------------------------------------------------------------------------------
    |  4b   | 4b |1byte|  1byte |    1byte      |  4b  |     12b     | 2bytes |    n*4/16bytes |
    --------------------------------------------------------------------------------------------

    Type:
         1      Advertisement

    Auth Type:
         0   No Authentication
         1   Simple Text Password
         2   IP Authentication Header

    Reference: https://www.rfc-editor.org/rfc/rfc3768.txt
               https://www.rfc-editor.org/rfc/rfc5798.txt
    """

    AUTH_TYPES = {0:'No Authentication', 1:'Simple Text Password', 2:'IP Authentication Header'}

    def __init__(self, uplayer_ip, version=0x00, vrrp_type=0x01, vrid=0x00, priority=0x00, addr_count=0x00, 
                 auth_type=0x00, adver_int=0x01, checksum=0x00, ip_addrs=[], auth_data1='', auth_data2=''):
        """
        @uplayer_ip   : 0x04-ipv4  0x06-ipv6
        @version
        @vrrp_type
        @vrid
        @priority
        @addr_count
        @auth_type
        @adver_int
        @checksum
        @ip_addrs
        """
        self.uplayer_ip = uplayer_ip
        self.version = version
        self.vrrp_type = vrrp_type
        self.vrid = vrid
        self.priority = priority
        self.addr_count = addr_count

        if self.version == 2:
            self.auth_type = auth_type
            self.auth_data1 = auth_data1
            self.auth_data2 = auth_data2
        elif self.version == 3:
            self.rsvd = auth_type

        self.adver_int = adver_int
        self.checksum = checksum
        self.ip_addrs = ip_addrs

    @classmethod
    def create(cls, data, ip_version):
        """
        Create VRRPMessage object
        @data   : the data of VRRP
        @ip_version 
        """
        if not data or not ip_version in [4,6]:
            raise ParamsError()

        if not isinstance(data, bytes):
            raise ParamsError()

        version = data[0] >> 4 & 0x0F
        addr_count = data[3]
        ip_addrs = []
        if version == 2:
            if addr_count:
                for idx in range(addr_count):
                    ip_addrs.append(format_ip2s(data[8+idx*4:8+(idx+1)*4], 4))
            length = len(data)
            auth_data1 = ''
            auth_data2 = ''
            if length > 8+4*addr_count:
                auth_data1 = data[8+4*addr_count:8+4*addr_count+8].hex()
                auth_data2 = data[8+4*addr_count+8:8+4*addr_count+16].hex()

            vrrp = cls(ip_version,
                       version=version,
                       vrrp_type=data[0] & 0x0F,
                       vrid=data[1],
                       priority=data[2],
                       addr_count=data[3],
                       auth_type=data[4],
                       adver_int=data[5],
                       checksum=int(data[6:8].hex(), 16),
                       ip_addrs=ip_addrs,
                       auth_data1=auth_data1,
                       auth_data2=auth_data2)
        elif version == 3:
            if addr_count:
                if ip_version == 4:
                   if addr_count:
                       for idx in range(addr_count):
                           ip_addrs.append(format_ip2s(data[8+idx*4:8+(idx+1)*4], 4))
                elif ip_version == 6:
                    for idx in range(addr_count):
                        ip_addrs.append(format_ip2s(data[8+idx*16:8+(idx+1)*16], 6))

            vrrp = cls(ip_version,
                       version=version,
                       vrrp_type=data[0] & 0x0F,
                       vrid=data[1],
                       priority=data[2],
                       addr_count=data[3],
                       auth_type=data[4]>>4 & 0x0F,
                       adver_int=int(data[4:6].hex(), 16) & 0x0FFF,
                       checksum=int(data[6:8].hex(), 16),
                       ip_addrs=ip_addrs)
        else:
            vrrp = None

        return vrrp

    def format_saddrs(self, indent=12):
        prefix = "%*s" % (indent, " ")
        idx = 0
        outmsg = ""
        for address in self.ip_addrs:
            if not idx:
                outmsg += prefix + "| IP Address |" + "\n"
                outmsg += prefix + "-"*(len(address)+4) + "\n"
            outmsg += prefix + "| %-*s |" % (len(address), address) + "\n"
            idx += 1
        if outmsg:
            outmsg += prefix + "-"*(len(address)+4) + "\n"
    
        return outmsg

    def format_outmsg(self, title="\033[32m VRRP Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        
        for attr in attrs.keys():
            if attr in ["auth_data1","auth_data2"]:
                if attrs[attr] and not int(attrs[attr], 16):
                    attrs[attr] = 0
                elif attrs[attr] and self.auth_type == 1:
                    attrs[attr] = convert_hex2ascii(attrs[attr].strip("00"))
            elif attr == "auth_type":
                attrs[attr] = "%d-%s" % (attrs[attr], self.AUTH_TYPES[attrs[attr]])
        
        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if attr in ["uplayer_ip", "ip_addrs"]:
                continue
            if attr in ["auth_data1","auth_data2"]:
                if not attrs[attr]:
                    continue
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))
        title_out = prefix + "|  %s  |\n" % title

        outmsg = title_out + \
                 prefix + "-"*num_sep + "\n" + \
                 prefix + head_out + "\n" + \
                 prefix + "-"*num_sep + "\n" + \
                 prefix + data_out + "\n" + \
                 prefix + "-"*num_sep + "\n"

        return outmsg + self.format_saddrs(indent=indent)

    def format_output(self, stdout, title="\033[32m VRRP Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        
