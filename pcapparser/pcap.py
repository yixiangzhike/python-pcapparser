#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.error import ParamsError
from pcapparser.utils import convert_timestamp
from pcapparser.packet import PacketHeader,PacketBody
from pcapparser.constants import LINK_TYPES,PCAP_FILE_HDR_LEN,PCAP_MAGIC_LITTLE, \
        PCAP_MAGIC_BIG,PACKET_HDR_LEN
from pcapparser.protocol import *
from pcapparser.app import MDNSMessage,NTPMessage,SSDPMessage,DHCPMessage,DHCPv6Message,DHCPv6RelayMessage


class PcapHeader(object):
    """
    A file.pcap can get from running the command "tcpdump -nvA -w file.pcap -c 4".

    The pcap file header data is coded by byteorder.
    If "little", calculate data from right to left.
    If "big", calculate data from lefto to right.

    You can get the byteorder from first 4bytes in the file.pcap, 
    and this only effect pcap file header coding and packet header coding. 
    byteorder="little" if 0xD4C3B2A1,  byteorder="big" if 0xA1B2C3D4.

    pcap file header format:
    ------------------------------------------------------------------------------------------------
    fields | Magic | Version Major | Version Minor | This Zone | SigFigs | SNAP Length | Link Type |
    ------------------------------------------------------------------------------------------------
    bytes  |   4   |       2       |       2       |      4    |    4    |      4      |     4     |
    ------------------------------------------------------------------------------------------------
    """
    
    def __init__(self, magic=0x00, major=0x00, minor=0x00, thiszone=0x00, 
                 sigfigs=0x00, snap_length=0x00, link_type=0x00, byteorder="little"):
        """
        @magic        : identify current file and byteorder for pcap file header and packet header.
        @major        : current file major version, eg: 0x0002
        @minor        : current file minor version, eg: 0x0004
        @thiszone     : local standard time, full 0 if GMT
        @sigfigs      : timestamp accuracy
        @snap_length  : the max length for saving packets
        @link_type    : Link type, 1-ethernet 7-ARCnet 9-ppp 114-LocalTalk
        @byteorder
        """
        self.magic = magic
        self.major = major
        self.minor = minor
        self.thiszone = thiszone
        self.sigfigs = sigfigs
        self.snap_length = snap_length
        self.link_type = link_type
        self.byteorder = byteorder

    @classmethod
    def get_byteorder(cls, data):
        """
        @data   : the data which include magic
        """
        if not data:
            raise ParamsError("Param data can not be None")
         
        if not isinstance(data, bytes):
            raise ParamsError("Param data error.")

        magic = int(data[:4].hex(), 16)

        if magic == PCAP_MAGIC_LITTLE:
            return "little"
        elif magic == PCAP_MAGIC_BIG:
            return "big"
        else:
            raise ParamsError()

    @classmethod
    def create(cls, data, size=PCAP_FILE_HDR_LEN, byteorder="little"):
        """
        @data         : the data of packet header
        @size         : the length of packet header
        @byteorder    : little or big
        """
        if not data or size < PCAP_FILE_HDR_LEN:
            raise ParamsError("Param data or size error")

        if not isinstance(data, bytes):
            raise ParamsError()

        pkthdr = cls(magic=int(data[:4].hex(), 16),
                     major=int.from_bytes(data[4:6], byteorder),
                     minor=int.from_bytes(data[6:8], byteorder),
                     thiszone=int.from_bytes(data[8:12], byteorder),
                     sigfigs=int.from_bytes(data[12:16], byteorder),
                     snap_length=int.from_bytes(data[16:20], byteorder),
                     link_type=int.from_bytes(data[20:24], byteorder),
                     byteorder=byteorder)

        return pkthdr
    
    def format_outmsg(self, title="\033[32m Pcap File Header \033[0m", indent=0):
        prefix = "%*s" % (indent, " ")
        fmt = "| %-8s | %-5s | %-5s | %-8s | %-8s | %-11s | %-9s | %-4s |"
        head_out = prefix + fmt % ("Magic", "Major", "Minor", "Thiszone", 
                                   "Sigfigs", "Snap Length", "Link Type", "    ")
        hex_data_out = prefix + fmt % ("%08X" % self.magic, 
                                       "%04X" % self.major, 
                                       "%04X" % self.minor, 
                                       "%08X" % self.thiszone, 
                                       "%08X" % self.sigfigs,
                                       "%08X" % self.snap_length,
                                       "%08X" % self.link_type,
                                       "HEX")

        data_out = prefix + fmt % ("%08X" % self.magic,
                                   str(self.major), 
                                   str(self.minor), 
                                   str(self.thiszone), 
                                   str(self.sigfigs),
                                   str(self.snap_length),
                                   str(self.link_type),
                                   "DEC")
        sep_num = 83
        outmsg = (prefix + "-" * sep_num) + "\n" \
              + prefix + "|  %s  |  Link Type: %d-%s " % (title, self.link_type, LINK_TYPES[self.link_type]) + "\n" \
              + (prefix + "-" * sep_num) + "\n" \
              + head_out + "\n" \
              + (prefix + "-" * sep_num) + "\n" \
              + hex_data_out + "\n" \
              + (prefix + "-" * sep_num) + "\n" \
              + data_out + "\n" \
              + (prefix + "-" * sep_num) + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m Pcap File Header \033[0m", indent=0):
        stdout.write(self.format_outmsg(title=title, indent=indent))



class PcapFile(object):
    """
    pcap file format:
    --------------------------------------------------------------------------------------------
    |  Pcap Header  | Packet Header 1 |  Packet Body 1 | Packet Header 2 | Packet Body 2 | ... |
    --------------------------------------------------------------------------------------------
    """
    
    def __init__(self, pcapfile=None, pcaphdr=None, packets=[(None, None)]):
        """
        @pcapfile     : The xxx.pcap file path
        @pcaphdr      : The object of PcapHeader
        @packets      : All packets in xxx.pcap, [(PacketHeader, Packet Data)]
        """
        self.pcapfile = pcapfile
        self.pcaphdr = pcaphdr
        self.packets = packets
    
    @classmethod
    def create(cls, filepath, filefmt="bin", mode="rb", byteorder="little"):
        """
        Create PcapFile object
        @filepath  : The xxx.pcap file path
        @filefmt   : The format of xxx.pcap, default binary
        @mode      : The mode for opening xxx.pcap
        @byteorder : Little or big
        """
        if not filepath:
            raise ParamsError("Param filepath can not be None")
        
        if filefmt != "bin":
            raise ParamsError("The format of file [%s] unsupport" % filepath)

        pfile = open(filepath, mode=mode)
        
        # read pcap file header data
        pcaphdr_data = pfile.read(PCAP_FILE_HDR_LEN)

        # create pcap file header object
        pcaphdr = PcapHeader.create(pcaphdr_data, size=PCAP_FILE_HDR_LEN, byteorder=byteorder)
        if not pcaphdr.link_type == 0x01:
            raise ParamsError("Support ethernet link layer only currently.")

        # read all packet dataes
        packets = []
        while True:
            # read packet header data
            pkthdr_data = pfile.read(PACKET_HDR_LEN)
            if not pkthdr_data:
                break
            pkthdr = PacketHeader.create(pkthdr_data, size=PACKET_HDR_LEN, byteorder=byteorder)

            # read packet data
            pktdata = pfile.read(pkthdr.length)
            if not pktdata:
                break

            packets.append((pkthdr, pktdata))

        pcapfile = cls(pcapfile=filepath, pcaphdr=pcaphdr, packets=packets)

        pfile.close()

        return pcapfile

    def format_output_pcaphdr(self, stdout, indent=0):
        """
        @stdout
        @indent
        """
        if self.pcaphdr:
            self.pcaphdr.format_output(stdout, indent=indent)
    
    def format_output(self, stdout, indent=0, **kwargs):
        """
        @stdout
        @indent
        @out_line: output the out_line, all if 0, last line if greater than len(self.packets)
        @out_data: whether to output app data or not
        @ether_type: output custom net-packet : ipv4/ipv6/arp/stp
        @smac: source mac
        @dmac: destination mac
        @begin_dt: begin datetime
        @end_dt: end datetime
        @udp_protocol
        @tcp_protocol
        @port
        @igmp_protocol
        @icmp4_protocol
        @icmp6_protocol
        """
        out_line=kwargs.get("line", 0)
        if not out_line:
            out_line = 0
        out_data=kwargs.get("appdata", False)

        ether_type=-1
        etype = kwargs.get("etype", "")
        if etype:
            if etype == "ipv4":
                ether_type = 0x0800
            elif etype == "ipv6":
                ether_type = 0x86dd
            elif etype == "arp":
                ether_type = 0x0806
            elif etype == "rarp":
                ether_type = 0x8035
            elif etype == "stp":
                ether_type = 0x00

        smac=kwargs.get("smac", "")
        dmac=kwargs.get("dmac","")
        begin_dt=kwargs.get("begin_dt","")
        end_dt=kwargs.get("end_dt", "")
        udp_protocol=kwargs.get("udp_packet", False)
        tcp_protocol=kwargs.get("tcp_packet", False)
        port=kwargs.get("port", 0)
        igmp_protocol=kwargs.get("igmp_packet", False)
        icmp4_protocol=kwargs.get("icmp4_packet", False)
        icmp6_protocol=kwargs.get("icmp6_packet", False)

        if out_line > len(self.packets):
            out_line = len(self.packets)
        index = 0
        for (pkthdr, pktdata) in self.packets:

            if begin_dt:
                b_seconds = convert_timestamp(begin_dt)
                if pkthdr.timestamp_s+pkthdr.timestamp_us/1000000 < b_seconds:
                    continue

            if end_dt:
                e_seconds = convert_timestamp(end_dt)
                if pkthdr.timestamp_s+pkthdr.timestamp_us/1000000 > e_seconds:
                    continue

            pktbody = PacketBody.create(pktdata)

            if smac:
                if not smac.lower() == pktbody.frame_hdr.format_macs()[0].lower():
                    continue

            if dmac:
                if not dmac.lower() == pktbody.frame_hdr.format_macs()[1].lower():
                    continue

            if ether_type >= 0x00:
                if not ether_type == pktbody.frame_hdr.etype:
                    continue

            if udp_protocol and not isinstance(pktbody.trans_hdr, UDPFrameHeader):
                continue

            if tcp_protocol and not isinstance(pktbody.trans_hdr, TCPFrameHeader):
                continue

            if port:
                if not isinstance(pktbody.trans_hdr, UDPFrameHeader) and not isinstance(pktbody.trans_hdr, TCPFrameHeader):
                    continue
                else:
                    if not port in [pktbody.trans_hdr.sport, pktbody.trans_hdr.dport]:
                        continue

            if igmp_protocol and not isinstance(pktbody.trans_hdr, IGMPMessage) \
                             and not isinstance(pktbody.trans_hdr, IGMPv3QueryMessage) \
                             and not isinstance(pktbody.trans_hdr, IGMPv3ReportMessage):
                continue

            if icmp4_protocol and not isinstance(pktbody.trans_hdr, ICMPv4DestUnreachableMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv4SourceQuenchMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv4TimeOutMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv4ParamProblemMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv4EchoMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv4RedirectMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv4TimeStampMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv4InformationMessage):
                continue

            if icmp6_protocol and not isinstance(pktbody.trans_hdr, ICMPv6DestUnreachableMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv6PacketTooBigMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv6TimeOutMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv6ParamProblemMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv6EchoMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv6RSMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv6RAMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv6NSMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv6NAMessage) \
                              and not isinstance(pktbody.trans_hdr, ICMPv6RedirectMessage):
                continue

            if out_line != 0 and index < (out_line-1):
                index += 1
                continue

            if pkthdr:
                pkthdr.format_output(stdout, title="\033[32m Packet Header [%d] \033[0m" % index, indent=indent)
            pktbody.format_output(stdout)

            if out_data:
                pktbody.format_app_data(stdout, indent=0)
                if isinstance(pktbody.trans_hdr, UDPFrameHeader):
                    if pktbody.trans_hdr.sport in [53,5353] or pktbody.trans_hdr.dport in [53,5353]:
                        # mdns/dns message data
                        mdnsmsg = MDNSMessage.create(pktbody.app_data)
                        mdnsmsg.format_output(stdout, indent=0)
                    elif pktbody.trans_hdr.sport == 123 or pktbody.trans_hdr.dport == 123:
                        # ntp message data
                        ntpmsg = NTPMessage.create(pktbody.app_data, pktbody.net_hdr.version)
                        ntpmsg.format_output(stdout, indent=0)
                    elif pktbody.trans_hdr.sport == 1900 or pktbody.trans_hdr.dport == 1900:
                        # ssdp message data
                        ssdpmsg = SSDPMessage(pktbody.app_data)
                        ssdpmsg.format_output(stdout, indent=0)
                    elif pktbody.trans_hdr.sport in [67,68] or pktbody.trans_hdr.dport in [67,68]:
                        # dhcp message data
                        dhcpmsg = DHCPMessage.create(pktbody.app_data)
                        dhcpmsg.format_output(stdout, indent=0)
                    elif pktbody.trans_hdr.sport in [546,547] or pktbody.trans_hdr.dport in [546,547]:
                        # dhcpv6 message data
                        if pktbody.app_data[0] in [12,13]:
                            dhcpv6msg = DHCPv6RelayMessage.create(pktbody.app_data)
                            title_message = "\033[32m DHCPv6 Relay Agent/Server Message \033[0m"
                        else:
                            dhcpv6msg = DHCPv6Message.create(pktbody.app_data)
                            title_message = "\033[32m DHCPv6 Message \033[0m"
                        dhcpv6msg.format_output(stdout, title=title_message, indent=0)
            index += 1

            if out_line != 0 and index >= (out_line-1):
                break

