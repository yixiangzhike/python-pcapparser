#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.constants import ICMPV6_MESSAGE_TYPES,ICMPV6_OPTIONS_TYPES
from pcapparser.utils import format_ip2s
from pcapparser.option import *


__all__ = [
        'get_message_type_and_code', 
        'ICMPv6DestUnreachableMessage', 
        'ICMPv6PacketTooBigMessage',
        'ICMPv6TimeOutMessage',
        'ICMPv6ParamProblemMessage',
        'ICMPv6EchoMessage',
        'ICMPv6RSMessage',
        'ICMPv6RAMessage',
        'ICMPv6NSMessage',
        'ICMPv6NAMessage',
        'ICMPv6RedirectMessage'
]

def get_message_type_and_code(icmpv6_data):
    """
    Get message type and code from icmpv6 message data
    @icmpv6_data : min size 2, include type and code
    """
    if not icmpv6_data or len(icmpv6_data) < 2:
        raise ParamsError()

    if not isinstance(icmpv6_data, bytes):
        raise ParamsError()

    return (icmpv6_data[0], icmpv6_data[1])

class ICMPv6BaseMessage(object):
    """
    ICMPv6 base message format:
    ------------------------------------------------
    fields | Type | Code | Checksum | Message Body |
    ------------------------------------------------
    bytes  |  1   |  1   |    2     |     n        |
    ------------------------------------------------

    Two type message:
    1. Error message    type: 0~127
    2. Information Message   type: 128~255

    Reference: https://www.rfc-editor.org/rfc/rfc4443.txt
    """

    EXCLUDE_VARS = ["message_body"]

    def __init__(self, mtype=0x00, code=0x00, checksum=0x00, message_body=b''):
        """
        @mtype : 0~127 Error Message, 128~255 Information Message
        @code         : An additional level of message granularity
        @checksum     : Checksum, udp header and data
        @message_body : Remaining fields in different messages
        """
        self.mtype = mtype
        self.code = code
        self.checksum = checksum
        self.message_body = message_body

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        pass

    def format_outmsg(self, title="\033[32m ICMPv6 Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "rf" and attrs[attr]:
                attrs[attr] = "%d-%s" % (attrs[attr], "Router")
            elif attr == "sf" and attrs[attr]:
                attrs[attr] = "%d-%s" % (attrs[attr], "Resp")
            elif attr == "of" and attrs[attr]:
                if isinstance(self, ICMPv6NAMessage):
                    attrs[attr] = "%d-%s" % (attrs[attr], "Override")

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if attr in self.EXCLUDE_VARS:
                continue
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))
        title_out = prefix + "|>>>  %s  | Desc: %d-%s\n" % (title, self.mtype, ICMPV6_MESSAGE_TYPES[str(self.mtype)+str(self.code)])
        if len(title_out) - indent > num_sep:
            num_sep = len(title_out) - indent

        return title_out + \
               prefix + "-"*num_sep + "\n" + \
               prefix + head_out + "\n" + \
               prefix + "-"*num_sep + "\n" + \
               prefix + data_out + "\n" + \
               prefix + "-"*num_sep + "\n"

    def format_output(self, stdout, title="\033[32m ICMPv6 Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))


class ICMPv6DestUnreachableMessage(ICMPv6BaseMessage):
    """
    ICMPv6 destination unreachable  message format:
    -------------------------------------------
    fields | Type | Code | Checksum | Unused  |
    -------------------------------------------
    bytes  |  1   |  1   |    2     |   4     |
    -------------------------------------------
    Type : 0x01
    Code :
        0  'No route to destination'
        1  'Communication with destination administratively prohibited'
        2  'Beyond scope of source address'
        3  'Address unreachable'
        4  'Port unreachable'
        5  'Source address failed ingress/egress policy'
        6  'Reject route to destination'

    Reference: https://www.rfc-editor.org/rfc/rfc4443.txt
    """
    def __init__(self, mtype=0x01, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.unused = int(self.message_body[:4].hex(), 16)

class ICMPv6PacketTooBigMessage(ICMPv6BaseMessage):
    """
    ICMPv6 Packet-Too-Big message format:
    -------------------------------------------------
    fields | Type | Code | Checksum | Next Hop MTU  |
    -------------------------------------------------
    bytes  |  1   |  1   |    2     |      4        |
    -------------------------------------------------
    Type : 0x02
    Code : 0x00

    Reference: https://www.rfc-editor.org/rfc/rfc4443.txt
    """
    def __init__(self, mtype=0x02, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            # MTU : The Maximum Transmission Unit of the next-hop link.
            self.next_hop_mtu = int(self.message_body[:4].hex(), 16)


class ICMPv6TimeOutMessage(ICMPv6BaseMessage):
    """
    ICMPv6 timeout message format:
    -------------------------------------------
    fields | Type | Code | Checksum | Unused  |
    -------------------------------------------
    bytes  |  1   |  1   |    2     |   4     |
    -------------------------------------------
    Type : 0x03
    Code :
        0   'Hop limit timeout in transit'
        1   'Fragment reassembly timeout'

    Reference: https://www.rfc-editor.org/rfc/rfc4443.txt
    """
    def __init__(self, mtype=0x03, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.unused = int(self.message_body[:4].hex(), 16)


class ICMPv6ParamProblemMessage(ICMPv6BaseMessage):
    """
    ICMPv6 parameter problem message format:
    -------------------------------------------
    fields | Type | Code | Checksum | Pointer |
    -------------------------------------------
    bytes  |  1   |  1   |    2     |   4     |
    -------------------------------------------
    Type : 0x04
    Code :
        0  'Erroneous header field encountered'
        1  'Unrecognized Next Header type encountered'
        2  'Unrecognized IPv6 option encountered'

    Reference: https://www.rfc-editor.org/rfc/rfc4443.txt
    """
    def __init__(self, mtype=0x04, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            # Pointer Identifies the octet offset within the 
            # invoking packet where the error was detected. 
            self.pointer = int(self.message_body[:4].hex(), 16)

class ICMPv6EchoMessage(ICMPv6BaseMessage):
    """
    ICMPv6 echo request or echo response message format:
    -------------------------------------------------------------------------
    fields | Type | Code | Checksum | Identifier | Sequence Number |  Data  |
    -------------------------------------------------------------------------
    bytes  |  1   |  1   |    2     |      2     |       2         |   n    |
    -------------------------------------------------------------------------
    Type : 
            0x80     Echo Request Message
            0x81     Echo Response Message

    Code :  0x00

    Reference: https://www.rfc-editor.org/rfc/rfc4443.txt
    """

    EXCLUDE_VARS = ["message_body", "data"]

    def __init__(self, mtype=0x80, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.identifier = int(self.message_body[:2].hex(), 16)
            self.seq_number = int(self.message_body[2:4].hex(), 16)
            self.data = self.message_body[4:].hex()

    def format_outmsg(self, title="\033[32m ICMPv6 Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if attr in self.EXCLUDE_VARS:
                continue
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))
        title_out = prefix + "|>>>  %s  | Desc: %s\n" % (title, ICMPV6_MESSAGE_TYPES[str(self.mtype)+str(self.code)])

        if len(title_out) - indent > num_sep:
            num_sep = len(title_out) - indent

        data_out += prefix + "-"*num_sep + "\n"
        data_out += prefix + "| Data |" + "\n"
        data_out += prefix + "-"*num_sep + "\n"

        length = len(self.data)
        n = (length // num_sep + 1) if length%num_sep else (length // num_sep)
        for idx in range(n):
            data_out += prefix + self.data[idx*num_sep:(idx+1)*num_sep] + "\n"

        return title_out + \
               prefix + "-"*num_sep + "\n" + \
               prefix + head_out + "\n" + \
               prefix + "-"*num_sep + "\n" + \
               prefix + data_out + \
               prefix + "-"*num_sep + "\n"

class ICMPv6NDPMessage(ICMPv6BaseMessage):
    """
    Base class for ICMPv6 NDP Message

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """
    def unpack_options(self, options_data):
        """
        Parse every option from options_data
        @options_data : options string include one or more option-TLV data
        """
        opts = []
        if not options_data:
            return opts

        if not isinstance(options_data, bytes):
            raise ParamsError()

        options = options_data
        options_length = len(options_data)
        pos = 0
        option = None
        while True:
            opt_type = get_option_type(options[:1])
            opt_length = get_option_length(options[:2])
            if opt_type in [1,2]:
                option = ICMPv6LinkLayerOption(opt_type=options[0], opt_length=options[1], 
                                               opt_body=options[2:opt_length])
            elif opt_type == 3:
                option = ICMPv6PrefixInforOption(opt_type=options[0], opt_length=options[1], 
                                               opt_body=options[2:opt_length])
            elif opt_type == 4:
                option = ICMPv6RedirectedHeaderOption(opt_type=options[0], opt_length=options[1], 
                                               opt_body=options[2:opt_length])
            elif opt_type == 5:
                option = ICMPv6MTUOption(opt_type=options[0], opt_length=options[1], 
                                               opt_body=options[2:opt_length])
            elif opt_type in [9, 10]:
                option = ICMPv6AddrListOption(opt_type=options[0], opt_length=options[1], 
                                               opt_body=options[2:opt_length])
            elif opt_type == 11:
                option = ICMPv6SENDCGAOption(opt_type=options[0], opt_length=options[1], 
                                               opt_body=options[2:opt_length])
            elif opt_type == 12:
                option = ICMPv6SENDRsaSignOption(opt_type=options[0], opt_length=options[1], 
                                               opt_body=options[2:opt_length])
            elif opt_type == 13:
                option = ICMPv6SENDTimestampOption(opt_type=options[0], opt_length=options[1], 
                                               opt_body=options[2:opt_length])
            elif opt_type == 14:
                option = ICMPv6SENDNonceOption(opt_type=options[0], opt_length=options[1], 
                                               opt_body=options[2:opt_length])
            elif opt_type == 25:
                option = ICMPv6RDNSSOption(opt_type=options[0], opt_length=options[1],
                                               opt_body=options[2:opt_length])
            else:
                raise ParamsError("Option type[%d] not supported currently." % opt_type)
            if option:
                option.parse_option_body()
                opts.append(option)

            pos += opt_length
            # end data
            if pos >= options_length:
                break

            options = options[opt_length:]

        return opts

    def format_output(self, stdout, title="\033[32m ICMPv6 Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        if self.options:
            count = 0
            for option in self.options:
                option.format_output(stdout, title="\033[32m ICMPv6 Option [%d] \033[0m" % count, indent=indent)
                count += 1

class ICMPv6RSMessage(ICMPv6NDPMessage):
    """
    ICMPv6 router solicitation message format:
    -----------------------------------------------------
    fields| Type | Code | Checksum | Reserved | Options |
    -----------------------------------------------------
    bytes |  1   |  1   |    2     |   4      |    n    |
    -----------------------------------------------------

    Fields:

    Type       0x85

    Code       0x00

    Checksum   The ICMP checksum.  See [ICMPv6].

    Reserved   This field is unused.  It MUST be initialized to
               zero by the sender and MUST be ignored by the
               receiver.

    Valid Options:
           Source link-layer address The link-layer address of the sender, if
           known.  MUST NOT be included if the Source Address
           is the unspecified address.  Otherwise, it SHOULD
           be included on link layers that have addresses.

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """

    EXCLUDE_VARS = ["message_body", "options"]

    def __init__(self, mtype=0x85, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        self.options = []
        if self.message_body:
            self.reserved = int(self.message_body[:4].hex(), 16)
            if len(self.message_body) > 4:
                self.options = self.unpack_options(self.message_body[4:])

class ICMPv6RAMessage(ICMPv6NDPMessage):
    """
    ICMPv6 router advertisement message format:
    ------------------------------------------------------------------------------------------------------------
    fields|Type|Code|Checksum|Cur Hop Limit|MF|OF|Reserved|Router Lifetime|Reachable Time|Retrans Timer|Options|
    ------------------------------------------------------------------------------------------------------------
    bytes | 1  | 1  |   2    |      1      |1b|1b|   6b   |      2        |      4       |       4     |   n   |
    ------------------------------------------------------------------------------------------------------------

    Fields:

    Type           0x86
    Code           0x00
    Checksum       The ICMP checksum.  See [ICMPv6].
    Cur Hop Limit  8-bit unsigned integer.  The default value that
                   should be placed in the Hop Count field of the IP
                   header for outgoing IP packets.  A value of zero
                   means unspecified (by this router).

    M              1-bit "Managed address configuration" flag.  When
                   set, it indicates that addresses are available via
                   Dynamic Host Configuration Protocol [DHCPv6].
                   If the M flag is set, the O flag is redundant and
                   can be ignored because DHCPv6 will return all
                   available configuration information.

    O              1-bit "Other configuration" flag.  When set, it
                   indicates that other configuration information is
                   available via DHCPv6.  Examples of such information
                   are DNS-related information or information on other
                   servers within the network.

    Note: If neither M nor O flags are set, this indicates that no
    information is available via DHCPv6.

    Reserved       A 6-bit unused field.  It MUST be initialized to
                   zero by the sender and MUST be ignored by the
                   receiver.

    Router Lifetime   16-bit unsigned integer.  The lifetime associated
                      with the default router in units of seconds.  The
                      field can contain values up to 65535 and receivers
                      should handle any value, while the sending rules in
                      Section 6 limit the lifetime to 9000 seconds.  A
                      Lifetime of 0 indicates that the router is not a
                      default router and SHOULD NOT appear on the default
                      router list.  The Router Lifetime applies only to
                      the router's usefulness as a default router; it
                      does not apply to information contained in other
                      message fields or options.  Options that need time
                      limits for their information include their own
                      lifetime fields.

    Reachable Time  32-bit unsigned integer.  The time, in
                    milliseconds, that a node assumes a neighbor is
                    reachable after having received a reachability
                    confirmation.  Used by the Neighbor Unreachability
                    Detection algorithm (see Section 7.3).  A value of
                    zero means unspecified (by this router).

    Retrans Timer  32-bit unsigned integer.  The time, in
                   milliseconds, between retransmitted Neighbor
                   Solicitation messages.  Used by address resolution
                   and the Neighbor Unreachability Detection algorithm
                   (see Sections 7.2 and 7.3).  A value of zero means
                   unspecified (by this router).

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """

    EXCLUDE_VARS = ["message_body", "options"]

    def __init__(self, mtype=0x86, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        self.options = []
        if self.message_body:
            self.cur_hop_limit = self.message_body[0]
            self.mf = self.message_body[1]  >> 7 & 0x01
            self.of = self.message_body[1]  >> 6 & 0x01
            self.reserved = self.message_body[1] & 0x3F
            self.router_lifetime = int(self.message_body[2:4].hex(), 16)
            self.reachable_time = int(self.message_body[4:8].hex(), 16)
            self.router_lifetime = int(self.message_body[8:12].hex(), 16)

            if len(self.message_body) > 12:
                self.options = self.unpack_options(self.message_body[12:])

class ICMPv6NSMessage(ICMPv6NDPMessage):
    """
    ICMPv6 neighbor solicitation message format:
    -----------------------------------------------------------------------
    fields| Type | Code | Checksum |  Reserved | Target Address | Options |
    -----------------------------------------------------------------------
    bytes |  1   |  1   |    2     |     4     |      16        |    n    |
    -----------------------------------------------------------------------

    Fields:

      Type           0x87
      Code           0x00

      Checksum       The ICMP checksum.  See [ICMPv6].

      Reserved       This field is unused.  It MUST be initialized to
                     zero by the sender and MUST be ignored by the
                     receiver.

      Target Address The IP address of the target of the solicitation.
                     It MUST NOT be a multicast address.

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """

    EXCLUDE_VARS = ["message_body", "options"]

    def __init__(self, mtype=0x87, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        self.options = []
        if self.message_body:
            self.reserved = int(self.message_body[:4].hex(), 16)
            self.target_address = format_ip2s(self.message_body[4:20], 0x06)

            if len(self.message_body) > 20:
                self.options = self.unpack_options(self.message_body[20:])

class ICMPv6NAMessage(ICMPv6NDPMessage):
    """
    ICMPv6 neighbor advertisement message format:
    ---------------------------------------------------------------------------------------------------------------
    | Type | Code | Checksum | Router flag | Solicited flag | Override flag | Reserved | Target Address | Options |
    ---------------------------------------------------------------------------------------------------------------
    |  1   |  1   |    2     |     1bit    |    1bit        |    1bit       |  29bits  |      16        |    n    |
    ---------------------------------------------------------------------------------------------------------------

    Type : 0x88
    Code : 0x00
     R              Router flag.  When set, the R-bit indicates that
                    the sender is a router.  The R-bit is used by
                    Neighbor Unreachability Detection to detect a
                    router that changes to a host.

     S              Solicited flag.  When set, the S-bit indicates that
                    the advertisement was sent in response to a
                    Neighbor Solicitation from the Destination address.
                    The S-bit is used as a reachability confirmation
                    for Neighbor Unreachability Detection.  It MUST NOT
                    be set in multicast advertisements or in
                    unsolicited unicast advertisements.

     O              Override flag.  When set, the O-bit indicates that
                    the advertisement should override an existing cache
                    entry and update the cached link-layer address.
                    When it is not set the advertisement will not
                    update a cached link-layer address though it will
                    update an existing Neighbor Cache entry for which
                    no link-layer address is known.  It SHOULD NOT be
                    set in solicited advertisements for anycast
                    addresses and in solicited proxy advertisements.
                    It SHOULD be set in other solicited advertisements
                    and in unsolicited advertisements.

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """

    EXCLUDE_VARS = ["message_body", "options"]

    def __init__(self, mtype=0x88, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        self.options = []
        if self.message_body:
            self.rf = self.message_body[0] >> 7 & 0x01
            self.sf = self.message_body[0] >> 6 & 0x01
            self.of = self.message_body[0] >> 5 & 0x01
            self.reserved = int(self.message_body[:4].hex(), 16) & 0x1FFFFFFF
            self.target_address = format_ip2s(self.message_body[4:20], 0x06)

            if len(self.message_body) > 20:
                self.options = self.unpack_options(self.message_body[20:])

class ICMPv6RedirectMessage(ICMPv6NDPMessage):
    """
    ICMPv6 redirect message format:
    --------------------------------------------------------------------------------------
    fields| Type | Code | Checksum |  Reserved | Target Address | Dest Address | Options |
    --------------------------------------------------------------------------------------
    bytes |  1   |  1   |    2     |     4     |      16        |      16      |    n    |
    --------------------------------------------------------------------------------------

    Fields:
      Type           0x89
      Code           0x00
      Checksum       The ICMP checksum.  See [ICMPv6].
      Reserved       This field is unused.  It MUST be initialized to
                     zero by the sender and MUST be ignored by the
                     receiver.
      Target Address An IP address that is a better first hop to use for
                     the ICMP Destination Address.  When the target is
                     the actual endpoint of communication, i.e., the
                     destination is a neighbor, the Target Address field
                     MUST contain the same value as the ICMP Destination
                     Address field.  Otherwise, the target is a better
                     first-hop router and the Target Address MUST be the
                     router's link-local address so that hosts can
                     uniquely identify routers.
      
      Destination Address   The IP address of the destination that is
                            redirected to the target.

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """

    EXCLUDE_VARS = ["message_body", "options"]

    def __init__(self, mtype=0x89, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        self.options = []
        if self.message_body:
            self.reserved = int(self.message_body[:4].hex(), 16)
            self.target_address = format_ip2s(self.message_body[4:20], 0x06)
            self.dest_address = format_ip2s(self.message_body[20:36], 0x06)

            if len(self.message_body) > 36:
                self.options = self.unpack_options(self.message_body[36:])

class ICMPv6INDMessage(ICMPv6NDPMessage):
    """
    ICMPv6 Inverse Neighbor Discovery Solicitation/Advertisement message.
    Format:
    -----------------------------------------------------
    fields| Type | Code | Checksum | Reserved | Options |
    -----------------------------------------------------
    bytes |  1   |  1   |    2     |   4      |    n    |
    -----------------------------------------------------

    Fields:

    Type       0x8d   Inverse Neighbor Discovery Solicitation
               0x8e   Inverse Neighbor Discovery Advertisement

    Code       0x00

    Reference: https://www.rfc-editor.org/rfc/rfc3122.txt
    """

    EXCLUDE_VARS = ["message_body", "options"]

    def __init__(self, mtype=0x8d, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        self.options = []
        if self.message_body:
            self.reserved = int(self.message_body[:4].hex(), 16)
            if len(self.message_body) > 4:
                self.options = self.unpack_options(self.message_body[4:])

#------------------------------------------------------------------------
if __name__ == '__main__':
    pass
