#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""
from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.constants import MLD_BASE_LEN,ICMPV6_MESSAGE_TYPES,IGMP_GROUP_RECORD_TYPES
from pcapparser.utils import format_ip2s

__all__=['get_mld_type', 'MLDv2QRDMessage', 'MLDv2ReportMessage']

def get_mld_type(mld_data):
    """
    Get MLD message type from the data
    """
    if not mld_data:
        raise ParamsError()

    if not isinstance(mld_data, bytes):
        raise paramsError()

    return mld_data[0]


class MLDv1Message(object):
    """
    MLDv1 Message format:
    ------------------------------------------------------------------------------------------
    fields | Type  | Code | Checksum | Maximum Response Delay | Reserved | Multicast Address |
    ------------------------------------------------------------------------------------------
    bytes  |   1   |  1   |    2     |           2            |    2     |       16          |
    ------------------------------------------------------------------------------------------

    Type
    There are three types of MLD messages:

     Multicast Listener Query (Type = decimal 130)

     There are two subtypes of Multicast Listener Query messages:

     - General Query, used to learn which multicast addresses have
     listeners on an attached link.
     - Multicast-Address-Specific Query, used to learn if a
     particular multicast address has any listeners on an attached
     link.

     These two subtypes are differentiated by the contents of the
     Multicast Address field.

     Multicast Listener Report (Type = decimal 131)

     Multicast Listener Done (Type = decimal 132)

    Reference: https://www.rfc-editor.org/rfc/rfc2710.txt
    """

    EXCLUDE_VARS = ["version"]

    def __init__(self, mld_type=0x00, code=0x00, checksum=0x00, max_resp_delay=0x00, 
                 reserved=0x00, multicast_address=b''):
        """
        @mld_type   : 130/131/132
        @code       : 0
        @checksum
        @max_resp_delay
        @reserved
        @multicast_address
        """
        self.mld_type  = mld_type
        self.code = code
        self.checksum = checksum
        self.max_resp_delay = max_resp_delay
        self.reserved = reserved
        self.multicast_address = multicast_address

        if self.mld_type in [0x82,0x83,0x84]:
            self.version = 1
        else:
            self.version = 2

    @classmethod
    def create(cls, data, size=MLD_BASE_LEN):
        """
        Create MLDv1Message object
        @data : the data of MLD
        @size : the length of MLD data
        """
        if not data or size < MLD_BASE_LEN:
            raise ParamsError()

        if not isinstance(data, bytes):
            raise ParamsError()

        mld = cls(mld_type=data[0],
                  code=data[1],
                  checksum=int(data[2:4].hex(), 16),
                  max_resp_delay=int(data[4:6], 16),
                  reserved=int(data[6:8], 16),
                  multicast_address=data[8:24])

        return mld

    def parse_message_body(self):
        pass

    def format_outmsg(self, title="\033[32m MLD Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        
        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "multicast_address":
                attrs[attr] = format_ip2s(attrs[attr], 6)

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if attr in self.EXCLUDE_VARS:
                continue
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))
        title_out = prefix + "|>>>  %s  |\n" % title
        width = len(ICMPV6_MESSAGE_TYPES[str(self.mld_type)+str(self.code)])
        if width < len("Description"):
            width = len("Description")
        head_out = prefix + head_out + " %-*s |\n" % (width, "Description")
        data_out = prefix + data_out + " %-*s |\n" % (width, ICMPV6_MESSAGE_TYPES[str(self.mld_type)+str(self.code)])
        num_sep += width+3
        
        return title_out + prefix + "-"*num_sep + "\n" + head_out + \
                prefix + "-"*num_sep + "\n" + data_out + prefix + "-"*num_sep + "\n"

    def format_output(self, stdout, title="\033[32m MLD Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        

class MLDv2QRDMessage(MLDv1Message):
    """
    MLDv2 Query/Report/Done Message format:
    -----------------------------------------------------------------------------------------------------------
    |Type |Code |Checksum|Maximum Response Delay|Reserved|Multicast Address|Resv| S  |QRV |QQIC |Number|S.ADDR|
    -----------------------------------------------------------------------------------------------------------
    |1byte|1byte|2bytes  |   2bytes             |  2bytes|     16bytes     |4bit|1bit|3bit|1byte|2bytes| n*16 |
    -----------------------------------------------------------------------------------------------------------

    Type
           130  Multicast Listener Query 
           131  Version 1 Multicast Listener Report
           132  Version 1 Multicast Listener Done

    Reference: https://www.rfc-editor.org/rfc/rfc3810.txt
    """

    EXCLUDE_VARS = ["version", "ext_data", "saddrs"]

    def __init__(self, mld_type=0x00, code=0x00, checksum=0x00, max_resp_delay=0x00, 
                 reserved=0x00, multicast_address=b'', ext_data=b''):
        """
        @mld_type   : 130/131/132
        @code       : 0
        @checksum
        @max_resp_delay
        @reserved
        @multicast_address
        @ext_data
        """
        super().__init__(mld_type=mld_type, code=code,
                         checksum=checksum, max_resp_delay=max_resp_delay,
                         reserved=reserved, multicast_address=multicast_address)

        self.ext_data = ext_data
        self.saddrs = []

        if self.ext_data:
            self.version = 2

    def parse_extension_data(self):
        """
        Parse fields from extension data.
        """
        if not self.ext_data:
            raise ParamsError()

        if not isinstance(self.ext_data, bytes):
            raise ParamsError()

        self.resv = int(self.ext_data[:1].hex(), 16) >> 4 & 0x0F
        self.s = int(self.ext_data[:1].hex(), 16) >> 3 & 0x01
        self.qrv = int(self.ext_data[:1].hex(), 16) & 0x07
        self.qqic = int(self.ext_data[1:2].hex(), 16)
        self.number = int(self.ext_data[2:4].hex(), 16)

        for idx in range(self.number):
            self.saddrs.append(self.ext_data[4+idx*16:4+(idx+1)*16])

    def parse_message_body(self):
        if self.ext_data:
            self.parse_extension_data()

    @classmethod
    def create(cls, mld_data):
        pass

    def format_outmsg(self, title="\033[32m MLD Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        outmsg = super().format_outmsg(title=title, indent=indent)
        return outmsg + self.format_saddrs(indent=indent+4)

    def format_saddrs(self, indent=16):
        prefix = "%*s" % (indent, " ")
        idx = 0
        outmsg = ""
        for address in self.saddrs:
            if not idx:
                outmsg += prefix + "| \033[32m Source Address \033[0m |" + "\n"
                outmsg += prefix + "-"*42 + "\n"
            outmsg += prefix + "| " + format_ip2s(address, 6) + " |\n"
            idx += 1
        if outmsg:
            outmsg += prefix + "-"*42 + "\n"

        return outmsg


class MultiAddrRecord(object):
    """
    Multicast Address Record in MLDv2 Report Message.
    format:
    -------------------------------------------------------------------------------------------
    | Record Type | Aux DataLen | Num S.Addr | MultiAddr | S.Addr1 | S.Addr2 | ... | Aux Data |
    -------------------------------------------------------------------------------------------
    |   1byte     |  1byte      |  2bytes    |   16bytes | 16bytes | 16bytes | ... |    n     | 
    -------------------------------------------------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc3810.txt
    """

    EXCLUDE_VARS = ["saddr_data", "saddrs"]

    def __init__(self, record_type=0x00, aux_datalen=0x00, num_saddr=0x00,
                 multicast_address=b'', saddr_data=b'', aux_data=b''):
        """
        @record_type
        @aux_datalen
        @mum_saddr
        @multicast_address
        @saddr_data
        @aux_data
        """
        self.record_type = record_type
        self.aux_datalen = aux_datalen
        self.num_saddr = num_saddr
        self.multicast_address = multicast_address
        self.saddr_data = saddr_data
        self.aux_data = aux_data
        self.saddrs = []

    def parse_source_address(self):
        """
        Parse every source address from self.saddr_data
        """
        for count in range(self.num_saddr):
            self.saddrs.append(self.saddr_data[16*count:16*(count+1)])

    @classmethod
    def get_multicast_address_record_length(cls, mar_data):
        """
        Get multicast address record length from MLD data.
        @mar_data
        """
        if not mar_data or len(mar_data) < 16:
            raise ParamsError()

        if not isinstance(mar_data, bytes):
            raise ParamsError()

        aux_data_len = mar_data[1]
        saddr_data_len = 16 * int(mar_data[2:4].hex(), 16)

        return (1+1+2+16+saddr_data_len+aux_data_len)

    @classmethod
    def create(cls, mar_data, size=0x00):
        """
        Create MultiAddrRecord object.
        @mar_data
        @size
        """
        if not mar_data:
            raise ParamsError()
        if not isinstance(mar_data, bytes):
            raise ParamsError()

        aux_data_len = mar_data[1]
        saddr_data_len = 16 * int(mar_data[2:4].hex(), 16)

        multicast_address_record = cls(record_type=mar_data[0],
                                aux_datalen=aux_data_len,
                                num_saddr=int(mar_data[2:4].hex(), 16),
                                multicast_address=mar_data[4:20],
                                saddr_data=mar_data[20:20+saddr_data_len],
                                aux_data=mar_data[20+saddr_data_len:20+saddr_data_len+aux_data_len])

        return multicast_address_record

    def format_outmsg(self, title="\033[32m Multicast Address Record \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)

        for attr in attrs.keys():
            if attr == "multicast_address":
                attrs[attr] = format_ip2s(attrs[attr], 6)
            elif attr == "aux_data":
                if attrs[attr]:
                    attrs[attr] = attrs[attr].hex()
                else:
                    attrs[attr] = ""
            elif attr == "record_type":
                attrs[attr] = "%d-%s" % (attrs[attr], IGMP_GROUP_RECORD_TYPES[attrs[attr]])

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if attr in self.EXCLUDE_VARS:
                continue
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))
        title_out = prefix + "|  %s  |\n" % title

        outmsg = title_out + \
                prefix + "-"*num_sep + "\n" + \
                prefix + head_out + "\n" + \
                prefix + "-"*num_sep + "\n" + \
                prefix + data_out + "\n" + \
                prefix + "-"*num_sep + "\n"

        return outmsg + self.format_saddrs(indent=indent+4)


    def format_saddrs(self, indent=16):
        prefix = "%*s" % (indent, " ")
        idx = 0
        outmsg = ""
        for address in self.saddrs:
            if not idx:
                outmsg += prefix + "| \033[32m Source Address \033[0m |" + "\n"
                outmsg += prefix + "-"*40 + "\n"
            outmsg += prefix + "| " + format_ip2s(address, 4) + " |\n"
            idx += 1

        if outmsg:
            outmsg += prefix + "-"*40 + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m Multicast Address Record \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))


class MLDv2ReportMessage(object):
    """
    MLDv2 Report Message format:
    ------------------------------------------------------------------------
    | Type  | Reserved | Checksum | Reserved | Num MAR | MAR1 | MAR2 | ... |
    ------------------------------------------------------------------------
    | 1byte | 1byte    |  2bytes  |  2bytes  |  2byte  | n    |  n   | ... |
    -------------------------------------------------------------------------

    Type    143

    Reference: https://www.rfc-editor.org/rfc/rfc3810.txt
    """

    EXCLUDE_VARS = ["version", "mar_data", "multicast_address_records"]

    def __init__(self, mld_type=0x8f, reserved1=0x00, checksum=0x00, reserved2=0x00, mar_num=0x00, mar_data=b''):
        """
        @mld_type
        @reserved1
        @checksum
        @reserved2
        @mar_num
        @mar_data
        """
        self.version = 2
        self.mld_type = mld_type
        self.reserved1 = reserved1
        self.checksum = checksum
        self.reserved2 = reserved2
        self.mar_num = mar_num
        self.mar_data = mar_data
        self.multicast_address_records = []

    def parse_multicast_address_record_data(self):
        """
        Parse multicast address record
        """
        if not self.mar_data:
            raise ParamsError()

        if not isinstance(self.mar_data, bytes):
            raise ParamsError()

        data = self.mar_data
        while True:
            if not data:
                break
            mar_length = MultiAddrRecord.get_multicast_address_record_length(data)
            mar = MultiAddrRecord.create(data[:mar_length], size=mar_length)
            mar.parse_source_address()
            self.multicast_address_records.append(mar)
            data = data[mar_length:]

    def parse_message_body(self):
        if self.mar_data:
            self.parse_multicast_address_record_data()

    @classmethod
    def create(cls, mld_data):
        pass

    def format_outmsg(self, title="\033[32m MLD Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        
        attrs = deepcopy(self.__dict__)
        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if attr in self.EXCLUDE_VARS:
                continue
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))
        title_out = prefix + "|>>>  %s  |\n" % title
        width = len(ICMPV6_MESSAGE_TYPES[str(self.mld_type)+"0"])
        if width < len("Description"):
            width = len("Description")
        head_out = prefix + head_out + " %-*s |\n" % (width, "Description")
        data_out = prefix + data_out + " %-*s |\n" % (width, ICMPV6_MESSAGE_TYPES[str(self.mld_type)+"0"])
        num_sep += width+3
        
        return title_out + prefix + "-"*num_sep + "\n" + head_out + \
                prefix + "-"*num_sep + "\n" + data_out + prefix + "-"*num_sep + "\n"

    def format_output(self, stdout, title="\033[32m MLD Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        if self.multicast_address_records:
            count = 0
            for mar in self.multicast_address_records:
                mar.format_output(stdout, title="\033[32m Multicast Address Record [%d] \033[0m" % count, indent=indent)
                count += 1

#------------------------------------------------------------------------
if __name__ == '__main__':
    pass
