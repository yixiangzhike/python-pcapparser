#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

import re
from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.constants import IPV4_OPTIONS_TYPES,IPV6_OPTIONS_TYPES,TCP_OPTIONS_TYPES,ICMPV6_OPTIONS_TYPES
from pcapparser.utils import conv_format_mac,format_ip2s,format_datetime

def get_option_type(option_data):
    """
    Get option type from option data
    @option_data : option data
    """
    if not option_data:
        raise ParamsError()

    if not isinstance(option_data, bytes):
        raise ParamsError()

    return option_data[0]

def get_option_length(option_data):
    """
    Get option length from option data
    @option_data : option data, include type and length at least
    """
    if not option_data or len(option_data) < 2:
        raise ParamsError()

    if not isinstance(option_data, bytes):
        raise ParamsError()

    return 8 * option_data[1]

def get_public_key_length(data):
    """
    Get the length from data which inclue public key (ASN.1 DER-encoded)
    @data: the data which include public key. (ASN.1 DER-encoded)
    @return:
           length, include full TLV
    """
    if not data:
        raise ParamsError()

    tag = data[0]
    length_first = data[1] & 0xFF
    if length_first < 0x80:
        # length value : b7~b1
        length = length_first + 1 + 1
    elif length_first == 0x80:
        # DER-encoded not support
        raise ParamsError()
    else:
        # b7~b1 : bytes of length value
        length_size = length_first & 0x7F
        length = 1 + 1 + length_size + int(data[2:1+1+length_size].hex(), 16)

    return length

class Option(object):
    """
    Option format(TLV):
    ------------------------------------------------
    | Option Type  | Option  Length | Options Data |
    ------------------------------------------------
    |   1byte      |       1byte    |       n      |
    ------------------------------------------------
    """
    def __init__(self, opt_type=0x00, opt_length=0x00, opt_value=b'', protocol="ipv4"):
        """
        @opt_type
        @opt_length : the length of option, inclue type,length,data
        @opt_value
        @protocol   : support ipv4 ipv6 tcp
        """
        self.opt_type = opt_type
        self.opt_length = opt_length
        self.opt_value = opt_value
        self.protocol = protocol

    @classmethod
    def get_option_length(cls, data, size=2, protocol="ipv4"):
        """
        @data: include type and length at least
        @size
        @protocol
        """
        if not isinstance(data, bytes):
            raise ParamsError()

        opt_type = data[0]
        if protocol in ["ipv4", "tcp"]:
            if opt_type in [0,1]:
                return 1
            else:
                return data[1]
        else:
            if opt_type == 0:
                return 1
            else:
                # The length in ipv6 extension header option
                # only include the option data field.
                # Add the option type byte and length byte
                # for return value
                return data[1] + 2

    @classmethod
    def create(cls, data, size=2, protocol="ipv4"):
        """
        Create Option object
        @data : full option data, inclue type and length and value
        @size : the length of data 
        @protocol : support ipv4 ipv6 tcp
        """
        if not isinstance(data, bytes):
            raise ParamsError()

        opt = cls(opt_type=int(data[:1].hex(), 16),
                  opt_length=size,
                  opt_value=data[2:size] if size > 2 else None,
                  protocol=protocol)

        return opt

    def format_headmsg(self, title="\033[32m Options \033[0m", indent=12):
        """
        Format output head message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-30s | %-13s | %-20s |"
        head_out = prefix + fmt % ("Option Type/Code", "Option Length", "Option Data")
        sep_num = 73

        return prefix + "| %s |" % title + "\n" \
               + prefix + "-"*sep_num + "\n" \
               + head_out + "\n" \
               + prefix + "-"*sep_num + "\n"

    def format_bodymsg(self, indent=12):
        """
        Format output body message
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-30s | %-13s | %-20s |"
        if self.protocol == "ipv4":
            out_type = "%d-%s" % (self.opt_type, IPV4_OPTIONS_TYPES[self.opt_type])
        elif self.protocol == "ipv6":
            out_type = "%d-%s" % (self.opt_type, IPV6_OPTIONS_TYPES[self.opt_type])
        elif self.protocol == "tcp":
            out_type = "%d-%s" % (self.opt_type, TCP_OPTIONS_TYPES[self.opt_type])
        else:
            out_type = str(self.opt_type)

        data_out = prefix + fmt % (out_type, 
                                   str((self.opt_length-2)) if self.opt_length >= 2 else "0", 
                                   self.opt_value.hex() if self.opt_value else "")
        sep_num = 73

        return data_out + "\n" + prefix + "-"*sep_num + "\n"


    def format_outmsg(self, title="\033[32m Option \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """

        outmsg = self.format_headmsg(title=title, indent=indent) + \
                 self.format_bodymsg(indent=indent)

        return outmsg

    def format_output(self, stdout, title="\033[32m Option \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))


class ICMPv6Option(object):
    """
    ICMPv6 Option format(TLV):
    ------------------------------------------------
    | Option Type  | Option Length  | Options Data |
    ------------------------------------------------
    |   1byte      |       1byte    |       n      |
    ------------------------------------------------

    Fields:
      Type           8-bit identifier of the type of option.  The
                     options defined in this document are:
                        Option Name                             Type
                     Source Link-Layer Address                    1
                     Target Link-Layer Address                    2
                     Prefix Information                           3
                     Redirected Header                            4
                     MTU                                          5

      Length         8-bit unsigned integer.  The length of the option
                     (including the type and length fields) in units of
                     8 octets.  The value 0 is invalid.  Nodes MUST
                     silently discard an ND packet that contains an
                     option with length zero.

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """

    EXCLUDE_VARS = ["opt_body"]

    def __init__(self, opt_type=0x00, opt_length=0x00, opt_body=b''):
        """
        @opt_type   : identifier of the type of option
                      1  Source Link-Layer Address
                      2  Target Link-Layer Address
                      3  Prefix Information
                      4  Redirected Header
                      5  MTU 
        @opt_length : the length of option, inclue type,length,data  
                      unit: 8bytes
        @opt_body   : main fields
        """
        self.opt_type = opt_type
        self.opt_length = opt_length
        self.opt_body = opt_body

    @classmethod
    def get_option_length(cls, opt_data, size=2):
        """
        Get option length from option data
        @opt_data: include type and length at least
        @size: min 2bytes
        """
        if not opt_data or len(opt_data) < 2:
            raise ParamsError()

        if not isinstance(opt_data, bytes):
            raise ParamsError()

        if not opt_data[1]:
            raise ParamsError()

        return 8 * opt_data[1]

    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        pass

    def format_outmsg(self, title="\033[32m Option \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        attrs = deepcopy(self.__dict__)
        for attr in attrs:
            if attr == "opt_length":
                attrs[attr] = "%d (8B)" % attrs[attr]
            elif attr == "opt_type":
                attrs[attr] = "%d - %s" % (attrs[attr], ICMPV6_OPTIONS_TYPES[attrs[attr]])

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs:
            if attr in self.EXCLUDE_VARS:
                continue
            if len(attr) >= len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_',' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))

        #description  = "Desc: %d-%s" % (self.opt_type, ICMPV6_OPTIONS_TYPES[attrs["opt_type"]])
        #title_out = prefix + "|  %s | %s  |\n" % (title, description)
        title_out = prefix + "|  %s |\n" % title
        if num_sep < len(title_out) - indent:
            num_sep = len(title_out) - indent

        outmsg = title_out \
                +prefix + "-"*num_sep + "\n" \
                +prefix + head_out + "\n" \
                +prefix + "-"*num_sep + "\n" \
                +prefix + data_out + "\n" \
                +prefix + "-"*num_sep + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m Option \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))


class ICMPv6LinkLayerOption(ICMPv6Option):
    """
    ICMPv6 link-layer address option format(TLV):
    ------------------------------------------------------
    | Option Type  | Option Length  | Link-Layer Address |
    ------------------------------------------------------
    |   1byte      |       1byte    |         n          |
    ------------------------------------------------------

    Fields:
      Type
                     1 for Source Link-layer Address
                     2 for Target Link-layer Address

      Length         8-bit unsigned integer.  The length of the option
                     (including the type and length fields) in units of
                     8 octets.  The value 0 is invalid.  Nodes MUST
                     silently discard an ND packet that contains an
                     option with length zero.
      Link-Layer Address
                     The variable length link-layer address (MAC).

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """
    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        if self.opt_body:
            self.link_layer_address = conv_format_mac(self.opt_body[:8*self.opt_length-1])

class ICMPv6PrefixInforOption(ICMPv6Option):
    """
    ICMPv6 prefix information option format(TLV):
    ----------------------------------------------------------------------------------------------------
    |Type |Length|Prefix Length|LF  |AF  |Reserved1|Valid Lifetime|Preferred Lifetime|Reserved2|Prefix |
    ----------------------------------------------------------------------------------------------------
    |1byte|1byte |   1byte     |1bit|1bit|  6bits  |   4bytes     |    4bytes        |  4bytes |16bytes|
    ----------------------------------------------------------------------------------------------------

    Fields:
      Type           3
      Length         4  (unit: 8bytes)

      Prefix Length  8-bit unsigned integer.  The number of leading bits
                     in the Prefix that are valid.  The value ranges
                     from 0 to 128.  The prefix length field provides
                     necessary information for on-link determination
                     (when combined with the L flag in the prefix
                     information option).  It also assists with address
                     autoconfiguration as specified in [ADDRCONF], for
                     which there may be more restrictions on the prefix
                     length.

      L              1-bit on-link flag.  When set, indicates that this
                     prefix can be used for on-link determination.  When
                     not set the advertisement makes no statement about
                     on-link or off-link properties of the prefix.  In
                     other words, if the L flag is not set a host MUST
                     NOT conclude that an address derived from the
                     prefix is off-link.  That is, it MUST NOT update a
                     previous indication that the address is on-link.

      A              1-bit autonomous address-configuration flag.  When
                     set indicates that this prefix can be used for
                     stateless address configuration as specified in
                     [ADDRCONF].

      Reserved1      6-bit unused field.  It MUST be initialized to zero
                     by the sender and MUST be ignored by the receiver.

      Valid Lifetime
                     32-bit unsigned integer.  The length of time in
                     seconds (relative to the time the packet is sent)
                     that the prefix is valid for the purpose of on-link
                     determination.  A value of all one bits
                     (0xffffffff) represents infinity.  The Valid
                     Lifetime is also used by [ADDRCONF].

      Preferred Lifetime
                     32-bit unsigned integer.  The length of time in
                     seconds (relative to the time the packet is sent)
                     that addresses generated from the prefix via
                     stateless address autoconfiguration remain
                     preferred [ADDRCONF].  A value of all one bits
                     (0xffffffff) represents infinity.  See [ADDRCONF].
                     Note that the value of this field MUST NOT exceed
                     the Valid Lifetime field to avoid preferring
                     addresses that are no longer valid.

      Reserved2      This field is unused.  It MUST be initialized to
                     zero by the sender and MUST be ignored by the
                     receiver.

      Prefix         An IP address or a prefix of an IP address.  The
                     Prefix Length field contains the number of valid
                     leading bits in the prefix.  The bits in the prefix
                     after the prefix length are reserved and MUST be
                     initialized to zero by the sender and ignored by
                     the receiver.  A router SHOULD NOT send a prefix
                     option for the link-local prefix and a host SHOULD
                     ignore such a prefix option.


    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """
    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        if self.opt_body:
            self.prefix_len = self.opt_body[0]
            self.lf = self.opt_body[1] >> 7 & 0x01
            self.af = self.opt_body[1] >> 6 & 0x01
            self.resvd1 = self.opt_body[1] & 0x3F
            self.valid_lifetime = int(self.opt_body[2:6].hex(), 16)
            self.preferred_lifetime = int(self.opt_body[6:10].hex(), 16)
            self.resvd2 = int(self.opt_body[10:14].hex(), 16)
            if self.prefix_len == 128:
                self.prefix = format_ip2s(self.opt_body[14:30], 0x06)
            else:
                self.prefix = ":".join(re.findall(r'.{4}', self.opt_body[14:14+self.prefix_len//8].hex()))
                self.prefix += "::"

    def format_outmsg(self, title="\033[32m RrefixInfor Option \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        attrs = deepcopy(self.__dict__)
        for attr in attrs:
            if attr == "opt_length":
                attrs[attr] = "%d (8B)" % attrs[attr]

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs:
            if attr in self.EXCLUDE_VARS:
                continue
            if len(attr) >= len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+1
            head_out += "%-*s|" % (width, attr.replace('_',' ').title())
            data_out += "%-*s|" % (width, str(attrs[attr]))

        description  = "Desc: %d-%s" % (self.opt_type, ICMPV6_OPTIONS_TYPES[attrs["opt_type"]])
        title_out = prefix + "|  %s | %s  |\n" % (title, description)
        if num_sep < len(title_out) - indent:
            num_sep = len(title_out) - indent

        outmsg = title_out \
                +prefix + "-"*num_sep + "\n" \
                +prefix + head_out + "\n" \
                +prefix + "-"*num_sep + "\n" \
                +prefix + data_out + "\n" \
                +prefix + "-"*num_sep + "\n"

        return outmsg

class ICMPv6RedirectedHeaderOption(ICMPv6Option):
    """
    ICMPv6 redirected header option format(TLV):
    ------------------------------------------------------------
    | Type | Length | Reserved1 | Reserved2 | IP header + data |
    ------------------------------------------------------------
    |1byte |  1byte | 2byte     |  4bytes   |        n         |
    ------------------------------------------------------------

    Fields:

      Type           4

      Length         The length of the option in units of 8 octets.

      Reserved       These fields are unused.  They MUST be initialized
                     to zero by the sender and MUST be ignored by the
                     receiver.

      IP header + data
                     The original packet truncated to ensure that the
                     size of the redirect message does not exceed the
                     minimum MTU required to support IPv6 as specified
                     in [IPv6].

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """
    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        if self.opt_body:
            self.reserved1 = int(self.opt_body[0:2].hex(), 16)
            self.reserved2 = int(self.opt_body[2:6].hex(), 16)
            self.ip_header_data = self.opt_body[6:8*self.opt_length]


class ICMPv6MTUOption(ICMPv6Option):
    """
    ICMPv6 MTU option format(TLV):
    -----------------------------------------
    | Type | Length | Reserved |    MTU     |
    -----------------------------------------
    |1byte |  1byte | 2byte    |   4bytes   |
    -----------------------------------------

    Fields:

      Type           5
      Length         1 (unit: 8bytes)

      Reserved       This field is unused.  It MUST be initialized to
                     zero by the sender and MUST be ignored by the
                     receiver.

      MTU            32-bit unsigned integer.  The recommended MTU for
                     the link.

    Reference: https://www.rfc-editor.org/rfc/rfc4861.txt
    """
    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        if self.opt_body:
            self.reserved = int(self.opt_body[0:2].hex(), 16)
            self.MTU = int(self.opt_body[2:8*self.opt_length].hex(), 16)

class ICMPv6AddrListOption(ICMPv6Option):
    """
    ICMPv6 Source/Target address list option.
    Format:
    ------------------------------------------------------------------
    | Type | Length | Reserved | IPv6 Address1 | IPv6 Address2 | ... |
    ------------------------------------------------------------------
    |1byte |  1byte |  6bytes  |     16bytes   |     16bytes   | ... |
    ------------------------------------------------------------------

    Fields:
      Type          9 Source Address List
                   10 Target Address List
      
      Length       The length of the option (including the Type,
                   Length, and the Reserved fields) in units of 8
                   octets.  The minimum value for Length is 3, for one
                   IPv6 address.

      IPv6 Addresses  One or more IPv6 addresses of the interface.

    Reference: https://www.rfc-editor.org/rfc/rfc3122.txt
    """

    EXCLUDE_VARS = ["opt_body", "addrs"]

    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        self.addrs = []
        if self.opt_body:
            self.reserved = int(self.opt_body[0:6].hex(), 16)
            addr_data = self.opt_body[6:self.opt_length*8-2]
            count = (self.opt_length*8 - 8)//16
            if count == 1:
                self.address = format_ip2s(addr_data[:16], 0x06)
            else:
                for idx in range(count):
                    self.addrs.append(format_ip2s(addr_data[idx*16:(idx+1)*16], 0x06))

    def format_addrs_outmsg(self, title="IPv6 Address", indent=12):
        """
        Format IPv6 Address output message.
        """
        if self.addrs and len(self.addrs) > 1:
            prefix = "%*s" % (indent, " ")
            head_out = prefix + "| %s |\n" % title
            data_out = ""
            sep_num = 0
            for addr in self.addrs:
                data_out += prefix + "| %-40s |\n" % addr
            sep_num += 40+4

            return head_out + prefix + "-"*sep_num + "\n" + data_out + prefix+"-"*sep_num+"\n"
        else:
            return ""

    def format_output(self, stdout, title="\033[32m Address List Option \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        stdout.write(self.format_addrs_outmsg(title=title, indent=indent))

class ICMPv6SENDCGAOption(ICMPv6Option):
    """
    Cryptographically Generated Addresses (CGA)
    ICMPv6 SEcure Neighbor Discovery CGA option format(TLV):
    --------------------------------------------------------------------
    | Type | Length | Pad Length | Reserved | CGA Parameters | Padding |
    --------------------------------------------------------------------
    |1byte |  1byte |   1byte    |  1byte   |     n          | pad len |
    --------------------------------------------------------------------

    Fields:

      Type            11
      Length          The length of the option (including the Type, Length, Pad Length,
                      Reserved, CGA Parameters, and Padding fields) in units of 8
                      octets.

      Pad Length      The number of padding octets beyond the end of the CGA Parameters
                      field but within the length specified by the Length field.
                      Padding octets MUST be set to zero by senders and ignored by
                      receivers.
      CGA Parameters  A variable-length field containing the CGA Parameters data
                      structure described in RFC3972.

                      This specification requires that if both the CGA option and the
                      RSA Signature option are present, then the public key found from
                      the CGA Parameters field in the CGA option MUST be that referred
                      by the Key Hash field in the RSA Signature option.  Packets
                      received with two different keys MUST be silently discarded.  Note
                      that a future extension may provide a mechanism allowing the owner
                      of an address and the signer to be different parties.

    Reference: https://www.rfc-editor.org/rfc/rfc3971.txt
               https://www.rfc-editor.org/rfc/rfc3972.txt
    """

    EXCLUDE_VARS = ["opt_body", "cga_parameters"]

    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        if self.opt_body:
            self.pad_length = self.opt_body[0]
            self.reserved = self.opt_body[1]
            self.cga_parameters = self.opt_body[2:self.opt_length*8-4-self.pad_length].hex()
            if self.pad_length:
                self.padding = self.opt_body[self.opt_length*8-self.pad_length:self.opt_length*8-2].hex()

    def format_cga_parameters_outmsg(self, title="CGA Parameters", indent=12):
        """
        Parse CGA parameters.
        """
        if self.cga_parameters:
            modifier = self.cga_parameters[:16].hex()
            subnet_prefix = ":".join(re.findall(r'.{4}', self.cga_parameters[16:24].hex()))
            subnet_prefix += "::"
            collision_count = self.cga_parameters[24]
            key_length = get_public_key_length(self.cga_parameters[25:])
            public_key = self.cga_parameters[25:25+key_length].hex()
            extension_fields = self.cga_parameters[25+key_length:].hex()

            prefix = "%*s" % (indent, " ")
            sep_num = 93
            head_out = prefix + "| %s |\n" % title
            data_out = prefix + "| %-20s | %-64s |" % ("Modifier", modifier) + "\n" + \
                       prefix + "| %-20s | %-64s |" % ("Subnet Prefix", subnet_prefix) + "\n" + \
                       prefix + "| %-20s | %-64s |" % ("Collision Count", str(collision_count)) + "\n"
            if extension_fields:
                data_out += prefix + "| %-20s | %-64s |" % ("Extension Fields", extension_fields) + "\n"
            n = key_length//64 if not key_length%64 else key_length//64 + 1
            data_out += prefix + "| %-20s |" % "Public Key"
            for i in range(n):
                if not i:
                    data_out += "| %-64s |\n" % public_key[i*64:(i+1)*64]
                elif i == n-1:
                    data_out += prefix + "| %-20s | %-64s |\n" % (" ", public_key[i*64:key_length])
                else:
                    data_out += prefix + "| %-20s | %-64s |\n" % (" ", public_key[i*64:(i+1)*64])
            data_out += prefix + "-"*sep_num + "\n"

            return head_out + prefix + "-"*sep_num + "\n" + data_out
        else:
            return ""

    def format_output(self, stdout, title="\033[32m CGA Option \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        stdout.write(self.format_cga_parameters_outmsg(title="CGA Parameters", indent=indent))

class ICMPv6SENDTimestampOption(ICMPv6Option):
    """
    ICMPv6 SEcure Neighbor Discovery Timestamp option format(TLV):
    ----------------------------------------
    | Type | Length | Reserved | TimeStamp |
    ----------------------------------------
    |1byte | 1byte  | 6bytes   |  8bytes   |
    ----------------------------------------

    Fields:

      Type         13
      Length       The length of the option (including the Type, Length, Reserved,
                   and Timestamp fields) in units of 8 octets; i.e., 2.

      Reserved     A 48-bit field reserved for future use.  The value MUST be
                   initialized to zero by the sender and MUST be ignored by the
                   receiver.
      Timestamp    A 64-bit unsigned integer field containing a timestamp.  The value
                   indicates the number of seconds since January 1, 1970, 00:00 UTC,
                   by using a fixed point format.  In this format, the integer number
                   of seconds is contained in the first 48 bits of the field, and the
                   remaining 16 bits indicate the number of 1/64K fractions of a
                   second.

    Reference: https://www.rfc-editor.org/rfc/rfc3971.txt
    """

    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        if self.opt_body:
            self.reserved = int(self.opt_body[:6].hex(), 16)
            pre_seconds = int(self.opt_body[6:12].hex(), 16)
            sufix_seconds = int(self.opt_body[12:14].hex(), 16)
            self.timestamp = format_datetime(pre_seconds)

class ICMPv6SENDRsaSignOption(ICMPv6Option):
    """
    ICMPv6 SEcure Neighbor Discovery RSA Signature option format(TLV):
    ---------------------------------------------------------------------
    | Type | Length | Reserved | Key Hash | Digital Signature | Padding |
    ---------------------------------------------------------------------
    |1byte | 1byte  | 1byte    | 16bytes  |        n          |   n     |
    ---------------------------------------------------------------------

    Fields:

      Type         12
      Length       The length of the option (including the Type, Length, Reserved,
                   Key Hash, Digital Signature, and Padding fields) in units of 8
                   octets.

      Key Hash     A 128-bit field containing the most significant (leftmost) 128
                   bits of a SHA-1 hash of the public key used for constructing
                   the signature.  The SHA-1 hash is taken over the presentation used
                   in the Public Key field of the CGA Parameters data structure
                   carried in the CGA option.  Its purpose is to associate the
                   signature to a particular key known by the receiver.  Such a key
                   can either be stored in the certificate cache of the receiver or
                   be received in the CGA option in the same message.

      Digital Signature   A variable-length field containing a PKCS#1 v1.5 signature

    Reference: https://www.rfc-editor.org/rfc/rfc3971.txt
    """

    EXCLUDE_VARS = ["opt_body", "digital_signature"]

    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        if self.opt_body:
            self.reserved = int(self.opt_body[:2].hex(), 16)
            self.key_hash = self.opt_body[2:18].hex()
            self.digital_signature= self.opt_body[18:].hex()

    def format_digital_signature_outmsg(self, title="Digital Signature", indent=12):
        """
        Output digital signature.
        """
        if self.digital_signature:
            prefix = "%*s" % (indent, " ")
            sep_num = 104
            sig_length = len(self.digital_signature)
            head_out = prefix + "| %s |\n" % title
            n = sig_length//100 if not sig_length%100 else sig_length//100 + 1
            for i in range(n):
                if i == n-1:
                    data_out += prefix + "| %-100s |\n" % self.digital_signature[i*100:sig_length]
                else:
                    data_out += prefix + "| %-100s |\n" % self.digital_signature[i*100:(i+1)*100]
            data_out += prefix + "-"*sep_num + "\n"

            return head_out + prefix + "-"*sep_num + "\n" + data_out
        else:
            return ""

    def format_output(self, stdout, title="\033[32m RSA Signature Option \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        stdout.write(self.format_digital_signature_outmsg(title="Digital Signature", indent=indent))

class ICMPv6SENDNonceOption(ICMPv6Option):
    """
    ICMPv6 SEcure Neighbor Discovery nonce option format(TLV):
    ----------------------------
    | Type | Length | Nonce    |
    ----------------------------
    |1byte |  1byte | >=6bytes |
    ----------------------------

    Fields:

      Type        14

      Length      The length of the option (including the Type, Length, and Nonce
                  fields) in units of 8 octets.

      Nonce       A field containing a random number selected by the sender of the
                  solicitation message.  The length of the random number MUST be at
                  least 6 bytes.  The length of the random number MUST be selected
                  so that the length of the nonce option is a multiple of 8 octets.

    Reference: https://www.rfc-editor.org/rfc/rfc3971.txt
    """
    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        if self.opt_body:
            self.nonce = self.opt_body[:8*self.opt_length-2].hex()

class ICMPv6RDNSSOption(ICMPv6Option):
    """
    ICMPv6 recursive dns server option format(TLV):
    ---------------------------------------------------------------
    | Type | Length | Reserved | Lifetime | Address of IPv6 RDNSS |
    ---------------------------------------------------------------
    |1byte |  1byte |  2bytes  |   4bytes |       n*16bytes       |
    ---------------------------------------------------------------

    Fields:

      Type           8-bit identifier of the RDNSS option type as assigned
                     by the IANA: 25

       Length        8-bit unsigned integer.  The length of the option
                     (including the Type and Length fields) is in units of
                     8 octets.  The minimum value is 3 if one IPv6 address
                     is contained in the option.  Every additional RDNSS
                     address increases the length by 2.  The Length field
                     is used by the receiver to determine the number of
                     IPv6 addresses in the option.

       Lifetime      32-bit unsigned integer.  The maximum time, in
                     seconds (relative to the time the packet is sent),
                     over which this RDNSS address MAY be used for name
                     resolution.  Hosts MAY send a Router Solicitation to
                     ensure the RDNSS information is fresh before the
                     interval expires.  In order to provide fixed hosts
                     with stable DNS service and allow mobile hosts to
                     prefer local RDNSSes to remote RDNSSes, the value of
                     Lifetime should be at least as long as the Maximum RA
                     Interval (MaxRtrAdvInterval) in RFC 4861, and be at
                     most as long as two times MaxRtrAdvInterval; Lifetime
                     SHOULD be bounded as follows:  MaxRtrAdvInterval <=
                     Lifetime <= 2*MaxRtrAdvInterval.  A value of all one
                     bits (0xffffffff) represents infinity.  A value of
                     zero means that the RDNSS address MUST no longer be
                     used.

       Addresses of IPv6 Recursive DNS Servers
                     One or more 128-bit IPv6 addresses of the recursive
                     DNS servers.  The number of addresses is determined
                     by the Length field.  That is, the number of
                     addresses is equal to (Length - 1) / 2.

    Reference: https://www.rfc-editor.org/rfc/rfc5006.txt
    """

    EXCLUDE_VARS = ["opt_body", "rdnss_addrs"]

    def parse_option_body(self):
        """
        Set other fields from option body data
        """
        self.rdnss_addrs = []
        if self.opt_body:
            self.reserved = int(self.opt_body[0:2].hex(), 16)
            self.lifetime = int(self.opt_body[2:6].hex(),16)

            count = (self.opt_length - 1) // 2
            if count == 1:
                self.rdnss_addr = format_ip2s(self.opt_body[6:22], 0x06)
            else:
                for idx in range(count):
                    self.rdnss_addrs.append(format_ip2s(self.opt_body[6+idx*16:6+(idx+1)*16], 0x06))

    def format_output(self, stdout, title="\033[32m RDNSS Option \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        super().format_output(stdout, title=title, indent=indent)
        if self.rdnss_addrs and len(self.rdnss_addrs) > 1:
            prefix = "%*s" % (indent, " ")
            sep_out = prefix + "-"*44 + "\n"
            head_out = prefix + "| Address of IPv6 RDNSS (One or more) |\n"
            data_out = prefix
            for addr in self.rdnss_addrs:
                data_out += "| %-*s |\n" % (40, addr)

            stdout.write(head_out + sep_out + data_out + sep_out)

