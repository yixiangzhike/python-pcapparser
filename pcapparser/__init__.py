#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.file import DataFile
from pcapparser.error import DataFileError
from pcapparser.pcap import PcapFile

class Parser(object):
    """
    A parser for xx.pcap or xx.pcapng.
    """
    def __init__(self, path, **kwargs):
        """
        @path: the path of xx.pcap and xx.pcapng
        """
        self.path = path
        self.kwargs = kwargs

    def parse_file(self):
        """
        Parse xx.pcap and xx.pcapng
        """
        df = DataFile(self.path)
        # check file
        if not df.check_file():
            raise DataFileError()
        df.set_file_format()
        df.set_file_endian()
        df.debug_output(debug=False)
        if df.format == "pcap":
            pcap = PcapFile.create(self.path, byteorder=df.endian)
            pcap.format_output_pcaphdr(self.kwargs["stdout"])
            if not self.kwargs.get("header", False):
                out_file  = self.kwargs.get("stdout", None)
                del self.kwargs["stdout"]
                pcap.format_output(out_file, **self.kwargs)
        elif df.format == "pcapng":
            print("Unimplement for xx.pcapng temporarily.")
            pass
