#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.error import ParamsError
from pcapparser.constants import IPV6_EXT_HEADER_TYPES
from pcapparser.option import Option


__all__ = ["HBHExtHeader", "DestOptExtHeader"]

class BaseExtHeader(object):
    """
    Base extension header:
    ---------------------------------------------
    | next header | header length | header data |
    --------------------------------------------
    |   1byte     |     1byte     |      n      |
    ---------------------------------------------

    Header length : The length of extension header in 8bytes units, 
                    but not including the first 8bytes.
                    This means 0 is 8bytes, 1 is 2*8bytes, and so on.

    Extension Headers order:
    0      Hop-by-Hop Options header
    60     Destination Options header
    43     Routing header
    44     Fragment header
    51     Authentication header
    50     Encapsulating Security Payload header
    60     Destination Options header
    59     No next header

           upper-layer header:
           6  - TCP
           17 - UDP
           58 - ICMPv6

    Reference: https://www.rfc-editor.org/rfc/rfc2460.txt
    """
    def __init__(self, current_header=0x00, next_header=0x00, header_length=0x00, header_data=b''):
        """
        @current_header : Identifies the type of current extension header.
        @next_header    : Identifies the type of next extension header which follow current header
        @header_length  : The length of extension header in 8bytes units, not including the first 8bytes
        @header_data    : Current extension header data
        """
        self.current_header = current_header
        self.next_header = next_header
        self.header_length = header_length
        self.header_data = header_data

    def parse_extension_header_data(self):
        """
        Parse the fields data in self.header_data by self.current_header
        """
        pass

    def format_headmsg(self, title="\033[32m Extension Header \033[0m", indent=12):
        """
        Format output head message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-35s | %-11s | %-6s | %-30s |"
        head_out = prefix + fmt % ("Current Header", "Next Header", "Length", "Header Data")
        sep_num = 95

        return prefix + "| %s |" % title + "\n" \
               + prefix + "-"*sep_num + "\n" \
               + head_out + "\n" \
               + prefix + "-"*sep_num + "\n"

    def format_bodymsg(self, indent=12):
        """
        Format output body message
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-35s | %-11s | %-6s | %-30s |"
        cur_type = "%d-%s" % (self.current_header, IPV6_EXT_HEADER_TYPES[self.current_header])
        next_type = "%d-%s" % (self.next_header, IPV6_EXT_HEADER_TYPES[self.next_header])
        data_out = prefix + fmt % (cur_type, next_type, str(self.header_length)+"(8B)", self.header_data.hex() if self.header_data else "")
        sep_num = 95

        return data_out + "\n" + prefix + "-"*sep_num + "\n"


    def format_outmsg(self, title="\033[32m Extension Header \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """

        outmsg = self.format_headmsg(title=title, indent=indent) + \
                 self.format_bodymsg(indent=indent)

        return outmsg

    def format_output(self, stdout, title="\033[32m Extension Header \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))



class ExtOption(Option):
    """
    The option in IPv6 extension header.
    ----------------------------------------------------
    | Option Type/Code | Options Length | Options Data |
    ----------------------------------------------------
    |       1byte      |       1byte    |       n      |
    ----------------------------------------------------

    IPv6 Extension Header Options Type:
    b6-b7  : Action
             00 Skip this option
             01 Discard datagram, no more action
             10 Discard datagram and send ICMP message
             11 Discard datagram send ICMP message if not multicast
    b5     : Change
             0 Dose not change in transit
             1 May be changed in transit
    b0-b4  : Type
             00000 Pad1
             00001 PadN
             00010 Jumbo payload

    IPv6 Extension Header Options Data Len only the length of value
    Option Type :
           = 0 : Pad1 - single byte, no length and value
           = 1 : PadN - multi-bytes

    Reference: https://www.rfc-editor.org/rfc/rfc2460.txt
    """
    def __init__(self, opt_type=0x00, opt_length=0x00, opt_value=b'', protocol="ipv6"):
        """
        @opt_type
        @opt_length
        @opt_value
        @protocol
        """
        super().__init__(opt_type=opt_type, opt_length=opt_length, opt_value=opt_value, protocol=protocol)


class HBHExtHeader(BaseExtHeader):  
    """
    Hop-by-Hop extension header

    Reference: https://www.rfc-editor.org/rfc/rfc2460.txt
    """
    def __init__(self, current_header=0x00, next_header=0x00, header_length=0x00, header_data=b''):
        """
        @current_header : Identifies the type of current extension header.
        @next_header    : Identifies the type of next extension header which follow current header
        @header_length  : The length of extension header in 8bytes units, 
                          but not including the first 8bytes.
                          This means 0 is 8bytes, 1 is 2*8bytes, and so on.
        @header_data    : Current extension header data
        """
        super().__init__(current_header=current_header, next_header=next_header, 
                          header_length=header_length, header_data=header_data)
        self.ext_options = []

    def parse_extension_header_data(self):
        """
        Parse the fields data in self.header_data by self.current_header
        """
        if self.header_data:
            opts_bytes = self.header_data
            pos = 0
            while True:
                opt_length = ExtOption.get_option_length(opts_bytes[:2], protocol="ipv6")
                option = ExtOption.create(opts_bytes[:opt_length], size=opt_length, protocol="ipv6")
        
                self.ext_options.append(option)
        
                pos += opt_length
                # end of options list
                if ((self.header_length+1)*8-2) <= pos:
                    break
                opts_bytes = opts_bytes[opt_length:]

    def format_output(self, stdout, title="\033[32m Extension Header \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        if self.ext_options:
            count = 0
            for option in self.ext_options:
                if not count:
                    stdout.write(option.format_headmsg(indent=indent+2))
                stdout.write(option.format_bodymsg(indent=indent+2))
                count += 1


class DestOptExtHeader(HBHExtHeader):
    """
    Destination options header, the structure is same as HBHExtHeader

    Reference: https://www.rfc-editor.org/rfc/rfc2460.txt
    """
    pass
