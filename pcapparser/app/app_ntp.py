#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""
from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.utils import format_ip2s,convert_hex2ascii,format_datetime

__all__ = [
    'NTPMessage',
]

# https://www.rfc-editor.org/rfc/rfc5905
NTP_MODE = {
    0  :  'reserved',
    1  :  'symmetric active',
    2  :  'symmetric passive',
    3  :  'client',
    4  :  'server',
    5  :  'broadcast',
    6  :  'NTP control message',
    7  :  'reserved for private use',
}

NTP_LEAP = {
    0  :  'no warning',
    1  :  'last minute of the day has 61 seconds',
    2  :  'last minute of the day has 59 seconds',
    3  :  'unknown (clock unsynchronized)',
}

NTP_STRATUM = {
    0   :   'unspecified or invalid',
    1   :   'primary server',
    2   :   'secondary server (via NTP)',
    3   :   'secondary server (via NTP)',
    4   :   'secondary server (via NTP)',
    5   :   'secondary server (via NTP)',
    6   :   'secondary server (via NTP)',
    7   :   'secondary server (via NTP)',
    8   :   'secondary server (via NTP)',
    9   :   'secondary server (via NTP)',
    10  :   'secondary server (via NTP)',
    11  :   'secondary server (via NTP)',
    12  :   'secondary server (via NTP)',
    13  :   'secondary server (via NTP)',
    14  :   'secondary server (via NTP)',
    15  :   'secondary server (via NTP)',
    16  :   'unsynchronized',
    17  :   'reserved',
}

DIFF_1900_1970 = 0x83aa7e80

class NTPExtensionField(object):
    """
    Extension Field Format:
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |          Field Type           |            Length             |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    .                                                               .
    .                            Value                              .
    .                                                               .
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                       Padding (as needed)                     |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    Reference: https://www.rfc-editor.org/rfc/rfc5905.txt
               https://www.rfc-editor.org/rfc/rfc5906.txt
    """
    pass

class NTPMessage(object):
    """
    NTP Message Format:
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |LI | VN  |Mode |    Stratum     |     Poll      |  Precision   |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                         Root Delay                            |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                         Root Dispersion                       |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                          Reference ID                         |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    +                     Reference Timestamp (64)                  +
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    +                      Origin Timestamp (64)                    +
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    +                      Receive Timestamp (64)                   +
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    +                      Transmit Timestamp (64)                  +
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    .                                                               .
    .                    Extension Field 1 (variable)               .
    .                                                               .
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    .                                                               .
    .                    Extension Field 2 (variable)               .
    .                                                               .
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                          Key Identifier                       |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                                                               |
    |                            dgst (128)                         |
    |                                                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    Root Delay (rootdelay): Total round-trip delay to the reference
    clock, in NTP short format. (16 + 16)

    Root Dispersion (rootdisp): Total dispersion to the reference clock,
    in NTP short format. (16 + 16)

    Key Identifier (keyid): 32-bit unsigned integer used by the client
    and server to designate a secret 128-bit MD5 key.

    Message Digest (digest): 128-bit MD5 hash computed over the key
    followed by the NTP packet header and extensions fields (but not the
    Key Identifier or Message Digest fields).


    Extension Field Format:
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |          Field Type           |            Length             |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    .                                                               .
    .                            Value                              .
    .                                                               .
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                       Padding (as needed)                     |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    Reference: https://www.rfc-editor.org/rfc/rfc5905.txt
    """
    def __init__(self, leap_indicator=0x00, version=0x00, mode=0x00, stratum=0x00,
                 poll_exponent=0x00, precision_exponent=0x00, root_delay=0x00,
                 root_dispersion=0x00, reference_id="", reference_timestamp="",
                 origin_timestamp="", receive_timestamp="", transmit_timestamp="",
                 extension=[], key_id="", dgst="", ip_version=0x04):
        """
        @leap_indicator : 2bits
                          | 0     | no warning                             |
                          | 1     | last minute of the day has 61 seconds  |
                          | 2     | last minute of the day has 59 seconds  |
                          | 3     | unknown (clock unsynchronized)         |
        @version        : 3bits  currently 4
        @mode           : 3bits
                          | 0     | reserved                 |
                          | 1     | symmetric active         |
                          | 2     | symmetric passive        |
                          | 3     | client                   |
                          | 4     | server                   |
                          | 5     | broadcast                |
                          | 6     | NTP control message      |
                          | 7     | reserved for private use |
        @stratum        : 1byte
                          | 0      | unspecified or invalid                              |
                          | 1      | primary server (e.g., equipped with a GPS receiver) |
                          | 2-15   | secondary server (via NTP)                          |
                          | 16     | unsynchronized                                      |
                          | 17-255 | reserved                                            |
        @poll_exponent  : 1byte
        @precision_exponent : 1byte
        @root_delay     : 4bytes
        @root_dispersion: 4bytes
        @reference_ID   : 4bytes
                          Identifying the particular server or reference clock.
                          The interpretation depends on the value in the
                          stratum field.  For packet stratum 0 (unspecified or invalid), this
                          is a four-character ASCII [RFC1345] string, called the "kiss code",
                          used for debugging and monitoring purposes.  For stratum 1 (reference
                          clock), this is a four-octet, left-justified, zero-padded ASCII
                          string assigned to the reference clock.

                          Above stratum 1 (secondary servers and clients): this is the
                          reference identifier of the server and can be used to detect timing
                          loops.  If using the IPv4 address family, the identifier is the four-
                          octet IPv4 address.  If using the IPv6 address family, it is the
                          first four octets of the MD5 hash of the IPv6 address.  Note that,
                          when using the IPv6 address family on an NTPv4 server with a NTPv3
                          client, the Reference Identifier field appears to be a random value
                          and a timing loop might not be detected.
        @reference_timestamp : NTP timestamp format
        @origin_timestamp : NTP timestamp format
        @receive_timestamp : NTP timestamp format
        @transmit_timestamp : NTP timestamp format
        @extension         : list
        @key_id
        @dgst
        """
        self.leap_indicator = leap_indicator
        self.version = version
        self.mode = mode
        self.stratum = stratum
        self.poll_exponent = poll_exponent
        self.precision_exponent = precision_exponent
        self.root_delay = root_delay
        self.root_dispersion = root_dispersion
        self.reference_id = reference_id
        self.reference_timestamp = reference_timestamp
        self.origin_timestamp = origin_timestamp
        self.receive_timestamp = receive_timestamp
        self.transmit_timestamp = transmit_timestamp
        self.extension = extension
        self.key_id = key_id
        self.dgst = dgst
        self.ip_version = ip_version

    def format_outmsg(self, title="\033[32m NTP Message \033[0m", indent=0):
        """
        Format output message.
        @title
        @indent
        """
        if not indent:
            prefix = ""
        else:
            prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "leap_indicator":
                attrs[attr] = "%d - %s" % (attrs[attr], NTP_LEAP[attrs[attr]])
            elif attr == "mode":
                attrs[attr] = "%d - %s" % (attrs[attr], NTP_MODE[attrs[attr]])
            elif attr == "stratum":
                if attrs[attr] > 17:
                    attrs[attr] = "%d - %s" % (attrs[attr], NTP_STRATUM[17])
                else:
                    attrs[attr] = "%d - %s" % (attrs[attr], NTP_STRATUM[attrs[attr]])
            elif attr == "poll_exponent":
                attrs[attr] = "%d   (%ds)" % (attrs[attr], 2**attrs[attr])
            elif attr == "reference_id":
                if self.stratum in [0,1]:
                    attrs[attr] = "%s   (%s)" % (attrs[attr].hex(), \
                            convert_hex2ascii(attrs[attr].hex()) if attrs[attr].hex().strip('0') else " ")
                else:
                    if self.ip_version == 0x04:
                        attrs[attr] = "%s   (%s)" % (attrs[attr].hex(), format_ip2s(attrs[attr], self.ip_version))
            elif attr in ["reference_timestamp","origin_timestamp","receive_timestamp","transmit_timestamp"]:
                attrs[attr] = "%s (%s)" % (attrs[attr], \
                        format_datetime(int(attrs[attr].split('.')[0])-DIFF_1900_1970))

        data_out = ""
        for attr in attrs.keys():
            if attr in ["extension","key_id","dgst","ip_version"]:
                continue
            data_out += prefix + "| %-*s | %-*s |\n" % (25, attr.replace('_', ' ').title(), 45, str(attrs[attr]))

        title_out = prefix + "|  %s  |\n" % title
        num_sep = 77

        outmsg = title_out + \
                prefix + "-"*num_sep + "\n" + \
                prefix + data_out + \
                prefix + "-"*num_sep + "\n"

        return outmsg 

    def format_output(self, stdout, title="\033[32m NTP Message \033[0m", indent=0):
        """
        Output ntp message.
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))

    @classmethod
    def create(cls, ntp_data, ip_version):
        """
        Create object of NTPMessage.
        @ntp_data
        """
        if not ntp_data or len(ntp_data) < 48:
            raise ParamsError()

        if not ip_version in [0x04, 0x06]:
            raise ParamsError()

        ntpmsg = cls(
                     leap_indicator=(ntp_data[0] >> 6) & 0x03,
                     version=(ntp_data[0] >> 3) & 0x07,
                     mode=ntp_data[0] & 0x07,
                     stratum=ntp_data[1],
                     poll_exponent=ntp_data[2],
                     precision_exponent=-(((ntp_data[3]-1)&0x7F)^0x7F),
                     root_delay=int(ntp_data[4:6].hex(), 16) + int(ntp_data[6:8].hex(),16)*(1/(2**16)),
                     root_dispersion=int(ntp_data[8:10].hex(), 16) + int(ntp_data[10:12].hex(),16)*(1/(2**16)),
                     reference_id=ntp_data[12:16],
                     reference_timestamp="%d.%09d" % (int(ntp_data[16:20].hex(), 16), \
                             int(int(ntp_data[20:24].hex(), 16)*(1/(2**32))*1000000000)),
                     origin_timestamp="%d.%09d" % (int(ntp_data[24:28].hex(), 16), \
                             int(int(ntp_data[28:32].hex(), 16)*(1/(2**32))*1000000000)),
                     receive_timestamp="%d.%09d" % (int(ntp_data[32:36].hex(), 16), \
                             int(int(ntp_data[36:40].hex(), 16)*(1/(2**32))*1000000000)),
                     transmit_timestamp="%d.%09d" % (int(ntp_data[40:44].hex(), 16), \
                             int(int(ntp_data[44:48].hex(), 16)*(1/(2**32))*1000000000)),
                     ip_version = ip_version
                    )

        return ntpmsg
