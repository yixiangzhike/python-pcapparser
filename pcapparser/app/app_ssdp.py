#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""
from pcapparser.utils import convert_hex2ascii

__all__ = [
    'SSDPMessage',
]

class SSDPMessage(object):
    def __init__(self, ssdp_data):
        self.ssdp_data = ssdp_data

    def format_outmsg(self, title="\033[32m SSDP Message \033[0m", indent=0):
        """
        Format output message.
        @title
        @indent
        """
        if not indent:
            prefix = ""
        else:
            prefix = "%*s" % (indent, " ")

        data_out = convert_hex2ascii(self.ssdp_data.hex())

        title_out = prefix + "|  %s  |\n" % title
        num_sep = 100

        outmsg = title_out + \
                prefix + "-"*num_sep + "\n" + \
                prefix + data_out + "\n" + \
                prefix + "-"*num_sep + "\n"

        return outmsg 

    def format_output(self, stdout, title="\033[32m SSDP Message \033[0m", indent=0):
        """
        Output ntp message.
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
