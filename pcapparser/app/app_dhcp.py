#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""
from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.utils import format_ip2s,convert_hex2ascii,conv_format_mac
from pcapparser.app.dhcp_option import *

__all__ = [
    'DHCPMessage',
    'DHCPv6Message',
    'DHCPv6RelayMessage',
]

# https://www.rfc-editor.org/rfc/rfc2131
DHCP_OPCODES = {
    1 : 'Client Request Message',
    2 : 'Server Response Message',
}

DHCP_HARDWARE_TYPES = {
    1 : 'Ethernet (10mb/s)',
}

class BaseMessage(object):
    @classmethod
    def unpack_options(self, options):
        """
        Parse every option from options_data
        @options_data : options string include one or more option-TLV data
        """
        opts = []
        if not options:
            return opts
        
        if not isinstance(options, bytes):
            raise ParamsError()
        
        options_data = options
        while True:
            opt_type = options_data[0]
            if opt_type == 0x00:
                # padding
                options_data = options_data[1:]
                continue
            elif opt_type == 0xff:
                # option end
                break

            opt_length = options_data[1]

            if opt_type in range(255):
                option = DHCPOption(opt_type=opt_type, opt_length=opt_length, opt_value=options_data[2:2+opt_length])
            else:
                raise ParamsError("DHCP Option type[%d] not supported currently." % opt_type)

            if option:
                option.parse_option_body()
                opts.append(option)
            options_data = options_data[2+opt_length:]

        return opts

    def format_output(self, stdout, title="\033[32m DHCP Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        if self.options:
            count = 0
            for option in self.options:
                option.format_output(stdout, title="\033[32m DHCP Option [%d] \033[0m" % count, indent=indent)
                count += 1

class DHCPMessage(BaseMessage):
    """
    DHCP Message Fromat:
    -----------------------------------------------------------------------------------------------------------
    |Opcode|Htype|Hlen|Hops|Transaction Id|Seconds|Flags|Ciaddr|Yiaddr|Siaddr|Giaddr|Chaddr|sname|file|options|
    -----------------------------------------------------------------------------------------------------------
    | 1    |  1  |  1 | 1  |    4         |   2   |  2  |  4   |  4   |   4  |  4   |  16  |  64 | 128|   n   |
    -----------------------------------------------------------------------------------------------------------

    Opcode:   1 - client request message 
              2 - server response message

    Flags:    The leftmost bit is defined as the BROADCAST (B) flag.
              The remaining bits of the flags field are reserved for future use.  
              They MUST be set to zero by clients and ignored by servers and relay agents.
              The leftmost bit: 0 - send response in unicast
                                1 - send response in broadcast

    Reference: https://www.rfc-editor.org/rfc/rfc2131.txt
    """

    def __init__(self, opcode=0x00, hardware_type=0x00, hardware_length=0x00, 
                 hops=0x00, transaction_id=0x00, seconds=0x00, flags=0x00, 
                 client_ip_addr="", your_ip_addr="", next_server_ip_addr="",
                 relay_agent_ip_addr="", client_mac="", server_hostname="", 
                 boot_file="", magic_cookie="", options=[]):
        """
        @opcode
        @hardware_type
        @hardware_length
        @hops
        @transaction_id
        @seconds
        @flags
        @client_ip_addr
        @your_ip_addr
        @next_server_ip_addr
        @relay_agent_ip_addr
        @client_mac
        @server_hostname
        @boot_file
        @magic_cookie
        @options
        """
        self.opcode = opcode
        self.hardware_type = hardware_type
        self.hardware_length = hardware_length
        self.hops = hops
        self.transaction_id = transaction_id
        self.seconds = seconds
        self.flags = flags
        self.client_ip_addr = client_ip_addr
        self.your_ip_addr = your_ip_addr
        self.next_server_ip_addr = next_server_ip_addr
        self.relay_agent_ip_addr = relay_agent_ip_addr
        self.client_mac = client_mac
        self.server_hostname = server_hostname
        self.boot_file = boot_file
        self.magic_cookie = magic_cookie
        self.options = options

    def format_outmsg(self, title="\033[32m DHCP Message \033[0m", indent=0):
        """
        Format output message.
        @title
        @indent
        """
        if not indent:
            prefix = ""
        else:
            prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "opcode":
                attrs[attr] = "%d - %s" % (attrs[attr], DHCP_OPCODES[attrs[attr]])
            elif attr == "hardware_type":
                attrs[attr] = "%d - %s" % (attrs[attr], DHCP_HARDWARE_TYPES[attrs[attr]])
            elif attr == "flags":
                if attrs[attr] == 0x00:
                    attrs[attr] = "%s   (%s)" % (str(bin(attrs[attr])), "Unicast")
                elif attrs[attr] == 0x8000:
                    attrs[attr] = "%s   (%s)" % (str(bin(attrs[attr])), "Broadcast")
                else:
                    attrs[attr] = "%s   (%s)" % (str(bin(attrs[attr])), "Unknown")

        data_out = ""
        for attr in attrs.keys():
            if attr in ["options"]:
                continue

            data_out += prefix + "| %-*s | %-*s |\n" % (25, attr.replace('_', ' ').title(), 60, str(attrs[attr]))

        title_out = prefix + "|  %s  |\n" % title
        num_sep = 92

        outmsg = title_out + \
                prefix + "-"*num_sep + "\n" + \
                prefix + data_out + \
                prefix + "-"*num_sep + "\n"

        return outmsg 

    @classmethod
    def create(cls, dhcp_data):
        """
        Create object of DHCPMessage.
        @dhcp_data
        """
        if not dhcp_data:
            raise ParamsError()

        magic_cookie = dhcp_data[236:240].hex()
        opts = cls.unpack_options(dhcp_data[240:])

        if not dhcp_data[44:108].hex().strip('0'):
            shostname = "not given"
        else:
            shostname = convert_hex2ascii(dhcp_data[44:108].hex())
        if not dhcp_data[108:236].hex().strip('0'):
            bootfile = "not given"
        else:
            bootfile = convert_hex2ascii(dhcp_data[108:236].hex())

        dhcpmsg = cls(
                      opcode=dhcp_data[0],
                      hardware_type=dhcp_data[1],
                      hardware_length=dhcp_data[2],
                      hops=dhcp_data[3],
                      transaction_id=int(dhcp_data[4:8].hex(), 16),
                      seconds=int(dhcp_data[8:10].hex(), 16),
                      flags=int(dhcp_data[10:12].hex(), 16),
                      client_ip_addr=format_ip2s(dhcp_data[12:16], 0x04),
                      your_ip_addr=format_ip2s(dhcp_data[16:20], 0x04),
                      next_server_ip_addr=format_ip2s(dhcp_data[20:24], 0x04),
                      relay_agent_ip_addr=format_ip2s(dhcp_data[24:28], 0x04),
                      client_mac=conv_format_mac(dhcp_data[28:34]),
                      server_hostname=shostname,
                      boot_file=bootfile,
                      magic_cookie=magic_cookie,
                      options = opts
                    )

        return dhcpmsg


# https://www.rfc-editor.org/rfc/rfc3315
# https://www.rfc-editor.org/rfc/rfc8415
# https://www.iana.org/assignments/dhcpv6-parameters/dhcpv6-parameters.xhtml
"""
1	SOLICIT	[RFC8415]
2	ADVERTISE	[RFC8415]
3	REQUEST	[RFC8415]
4	CONFIRM	[RFC8415]
5	RENEW	[RFC8415]
6	REBIND	[RFC8415]
7	REPLY	[RFC8415]
8	RELEASE	[RFC8415]
9	DECLINE	[RFC8415]
10	RECONFIGURE	[RFC8415]
11	INFORMATION-REQUEST	[RFC8415]
12	RELAY-FORW	[RFC8415]
13	RELAY-REPL	[RFC8415]
14	LEASEQUERY	[RFC5007]
15	LEASEQUERY-REPLY	[RFC5007]
16	LEASEQUERY-DONE	[RFC5460]
17	LEASEQUERY-DATA	[RFC5460]
18	RECONFIGURE-REQUEST	[RFC6977]
19	RECONFIGURE-REPLY	[RFC6977]
20	DHCPV4-QUERY	[RFC7341]
21	DHCPV4-RESPONSE	[RFC7341]
22	ACTIVELEASEQUERY	[RFC7653]
23	STARTTLS	[RFC7653]
24	BNDUPD	[RFC8156]
25	BNDREPLY	[RFC8156]
26	POOLREQ	[RFC8156]
27	POOLRESP	[RFC8156]
28	UPDREQ	[RFC8156]
29	UPDREQALL	[RFC8156]
30	UPDDONE	[RFC8156]
31	CONNECT	[RFC8156]
32	CONNECTREPLY	[RFC8156]
33	DISCONNECT	[RFC8156]
34	STATE	[RFC8156]
35	CONTACT	[RFC8156]
"""
DHCPV6_MESSAGE_TYPES = {
        1   :   "SOLICIT",
        2   :   "ADVERTISE",
        3   :   "REQUEST",
        4   :   "CONFIRM",
        5   :   "RENEW",
        6   :   "REBIND",
        7   :   "REPLY",
        8   :   "RELEASE",
        9   :   "DECLINE",
        10  :   "RECONFIGURE",
        11  :   "INFORMATION-REQUEST",
        12  :   "RELAY-FORW",
        13  :   "RELAY-REPL",
        14  :   "LEASEQUERY",
        15  :   "LEASEQUERY-REPLY",
}

class DHCPv6BaseMessage(object):
    @classmethod
    def unpack_options(self, options):
        """
        Parse every option from options_data
        @options_data : options string include one or more option-TLV data
        """
        opts = []
        if not options:
            return opts

        if not isinstance(options, bytes):
            raise ParamsError()

        options_data = options
        options_length = len(options)
        pos = 0
        while True:
            opt_type = int(options_data[:2].hex(), 16)
            opt_length = int(options_data[2:4].hex(), 16)
            option = DHCPv6Option(opt_type=opt_type, opt_length=opt_length, opt_value=options_data[4:4+opt_length])
            if option:
                option.parse_option_body()
                opts.append(option)

            pos += 2 + 2 + opt_length
            if pos >= options_length:
                # end
                break

            options_data = options_data[4+opt_length:]

        return opts

    def format_output(self, stdout, title="\033[32m DHCPv6 Message \033[0m", indent=0):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        if self.options:
            count = 0
            for option in self.options:
                option.format_output(stdout, title="\033[32m DHCPv6 Option [%d] \033[0m" % count, indent=indent)
                count += 1

class DHCPv6Message(DHCPv6BaseMessage):
    """
    DHCPv6 Message Fromat:
    -------------------------------------------
    | message type | transaction id | options |
    -------------------------------------------
    |      1       |        3       |    n    |
    -------------------------------------------

    message type:
        1   :   SOLICIT
        2   :   ADVERTISE
        3   :   REQUEST
        4   :   CONFIRM
        5   :   RENEW
        6   :   REBIND
        7   :   REPLY
        8   :   RELEASE
        9   :   DECLINE
        10  :   RECONFIGURE
        11  :   INFORMATION-REQUEST

    Reference: https://www.rfc-editor.org/rfc/rfc3315
               https://www.rfc-editor.org/rfc/rfc8415
    """
    def __init__(self, message_type=0x00, transaction_id=0x00, options_data="", options=[]):
        """
        @message_type
        @transaction_id
        @options_data
        @options
        """
        self.message_type = message_type
        self.transaction_id = transaction_id
        self.options_data = options_data
        self.options = options

    @classmethod
    def create(cls, dhcpv6_data):
        """
        Create DHCPv6Message object
        @dhcpv6_data
        """
        if not dhcpv6_data:
            raise ParamsError()

        opts = cls.unpack_options(dhcpv6_data[4:])
        dhcpv6msg = cls(message_type=dhcpv6_data[0], transaction_id=int(dhcpv6_data[1:4].hex(), 16), 
                        options_data=dhcpv6_data[4:].hex(), options=opts)

        return dhcpv6msg

    def format_outmsg(self, title="\033[32m DHCPv6 Message \033[0m", indent=0):
        """
        Format output message
        @title
        @indent
        """
        if not indent:
            prefix = ""
        else:
            prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "message_type":
                attrs[attr] = "%d - %s" % (attrs[attr], DHCPV6_MESSAGE_TYPES[attrs[attr]])

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if attr == "options":
                continue

            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                if attr == "options_data":
                    if len(attrs[attr]) <= 64:
                        width = len(str(attrs[attr]))
                    else:
                        width = 64
                else:
                    width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            if attr == "options_data":
                pre_length = len(data_out)
                if len(attrs[attr]) <= 64:
                    data_out += " %-*s |" % (width, str(attrs[attr]))
                else:
                    n = len(attrs[attr])//64 if not len(attrs[attr])%64 else (len(attrs[attr])//64 + 1)
                    for idx in range(n):
                        if not idx:
                            data_out += " %-*s |" % (width, str(attrs[attr])[idx*64:(idx+1)*64])
                        else:
                            data_out += "\n" + prefix + "|%-*s|" % (pre_length - 2, " ")
                            data_out += " %-*s |" % (width, str(attrs[attr])[idx*64:(idx+1)*64])
            else:
                data_out += " %-*s |" % (width, str(attrs[attr]))

        title_out = prefix + "|  %s  |\n" % title

        outmsg = title_out + \
                prefix + "-"*num_sep + "\n" + \
                prefix + head_out + "\n" + \
                prefix + "-"*num_sep + "\n" + \
                prefix + data_out + "\n" + \
                prefix + "-"*num_sep + "\n"

        return outmsg 

class DHCPv6RelayMessage(DHCPv6BaseMessage):
    """
    DHCPv6 Relay Message Fromat:
    --------------------------------------------------------------------
    | message type | hop count | link address | peer address | options |
    --------------------------------------------------------------------
    |      1       |    1      |      12      |      12      |   n     |
    --------------------------------------------------------------------

    message type:
        12  :   RELAY-FORW
        13  :   RELAY-REPL

    Reference: https://www.rfc-editor.org/rfc/rfc3315
               https://www.rfc-editor.org/rfc/rfc8415
    """
    def __init__(self, message_type=0x00, hop_count=0x00, link_address="",
                 peer_address="", options_data="", options=[]):
        """
        @message_type
        @hop_count
        @link_address
        @perr_address
        @options_data
        @options
        """
        self.message_type = message_type
        self.hop_count = hop_count
        self.link_address = link_address
        self.peer_address = peer_address
        self.options_data = options_data
        self.options = options

    @classmethod
    def create(cls, dhcpv6_data):
        """
        Create DHCPv6Message object
        @dhcpv6_data
        """
        if not dhcpv6_data:
            raise ParamsError()

        opts = cls.unpack_options(dhcpv6_data[34:])
        dhcpv6msg = cls(message_type=dhcpv6_data[0],
                        hop_count=dhcpv6_data[1],
                        link_address=format_ip2s(dhcpv6_data[2:18], 0x06),
                        peer_address=format_ip2s(dhcpv6_data[18:34], 0x06),
                        options_data=dhcpv6_data[34:].hex(),
                        options=opts)

        return dhcpv6msg

    def format_outmsg(self, title="\033[32m DHCPv6 Relay Agent/Server Message \033[0m", indent=0):
        """
        Format output message
        @title
        @indent
        """
        if not indent:
            prefix = ""
        else:
            prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "message_type":
                attrs[attr] = "%d - %s" % (attrs[attr], DHCPV6_MESSAGE_TYPES[attrs[attr]])

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if attr == "options":
                continue

            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                if attr == "options_data":
                    if len(attrs[attr]) <= 64:
                        width = len(str(attrs[attr]))
                    else:
                        width = 64
                else:
                    width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            if attr == "options_data":
                pre_length = len(data_out)
                if len(attrs[attr]) <= 64:
                    data_out += " %-*s |" % (width, str(attrs[attr]))
                else:
                    n = len(attrs[attr])//64 if not len(attrs[attr])%64 else (len(attrs[attr])//64 + 1)
                    for idx in range(n):
                        if not idx:
                            data_out += " %-*s |" % (width, str(attrs[attr])[idx*64:(idx+1)*64])
                        else:
                            data_out += "\n" + prefix + "|%-*s|" % (pre_length - 2, " ")
                            data_out += " %-*s |" % (width, str(attrs[attr])[idx*64:(idx+1)*64])
            else:
                data_out += " %-*s |" % (width, str(attrs[attr]))

        title_out = prefix + "|  %s  |\n" % title

        outmsg = title_out + \
                prefix + "-"*num_sep + "\n" + \
                prefix + head_out + "\n" + \
                prefix + "-"*num_sep + "\n" + \
                prefix + data_out + "\n" + \
                prefix + "-"*num_sep + "\n"

        return outmsg 

