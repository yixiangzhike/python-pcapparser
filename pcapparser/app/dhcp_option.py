#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""
from re import findall
from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.utils import format_ip2s,conv_format_mac,convert_hex2ascii
from pcapparser.constants import ARP_HARDWARE_TYPES

# https://www.rfc-editor.org/rfc/rfc2132#page-4
DHCP_NETBIOS_NODE_TYPES = {
    0x1  :  "B-node",
    0x2  :  "P-node",
    0x4  :  "M-node",
    0x8  :  "H-node",
}

DHCP_OPTION_OVERLOAD_TYPES = {
    1    :  "the file field is used to hold options",
    2    :  "the sname field is used to hold options",
    3    :  "both fields are used to hold options",
}

DHCP_MESSAGE_TYPES = {
    1    :  "DHCPDISCOVER",
    2    :  "DHCPOFFER",
    3    :  "DHCPREQUEST",
    4    :  "DHCPDECLINE",
    5    :  "DHCPACK",
    6    :  "DHCPNAK",
    7    :  "DHCPRELEASE",
    8    :  "DHCPINFORM",
}

class DHCPOption(object):
    """
    Option format(TLV):
    ------------------------------------------------
    | Option Type  | Option  Length | Options Data |
    ------------------------------------------------
    |   1byte      |       1byte    |       n      |
    ------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc2132.txt
               https://www.rfc-editor.org/rfc/rfc1497.txt
               https://www.rfc-editor.org/rfc/rfc3925.txt
    """
    # https://www.iana.org/assignments/bootp-dhcp-parameters/bootp-dhcp-parameters.xhtml
    DHCP_OPTION_TYPES = {
        0    :  "Pad",
        1    :  "Subnet Mask",
        2    :  "Time Offset",
        3    :  "Router",
        4    :  "Time Server",
        5    :  "Name Server",
        6    :  "Domain Server",
        7    :  "Log Server",
        8    :  "Quotes Server",
        9    :  "LPR Server",
        10   :  "Impress Server",
        11   :  "RLP Server",
        12   :  "Hostname",
        13   :  "Boot File Size",
        14   :  "Merit Dump File",
        15   :  "Domain Name",
        16   :  "Swap Server",
        17   :  "Root Path",
        18   :  "Extension File",
        19   :  "Forward On/Off",
        20   :  "SrcRte On/Off",
        21   :  "Policy Filter",
        22   :  "Max DG Assembly",
        23   :  "Default IP TTL",
        24   :  "MTU Timeout",
        25   :  "MTU Plateau",
        26   :  "MTU Interface",
        27   :  "MTU Subnet",
        28   :  "Broadcast Address",
        29   :  "Mask Discovery",
        30   :  "Mask Supplier",
        31   :  "Router Discovery",
        32   :  "Router Request",
        33   :  "Static Route",
        34   :  "Trailers",
        35   :  "ARP Timeout",
        36   :  "Ethernet",
        37   :  "Default TCP TTL",
        38   :  "Keepalive Time",
        39   :  "Keepalive Data",
        40   :  "NIS Domain",
        41   :  "NIS Servers",
        42   :  "NTP Servers",
        43   :  "Vendor Specific",
        44   :  "NETBIOS Name Srv",
        45   :  "NETBIOS Dist Srv",
        46   :  "NETBIOS Node Type",
        47   :  "NETBIOS Scope",
        48   :  "X Window Font",
        49   :  "X Window Manager",
        50   :  "Address Request",
        51   :  "Address Time",
        52   :  "Overload",
        53   :  "DHCP Msg Type",
        54   :  "DHCP Server Id",
        55   :  "Parameter List",
        56   :  "DHCP Message",
        57   :  "DHCP Max Msg Size",
        58   :  "Renewal Time",
        59   :  "Rebinding Time",
        60   :  "Class Id",
        61   :  "Client Id",
        62   :  "NetWare/IP Domain",
        63   :  "NetWare/IP Option",
        64   :  "NIS-Domain-Name",
        65   :  "NIS-Server-Addr",
        66   :  "Server-Name",
        67   :  "Bootfile-Name",
        68   :  "Home-Agent-Addrs",
        69   :  "SMTP-Server",
        70   :  "POP3-Server",
        71   :  "NNTP-Server",
        72   :  "WWW-Server",
        73   :  "Finger-Server",
        74   :  "IRC-Server",
        75   :  "StreetTalk-Server",
        76   :  "STDA-Server",
        77   :  "User-Class",
        78   :  "Directory Agent",
        79   :  "Service Scope",
        80   :  "Rapid Commit",
        81   :  "Client FQDN",
        82   :  "Relay Agent Information",
        83   :  "iSNS",
        84   :  "REMOVED/Unassigned",
        85   :  "NDS Servers",
        86   :  "NDS Tree Name",
        87   :  "NDS Context",
        88   :  "BCMCS Controller Domain Name list",
        89   :  "BCMCS Controller IPv4 address option",
        90   :  "Authentication",
        91   :  "client-last-transaction-time option",
        92   :  "associated-ip option",
        93   :  "Client System",
        94   :  "Client NDI",
        95   :  "LDAP",
        96   :  "REMOVED/Unassigned",
        97   :  "UUID/GUID",
        98   :  "User-Auth",
        99   :  "GEOCONF_CIVIC",
        100  :  "PCode",
        101  :  "TCode",
        108  :  "IPv6-Only Preferred",
        109  :  "OPTION_DHCP4O6_S46_SADDR",
        110  :  "REMOVED/Unassigned",
        111  :  "Unassigned",
        112  :  "Netinfo Address",
        113  :  "Netinfo Tag",
        114  :  "DHCP Captive-Portal",
        115  :  "REMOVED/Unassigned",
        116  :  "Auto-Config",
        117  :  "Name Service Search",
        118  :  "Subnet Selection Option",
        119  :  "Domain Search",
        120  :  "SIP Servers DHCP Option",
        121  :  "Classless Static Route Option",
        122  :  "CCC",
        123  :  "GeoConf Option",
        124  :  "V-I Vendor Class",
        125  :  "V-I Vendor-Specific Information",
        126  :  "Removed/Unassigned",
        127  :  "Removed/Unassigned",
        136  :  "OPTION_PANA_AGENT",
        137  :  "OPTION_V4_LOST",
        138  :  "OPTION_CAPWAP_AC_V4",
        139  :  "OPTION-IPv4_Address-MoS",
        140  :  "OPTION-IPv4_FQDN-MoS",
        141  :  "SIP UA Configuration Service Domains",
        142  :  "OPTION-IPv4_Address-ANDSF",
        143  :  "OPTION_V4_SZTP_REDIRECT",
        144  :  "GeoLoc",
        145  :  "FORCERENEW_NONCE_CAPABLE",
        146  :  "RDNSS Selection",
        147  :  "OPTION_V4_DOTS_RI",
        148  :  "OPTION_V4_DOTS_ADDRESS",
        149  :  "Unassigned",
        151  :  "status-code",
        152  :  "base-time",
        153  :  "start-time-of-state",
        154  :  "query-start-time",
        155  :  "query-end-time",
        156  :  "dhcp-state",
        157  :  "data-source",
        158  :  "OPTION_V4_PCP_SERVER",
        159  :  "OPTION_V4_PORTPARAMS",
        160  :  "Unassigned",
        161  :  "OPTION_MUD_URL_V4",
        162  :  "OPTION_V4_DNR",
        208  :  "PXELINUX Magic",
        209  :  "Configuration File",
        210  :  "Path Prefix",
        211  :  "Reboot Time",
        212  :  "OPTION_6RD",
        213  :  "OPTION_V4_ACCESS_DOMAIN",
        220  :  "Subnet Allocation Option",
        221  :  "Virtual Subnet Selection (VSS) Option",
        255  :  "End",
    }

    def __init__(self, opt_type=0x00, opt_length=0x00, opt_value=b''):
        """
        @opt_type
        @opt_length : the length of option, inclue type,length,data
        @opt_value
        """
        self.opt_type = opt_type
        self.opt_length = opt_length
        self.opt_value = opt_value

    def parse_option_body(self):
        """
        Parse option data.
        """
        self.values = []
        if self.opt_type in [2,22,23,24,26,35,37,38,51,57,58,59]:
            # Time Offset
            # Maximum Datagram Reassembly Size
            # Default IP Time-to-live
            # Path MTU Aging Timeout
            # Interface MTU
            # ARP Cache Timeout
            # TCP Default TTL
            # TCP Keepalive Interval
            # IP Address Lease Time
            # Maximum DHCP Message Size
            # Renewal (T1) Time Value
            # Rebinding (T2) Time Value
            self.values.append(str(int(self.opt_value.hex(), 16)))
        elif self.opt_type in [13]:
            # Boot File Size
            self.values.append(str(int(self.opt_value.hex(), 16)*512))
        elif self.opt_type in [1,3,4,5,6,7,8,9,10,11,16,28,32,41,42,44,45,48,49,65,68,69,70,71,72,73,74,75,76,50,54]:
            # Subnet mask
            # Router 
            # Time server
            # Name server
            # Domain name server
            # Log server
            # Cookie server
            # LPR server
            # Impress server
            # Resource location server
            # Swap server
            # Broadcast Address
            # Router Solicitation Address
            # Network Information Servers
            # Network Time Protocol Servers
            # NetBIOS over TCP/IP Name Server
            # NetBIOS over TCP/IP Datagram Distribution Server
            # X Window System Font Server
            # X Window System Display Manager
            # Network Information Service+ Servers
            # Mobile IP Home Agent
            # Simple Mail Transport Protocol (SMTP) Server
            # Post Office Protocol (POP3) Server
            # Network News Transport Protocol (NNTP) Server
            # Default World Wide Web (WWW) Server
            # Default Finger Server
            # Default Internet Relay Chat (IRC) Server
            # StreetTalk Server
            # StreetTalk Directory Assistance (STDA) Server
            # Requested IP
            # Server Identifier
            n = self.opt_length // 4
            for idx in range(n):
                self.values.append(format_ip2s(self.opt_value[idx*4:(idx+1)*4], 0x04))
        elif self.opt_type in [12,14,15,17,18,40,47,64,66,67,56,60]:
            # Host name
            # Dump file pathname
            # Domain name
            # Root dist pathname
            # Extensions pathname
            # Network Information Service Domain
            # NetBIOS over TCP/IP Scope
            # NIS Client Domain Name
            # TFTP server name
            # Bootfile name
            # Message
            # Vendor Class ID
            self.values.append(convert_hex2ascii(self.opt_value.hex()))
        elif self.opt_type in [19,20,27,29,30,31,34,36,39]:
            # IP Forwarding Enable/Disable
            # Non-Local Source Routing Enable/Disable
            # All Subnets are Local
            # Perform Mask Discovery
            # Mask Supplier
            # Perform Router Discovery
            # Trailer Encapsulation
            # Ethernet Encapsulation
            # TCP Keepalive Garbage
            self.values.append("%d - %s" % (self.opt_type, "Enable" if self.opt_type else "Disable"))
        elif self.opt_type in [21]:
            # Policy Filter (Address + Mask)
            n = self.opt_length // 8
            for idx in range(n):
                self.values.append({"address":format_ip2s(self.opt_value[idx*8:(idx+1)*8-4], 0x04),\
                               "mask":format_ip2s(self.opt_value[idx*8+4:(idx+1)*8], 0x04)})
        elif self.opt_type in [25]:
            # Path MTU Plateau Table
            n = self.opt_length // 2
            for idx in range(n):
                self.values.appen(str(int(self.opt_value[idx*2:(idx+1)*2].hex(), 16)))
        elif self.opt_type in [33]:
            # Policy Filter (Destination + Router)
            n = self.opt_length // 8
            for idx in range(n):
                self.values.append({"destination":format_ip2s(self.opt_value[idx*8:(idx+1)*8-4], 0x04),\
                               "router":format_ip2s(self.opt_value[idx*8+4:(idx+1)*8], 0x04)})
        elif self.opt_type in [46]:
            # NetBIOS over TCP/IP Node Type
            self.values.append("%d - %s" % (self.opt_value[0], DHCP_NETBIOS_NODE_TYPES[self.opt_value[0]]))
        elif self.opt_type in [52]:
            # Option Overload
            self.values.append("%d - %s" % (self.opt_value[0], DHCP_OPTION_OVERLOAD_TYPES[self.opt_value[0]]))
        elif self.opt_type in [53]:
            # DHCP Message Type
            self.values.append("%d - %s" % (self.opt_value[0], DHCP_MESSAGE_TYPES[self.opt_value[0]]))
        elif self.opt_type in [55]:
            # Parameter Request List
            self.values.append(".".join([str(x) for x in self.opt_value]))
        elif self.opt_type in [55]:
            # Vendor class identifier
            self.values.append(self.opt_value.hex())
        elif self.opt_type in [61]:
            # Client-identifier
            hwtype = self.opt_value[0]
            if hwtype == 1:
                client_id = conv_format_mac(self.opt_value[1:self.opt_length])
            else:
                client_id = self.opt_value[1:self.opt_length].hex()
            self.values.append({"type":"%d - %s"%(hwtype, ARP_HARDWARE_TYPES[hwtype]), "clientId":client_id})
        elif self.opt_type in [124]:
            # Vendor-Identifying Vendor Class
            subopt_data = self.opt_value
            subopt_length = len(self.opt_value)
            pos = 0
            while True:
                enterprise_number = subopt_data[:4].hex()
                length = subopt_data[4]
                vendor_class = subopt_data[5:5+length].hex()
                self.values.append({"number":enterprise_number, "length":length, "vendor_class":vendor_class})
                pos += 4+1+length
                if pos >= subopt_length:
                    # end
                    break
                subopt_data = subopt_data[pos:]
        elif self.opt_type in [125]:
            # Vendor-Identifying Vendor-Specific Information
            subopt_data = self.opt_value
            subopt_length = len(self.opt_value)
            pos = 0
            while True:
                enterprise_number = subopt_data[:4].hex()
                length = subopt_data[4]
                vendor_specific_options = subopt_data[5:5+length].hex()
                self.values.append({"number":enterprise_number, "length":length, \
                                    "vendor_options":vendor_specific_options})
                pos += 4+1+length
                if pos >= subopt_length:
                    # end
                    break
                subopt_data = subopt_data[pos:]
        else:
            self.values.append(self.opt_value.hex())

    def format_base_outmsg(self, title="\033[32m Option \033[0m", indent=0):
        """
        Format output message
        @title
        @indent
        """
        if indent:
            prefix = "%*s" % (indent, " ")
        else:
            prefix = ""

        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "opt_type":
                attrs[attr] = "%d - %s" % (attrs[attr], self.DHCP_OPTION_TYPES[attrs[attr]])
            elif attr == "opt_value":
                attrs[attr] = attrs[attr].hex()

        head_out = "|"
        data_out = "|"
        sep_num = 1
        for attr in attrs.keys():
            if attr == "values":
                continue

            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            sep_num += width
            head_out += " %-*s |" % (width, attr.replace("_", " ").title())
            data_out += " %-*s |" % (width, str(attrs[attr]))
            sep_num += 3

        extensions = ""
        if not self.values:
            head_out += "\n"
            data_out += "\n"
        else:
            if isinstance(self.values[0], dict):
                head_out += "\n"
                data_out += "\n"

                ext_title = "| Opt Value Detail |\n"

                for entry in self.values:
                    ext_head_out = "|"
                    ext_data_out = "|"
                    ext_sep_num = 1
                    for key,extval in entry.items():
                        if len(key) > len(str(extval)):
                            ext_width = len(key)
                        else:
                            ext_width = len(str(extval))

                        ext_sep_num += ext_width
                        ext_head_out += " %-*s |" % (ext_width, key.replace("_", " ").title())
                        ext_data_out += " %-*s |" % (ext_width, extval)
                        ext_sep_num += 3
                    ext_head_out += "\n"
                    ext_data_out += "\n"

                    extensions += prefix + ext_title + \
                                  prefix + "-"*ext_sep_num + "\n" + \
                                  prefix + ext_head_out + \
                                  prefix + "-"*ext_sep_num + "\n" + \
                                  prefix + ext_data_out + \
                                  prefix + "-"*ext_sep_num + "\n"
            else:
                if len(self.values) < 3:
                    for val in self.values:
                        width = 5
                        if len(val) > 5:
                            width = len(val)

                        sep_num += width
                        head_out += " %-*s |" % (width, "Value")
                        data_out += " %-*s |" % (width, val)
                        sep_num += 3
                    head_out += "\n"
                    data_out += "\n"
                else:
                    head_out += "\n"
                    data_out += "\n"

                    ext_title = "| Opt Value Detail |\n"

                    ext_data_out = ""
                    for extval in self.values:
                        ext_data_out += prefix + "|"
                        ext_sep_num = 1
                        ext_width = 50
                        ext_sep_num += ext_width
                        ext_data_out += " %-*s |\n" % (ext_width, extval)
                        ext_sep_num += 3

                    extensions = prefix + ext_title + \
                                 prefix + "-"*ext_sep_num + "\n" + \
                                 ext_data_out + \
                                 prefix + "-"*ext_sep_num + "\n"

        title_out = prefix + "| %-s |\n" % title

        return title_out + prefix + "-"*sep_num + "\n" + \
               prefix + head_out + \
               prefix + "-"*sep_num + "\n" + \
               prefix + data_out + \
               prefix + "-"*sep_num + "\n" + extensions 

    def format_outmsg(self, title="\033[32m Option \033[0m", indent=0):
        """
        Format output message
        @title
        @indent
        """
        return self.format_base_outmsg(title=title, indent=indent)

    def format_output(self, stdout, title="\033[32m Option \033[0m", indent=0):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))

# https://www.rfc-editor.org/rfc/rfc8415
# https://www.rfc-editor.org/rfc/rfc5007
DHCPv6_STATUS_CODES = {
    0    :   "Success",
    1    :   "UnspecFail",
    2    :   "NoAddrsAvail",
    3    :   "NoBinding",
    4    :   "NotOnLink",
    5    :   "UseMulticast",
    6    :   "NoPrefixAvail",
    7    :   "UnknownQueryType",
    8    :   "MalformedQuery",
    9    :   "NotConfigured",
    10   :   "NotAllowed",
}

class DHCPv6Option(DHCPOption):
    """
    DHCPv6 Option format(TLV):
    ------------------------------------------------
    | Option Type  | Option  Length | Options Data |
    ------------------------------------------------
    |   2bytes     |       2bytes   |       n      |
    ------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc3315.txt
               https://www.rfc-editor.org/rfc/rfc8415.txt
               https://www.rfc-editor.org/rfc/rfc7598.txt
               https://www.rfc-editor.org/rfc/rfc8539.txt
    """

    """
    https://www.iana.org/assignments/dhcpv6-parameters/dhcpv6-parameters.xhtml

    1	OPTION_CLIENTID	[RFC8415]
    2	OPTION_SERVERID	[RFC8415]
    3	OPTION_IA_NA	[RFC8415]
    4	OPTION_IA_TA	[RFC8415]
    5	OPTION_IAADDR	[RFC8415]
    6	OPTION_ORO	[RFC8415]
    7	OPTION_PREFERENCE	[RFC8415]
    8	OPTION_ELAPSED_TIME	[RFC8415]
    9	OPTION_RELAY_MSG	[RFC8415]
    11	OPTION_AUTH	[RFC8415]
    12	OPTION_UNICAST	[RFC8415]
    13	OPTION_STATUS_CODE	[RFC8415]
    14	OPTION_RAPID_COMMIT	[RFC8415]
    15	OPTION_USER_CLASS	[RFC8415]
    16	OPTION_VENDOR_CLASS	[RFC8415]
    17	OPTION_VENDOR_OPTS	[RFC8415]
    18	OPTION_INTERFACE_ID	[RFC8415]
    19	OPTION_RECONF_MSG	[RFC8415]
    20	OPTION_RECONF_ACCEPT	[RFC8415]
    21	OPTION_SIP_SERVER_D	[RFC3319]
    22	OPTION_SIP_SERVER_A	[RFC3319]
    23	OPTION_DNS_SERVERS	[RFC3646]
    24	OPTION_DOMAIN_LIST	[RFC3646]
    25	OPTION_IA_PD	[RFC3633][RFC8415]
    26	OPTION_IAPREFIX	[RFC3633][RFC8415]
    27	OPTION_NIS_SERVERS	[RFC3898]
    28	OPTION_NISP_SERVERS	[RFC3898]
    29	OPTION_NIS_DOMAIN_NAME	[RFC3898]
    30	OPTION_NISP_DOMAIN_NAME	[RFC3898]
    31	OPTION_SNTP_SERVERS	[RFC4075]
    32	OPTION_INFORMATION_REFRESH_TIME	[RFC4242][RFC8415]
    33	OPTION_BCMCS_SERVER_D	[RFC4280]
    34	OPTION_BCMCS_SERVER_A	[RFC4280]
    36	OPTION_GEOCONF_CIVIC	[RFC4776]
    37	OPTION_REMOTE_ID	[RFC4649]
    38	OPTION_SUBSCRIBER_ID	[RFC4580]
    39	OPTION_CLIENT_FQDN	[RFC4704]
    40	OPTION_PANA_AGENT	[RFC5192]
    41	OPTION_NEW_POSIX_TIMEZONE	[RFC4833]
    42	OPTION_NEW_TZDB_TIMEZONE	[RFC4833]
    43	OPTION_ERO	[RFC4994]
    44	OPTION_LQ_QUERY	[RFC5007]
    45	OPTION_CLIENT_DATA	[RFC5007]
    46	OPTION_CLT_TIME	[RFC5007]
    47	OPTION_LQ_RELAY_DATA	[RFC5007]
    48	OPTION_LQ_CLIENT_LINK	[RFC5007]
    49	OPTION_MIP6_HNIDF	[RFC6610]
    50	OPTION_MIP6_VDINF	[RFC6610]
    51	OPTION_V6_LOST	[RFC5223]
    52	OPTION_CAPWAP_AC_V6	[RFC5417]
    53	OPTION_RELAY_ID	[RFC5460]
    54	OPTION-IPv6_Address-MoS	[RFC5678]
    55	OPTION-IPv6_FQDN-MoS	[RFC5678]
    56	OPTION_NTP_SERVER	[RFC5908]
    57	OPTION_V6_ACCESS_DOMAIN	[RFC5986]
    58	OPTION_SIP_UA_CS_LIST	[RFC6011]
    59	OPT_BOOTFILE_URL	[RFC5970]
    60	OPT_BOOTFILE_PARAM	[RFC5970]
    61	OPTION_CLIENT_ARCH_TYPE	[RFC5970]
    62	OPTION_NII	[RFC5970]
    63	OPTION_GEOLOCATION	[RFC6225]
    64	OPTION_AFTR_NAME	[RFC6334]
    65	OPTION_ERP_LOCAL_DOMAIN_NAME	[RFC6440]
    66	OPTION_RSOO	[RFC6422]
    67	OPTION_PD_EXCLUDE	[RFC6603]
    68	OPTION_VSS	[RFC6607]
    69	OPTION_MIP6_IDINF	[RFC6610]
    70	OPTION_MIP6_UDINF	[RFC6610]
    71	OPTION_MIP6_HNP	[RFC6610]
    72	OPTION_MIP6_HAA	[RFC6610]
    73	OPTION_MIP6_HAF	[RFC6610]
    74	OPTION_RDNSS_SELECTION	[RFC6731]
    75	OPTION_KRB_PRINCIPAL_NAME	[RFC6784]
    76	OPTION_KRB_REALM_NAME	[RFC6784]
    77	OPTION_KRB_DEFAULT_REALM_NAME	[RFC6784]
    78	OPTION_KRB_KDC	[RFC6784]
    79	OPTION_CLIENT_LINKLAYER_ADDR	[RFC6939]
    80	OPTION_LINK_ADDRESS	[RFC6977]
    81	OPTION_RADIUS	[RFC7037]
    82	OPTION_SOL_MAX_RT	[RFC7083][RFC8415]
    83	OPTION_INF_MAX_RT	[RFC7083][RFC8415]
    84	OPTION_ADDRSEL	[RFC7078]
    85	OPTION_ADDRSEL_TABLE	[RFC7078]
    86	OPTION_V6_PCP_SERVER	[RFC7291]
    87	OPTION_DHCPV4_MSG	[RFC7341]
    88	OPTION_DHCP4_O_DHCP6_SERVER	[RFC7341]
    89	OPTION_S46_RULE	[RFC7598]
    90	OPTION_S46_BR	[RFC7598][RFC8539]
    91	OPTION_S46_DMR	[RFC7598]
    92	OPTION_S46_V4V6BIND	[RFC7598]
    93	OPTION_S46_PORTPARAMS	[RFC7598]
    94	OPTION_S46_CONT_MAPE	[RFC7598]
    95	OPTION_S46_CONT_MAPT	[RFC7598]
    96	OPTION_S46_CONT_LW	[RFC7598]
    97	OPTION_4RD	[RFC7600]
    98	OPTION_4RD_MAP_RULE	[RFC7600]
    99	OPTION_4RD_NON_MAP_RULE	[RFC7600]
    100	OPTION_LQ_BASE_TIME	[RFC7653]
    101	OPTION_LQ_START_TIME	[RFC7653]
    102	OPTION_LQ_END_TIME	[RFC7653]
    103	DHCP Captive-Portal	[RFC8910]
    104	OPTION_MPL_PARAMETERS	[RFC7774]
    105	OPTION_ANI_ATT	[RFC7839]
    106	OPTION_ANI_NETWORK_NAME	[RFC7839]
    107	OPTION_ANI_AP_NAME	[RFC7839]
    108	OPTION_ANI_AP_BSSID	[RFC7839]
    109	OPTION_ANI_OPERATOR_ID	[RFC7839]
    110	OPTION_ANI_OPERATOR_REALM	[RFC7839]
    111	OPTION_S46_PRIORITY	[RFC8026]
    112	OPTION_MUD_URL_V6	[RFC8520]
    113	OPTION_V6_PREFIX64	[RFC8115]
    114	OPTION_F_BINDING_STATUS	[RFC8156]
    115	OPTION_F_CONNECT_FLAGS	[RFC8156]
    116	OPTION_F_DNS_REMOVAL_INFO	[RFC8156]
    117	OPTION_F_DNS_HOST_NAME	[RFC8156]
    118	OPTION_F_DNS_ZONE_NAME	[RFC8156]
    119	OPTION_F_DNS_FLAGS	[RFC8156]
    120	OPTION_F_EXPIRATION_TIME	[RFC8156]
    121	OPTION_F_MAX_UNACKED_BNDUPD	[RFC8156]
    122	OPTION_F_MCLT	[RFC8156]
    123	OPTION_F_PARTNER_LIFETIME	[RFC8156]
    124	OPTION_F_PARTNER_LIFETIME_SENT	[RFC8156]
    125	OPTION_F_PARTNER_DOWN_TIME	[RFC8156]
    126	OPTION_F_PARTNER_RAW_CLT_TIME	[RFC8156]
    127	OPTION_F_PROTOCOL_VERSION	[RFC8156]
    128	OPTION_F_KEEPALIVE_TIME	[RFC8156]
    129	OPTION_F_RECONFIGURE_DATA	[RFC8156]
    130	OPTION_F_RELATIONSHIP_NAME	[RFC8156]
    131	OPTION_F_SERVER_FLAGS	[RFC8156]
    132	OPTION_F_SERVER_STATE	[RFC8156]
    133	OPTION_F_START_TIME_OF_STATE	[RFC8156]
    134	OPTION_F_STATE_EXPIRATION_TIME	[RFC8156]
    135	OPTION_RELAY_PORT	[RFC8357]
    136	OPTION_V6_SZTP_REDIRECT	[RFC8572]
    137	OPTION_S46_BIND_IPV6_PREFIX	[RFC8539]
    138	OPTION_IA_LL	[RFC8947]
    139	OPTION_LLADDR	[RFC8947]
    140	OPTION_SLAP_QUAD	[RFC8948]
    141	OPTION_V6_DOTS_RI	[RFC8973]
    142	OPTION_V6_DOTS_ADDRESS	[RFC8973]
    143	OPTION-IPv6_Address-ANDSF	[RFC6153]
    144	OPTION_V6_DNR	[RFC-ietf-add-dnr-16]
    145	OPTION_REGISTERED_DOMAIN	[RFC-ietf-homenet-naming-architecture-dhc-options-24, Section 4.1]
    146	OPTION_FORWARD_DIST_MANAGER	[RFC-ietf-homenet-naming-architecture-dhc-options-24, Section 4.2]
    147	OPTION_REVERSE_DIST_MANAGER	[RFC-ietf-homenet-naming-architecture-dhc-options-24, Section 4.3]
    """
    DHCP_OPTION_TYPES = {
        1    :   "OPTION_CLIENTID",
        2    :   "OPTION_SERVERID",
        3    :   "OPTION_IA_NA",
        4    :   "OPTION_IA_TA",
        5    :   "OPTION_IAADDR",
        6    :   "OPTION_ORO",
        7    :   "OPTION_PREFERENCE",
        8    :   "OPTION_ELAPSED_TIME",
        9    :   "OPTION_RELAY_MSG",
        11   :   "OPTION_AUTH",
        12   :   "OPTION_UNICAST",
        13   :   "OPTION_STATUS_CODE",
        14   :   "OPTION_RAPID_COMMIT",
        15   :   "OPTION_USER_CLASS",
        16   :   "OPTION_VENDOR_CLASS",
        17   :   "OPTION_VENDOR_OPTS",
        18   :   "OPTION_INTERFACE_ID",
        19   :   "OPTION_RECONF_MSG",
        20   :   "OPTION_RECONF_ACCEPT",
        23   :   "DNS Recursive Name Server",
        24   :   "DNS Domain Search List",
        25   :   "OPTION_IA_PD",
        26   :   "OPTION_IAPREFIX",
        32   :   "OPTION_INFORMATION_REFRESH_TIME",
        37   :   "Relay Agent Remote-ID",
        38   :   "Relay Agent Subscriber-ID",
        39   :   "OPTION_CLIENT_FQDN",
        44   :   "OPTION_LQ_QUERY",
        45   :   "OPTION_CLIENT_DATA",
        46   :   "OPTION_CLT_TIME",
        47   :   "OPTION_LQ_RELAY_DATA",
        48   :   "OPTION_LQ_CLIENT_LINK",
        64   :   "AFTR Name",
        82   :   "OPTION_SOL_MAX_RT",
        83   :   "OPTION_INF_MAX_RT",
    }

    def parse_option_body(self):
        """
        Parse option data.
        https://www.rfc-editor.org/rfc/rfc8415
        https://www.rfc-editor.org/rfc/rfc7598
        https://www.rfc-editor.org/rfc/rfc8539
        """
        self.values = []
        if self.opt_type in [1,2]:
            # DUID: https://www.rfc-editor.org/rfc/rfc8415#page-32
            # Client Identifier
            # Server Identifier
            sub_type = int(self.opt_value[:2].hex(), 16)
            if sub_type in [1,3]:
                hardware_type = int(self.opt_value[2:4].hex(), 16)
                if sub_type == 1:
                    # DUID Based on Link-layer Address Plus Time [DUID-LLT]
                    time = int(self.opt_value[4:8].hex(), 16)
                    linklayer_address = conv_format_mac(self.opt_value[8:14])
                    obj = {"type":"%d-%s" % (sub_type, "DUID Base on Link-layer address plus time"),
                           "hardware_type":str(hardware_type),
                           "time":str(time),
                           "linklayer_address":linklayer_address}
                else:
                    # DUID Based on Link-layer Address
                    linklayer_address = conv_format_mac(self.opt_value[4:10])
                    obj = {"type":"%d-%s" % (sub_type, "DUID Base on Link-layer Address"),
                           "hardware_type":str(hardware_type),
                           "linklayer_address":linklayer_address}
            elif sub_type == 2:
                # DUID Assigned by Vendor Based on Enterprise Number [DUID-EN]
                enterprise_number = int(self.opt_value[2:6].hex(), 16)
                identifier = self.opt_value[6:self.opt_length].hex()
                obj = {"type":"%d-%s" % (sub_type, "DUID Base on Enterprise Number"),
                       "enterprise_number":str(enterprise_number),
                       "identifier":identifier}
            elif sub_type == 4:
                # Universally Unique Identifier
                obj = {"type":"%d-%s" % (sub_type, "Universally Unique Identifier"),
                       "UUID":self.opt_value[2:self.opt_length].hex()}
            self.values.append(obj)
        elif self.opt_type in [3,4]:
            iaid = int(self.opt_value[0:4].hex(), 16)
            if self.opt_type == 3:
                # Identity Association for Non-temporary Addresses
                t1 = int(self.opt_value[4:8].hex(), 16)
                t2 = int(self.opt_value[8:12].hex(), 16)
                iana_options = self.opt_value[12:self.opt_length].hex()
                obj = {"iaid":str(iaid), "t1":str(t1), "t2":str(t2), "iana_options":iana_options}
                self.values.append(obj)

                if len(iana_options) < 8:
                    return

                # parsing IA_NA options data
                pos = 0
                iana_data = iana_options[0:]
                while True:
                    iana_opt_type = int(iana_data[:4], 16)
                    iana_opt_length = int(iana_data[4:8], 16)
                    ext_obj = DHCPv6Option(opt_type=iana_opt_type,
                                           opt_length=iana_opt_length,
                                           opt_value=bytes.fromhex(iana_data[8:8+iana_opt_length*2]))
                    ext_obj.parse_option_body()
                    obj = {}
                    obj["iana_opt_type"] = "%d - %s" % (iana_opt_type, self.DHCP_OPTION_TYPES[iana_opt_type])
                    obj["iana_opt_length"] = str(iana_opt_length)
                    obj.update(ext_obj.values[0])
                    self.values.append(obj)

                    pos += 8 + iana_opt_length *2
                    if pos >= (self.opt_length-12) * 2:
                        # end
                        break
                    iana_data = iana_data[8+iana_opt_length*2:]
            elif self.opt_type == 4:
                # Identity Association for Temporary Addresses
                iata_options = self.opt_value[4:self.opt_length].hex()
                obj = {"iaid":str(iaid), "iata_options":iana_options}
                self.values.append(obj)
        elif self.opt_type in [5]:
            # IA Address
            ipv6_address = format_ip2s(self.opt_value[:16], 0x06)
            preferred_lifetime = int(self.opt_value[16:20].hex(), 16)
            valid_lifetime = int(self.opt_value[20:24].hex(), 16)
            iaaddr_options = self.opt_value[24:self.opt_length].hex()
            obj = {"ipv6_address":ipv6_address,
                   "preferred_lifetime":str(preferred_lifetime),
                   "valid_lifetime":str(valid_lifetime),
                   "iaaddr_options":iaaddr_options}
            self.values.append(obj)
        elif self.opt_type in [6,8]:
            # Option Request
            # Elapsed Time
            n = self.opt_length // 2
            for idx in range(n):
                self.values.append(str(int(self.opt_value[idx*2:(idx+1)*2].hex(), 16)))
        elif self.opt_type in [7,19]:
            # Preference Option
            # Reconfigure Message
            result = str(self.opt_value[0])
            if self.opt_type == 19:
                if self.opt_value[0] == 5:
                    result = "5 - %s" % "Renew message"
                elif self.opt_value[0] == 11:
                    result = "11 - %s" % "Information-request message"
                elif self.opt_value[0] == 6:
                    result = "6 - %s" % "Rebind message"
            self.values.append(result)
        elif self.opt_type in [9,18,45]:
            # Relay Message
            # Interface-Id
            # Client Data
            self.values.append(self.opt_value[:self.opt_length].hex())
        elif self.opt_type in [11]:
            # Authentication Option
            protocol = self.opt_value[0]
            algorithm = self.opt_value[1]
            RDM = self.opt_value[2]
            replay_detection = self.opt_value[3:11].hex()
            auth_info = self.opt_value[11:self.opt_length].hex()
            obj = {"protocol":str(protocol),
                   "algorithm":str(algorithm),
                   "RDM":str(RDM),
                   "replay_detection":replay_detection,
                   "auth_info":auth_info}
            self.values.append(obj)
        elif self.opt_type in [12]:
            # Server Unicast
            self.values.append(format_ip2s(self.opt_value[:16], 0x06))
        elif self.opt_type in [13]:
            # Status Code
            status_code = int(self.opt_value[:2].hex(), 16)
            status_message = convert_hex2ascii(self.opt_value[2:self.opt_length].hex())
            obj = {"status_code":"%d-%s" % (status_code, DHCPv6_STATUS_CODES[status_code]),
                   "status_message":status_message}
            self.values.append(obj)
        elif self.opt_type in [14,20]:
            # Rapid Commit
            # Reconfigure Accept
            self.values.append("NONE")
        elif self.opt_type in [15]:
            # User Class
            pos = 0
            user_data = self.opt_value
            while True:
                user_class_len = int(user_data[:2].hex(), 16)
                opaque_data = user_data[2:2+user_class_len].hex()

                obj = {"user_calss_len":str(user_class_len), "opaque_data":opaque_data}
                self.values.append(obj)

                pos += 2+user_class_len
                if pos >= self.opt_length:
                    # end
                    break

                user_data = user_data[pos:]
        elif self.opt_type in [16]:
            # Vendor Class
            enterprise_number = int(self.opt_value[:4].hex(), 16)
            pos = 4
            user_data = self.opt_value[4:self.opt_length]
            while True:
                vendor_class_len = int(user_data[:2].hex(), 16)
                opaque_data = user_data[2:2+vendor_class_len].hex()

                obj = {"enterprise_number":str(enterprise_number), 
                       "vendor_calss_len":str(vendor_class_len),
                       "opaque_data":opaque_data}
                self.values.append(obj)

                pos += 2+vendor_class_len
                if pos >= self.opt_length:
                    # end
                    break

                user_data = user_data[pos:]
        elif self.opt_type in [17]:
            # Vendor-specific Information
            enterprise_number = int(self.opt_value[:4].hex(), 16)
            pos = 4
            user_data = self.opt_value[4:self.opt_length]
            while True:
                opt_code = int(user_data[:2].hex(), 16)
                option_length = int(user_data[2:4].hex(), 16)
                option_data = user_data[4:4+option_length].hex()

                obj = {"enterprise_number":str(enterprise_number), 
                       "opt_code":str(opt_code),
                       "option_length":str(option_length),
                       "option_data":option_data}
                self.values.append(obj)

                pos += 4+option_length
                if pos >= self.opt_length:
                    # end
                    break

                user_data = user_data[pos:]
        elif self.opt_type in [25]:
            # Identity Association for Prefix Delegation
            iaid = int(self.opt_value[0:4].hex(), 16)
            t1 = int(self.opt_value[4:8].hex(), 16)
            t2 = int(self.opt_value[8:12].hex(), 16)
            iapd_options = self.opt_value[12:self.opt_length].hex()
            obj = {"iaid":str(iaid), "t1":str(t1), "t2":str(t2), "iapd_options":iapd_options}

            self.values.append(obj)
        elif self.opt_type in [26]:
            # IA Prefix Option
            preferred_lifetime = int(self.opt_value[:4].hex(), 16)
            valid_lifetime = int(self.opt_value[4:8].hex(), 16)
            prefix_length = self.opt_value[8]
            prefix_bytes_size = prefix_length // 8
            ipv6_prefix = ":".join(findall(r'.{4}', self.opt_value[9:9+prefix_bytes_size].hex()))
            iaprefix_options = self.opt_value[25:self.opt_length].hex()
            obj = {"preferred_lifetime":str(preferred_lifetime),
                   "valid_lifetime":str(valid_lifetime),
                   "prefix_length":str(prefix_length),
                   "ipv6_prefix":ipv6_prefix,
                   "iaprefix_options":iaprefix_options}

            self.values.append(obj)
        elif self.opt_type in [32,46]:
            # Information Refresh Time
            # client-last-transaction-time
            self.values.append(str(int(self.opt_value[:self.opt_length], 16)))
        elif self.opt_type in [82,83]:
            # SOL_MAX_RT Option
            # INF_MAX_RT Option
            self.values.append(str(int(self.opt_value[:self.opt_length], 16)))
        elif self.opt_type in [44]:
            # Query Option
            QUERY_TYPES = {1:"QUERY_BY_ADDRESS", 2:"QUERY_BY_CLIENTID"}
            query_type = self.opt_value[0]
            link_address = format_ip2s(self.opt_value[1:17], 0x06)
            query_options = self.opt_value[17:self.opt_length].hex()
            obj = {"query_type":"%d - %s" % (query_type, QUERY_TYPES[query_type]),
                   "link_address":link_address,
                   "query_options":query_options}

            self.values.append(obj)
        elif self.opt_type in [47]:
            # Relay Data
            peer_address = format_ip2s(self.opt_value[:16], 0x06)
            dhcp_relay_message = self.opt_value[16:self.opt_length].hex()
            obj = {"peer_address":peer_address, "dhcp_relay_message":dhcp_relay_message}

            self.values.append(obj)
        elif self.opt_type in [48]:
            # Client Link
            n = self.opt_length // 16
            for idx in range(n):
                self.values.append(format_ip2s(self.opt_value[idx*16:(idx+1)*16], 0x06))
        elif self.opt_type in [39]:
            # https://www.rfc-editor.org/rfc/rfc4704 
            # Fully Qualified Domain Name (FQDN) Option
            flags = bin(self.opt_value[0])
            if self.opt_length > 1:
                domain_name = self.opt_value[1:self.opt_length].hex()
            else:
                domain_name = "(not given)"
            obj = {"flags_nos":str(flags), "domain_name":domain_name}
            self.values.append(obj)
