#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""
from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.utils import format_ip2s,convert_hex2ascii

__all__ = [
    'MDNSMessage',
]

# https://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml
"""
1	A	[RFC1035]
2	NS	[RFC1035]
3	MD	[RFC1035]
4	MF	[RFC1035]
5	CNAME	[RFC1035]
6	SOA	[RFC1035]
7	MB	[RFC1035]
8	MG	[RFC1035]
9	MR	[RFC1035]
10	NULL	[RFC1035]
11	WKS	[RFC1035]
12	PTR	[RFC1035]
13	HINFO	[RFC1035]
14	MINFO	[RFC1035]
15	MX	[RFC1035]
16	TXT	[RFC1035]
17	RP	[RFC1183]
18	AFSDB	[RFC1183][RFC5864]
19	X25	[RFC1183]
20	ISDN	[RFC1183]
21	RT	[RFC1183]
22	NSAP	[RFC1706][status-change-int-tlds-to-historic]
23	NSAP-PTR	[RFC1706][status-change-int-tlds-to-historic]
24	SIG	[RFC2536][RFC2931][RFC3110][RFC4034]
25	KEY	[RFC2536][RFC2539][RFC3110][RFC4034]
26	PX	[RFC2163]
27	GPOS	[RFC1712]
28	AAAA	[RFC3596]
29	LOC	[RFC1876]
30	NXT	[RFC2535][RFC3755]
31	EID	[Michael_Patton][http://ana-3.lcs.mit.edu/~jnc/nimrod/dns.txt]
32	NIMLOC	[1][Michael_Patton][http://ana-3.lcs.mit.edu/~jnc/nimrod/dns.txt]
33	SRV	[1][RFC2782]
34	ATMA    ATM Forum Technical Committee
35	NAPTR	[RFC3403]
36	KX	[RFC2230]
37	CERT	[RFC4398]
38	A6	[RFC2874][RFC3226][RFC6563]
39	DNAME	[RFC6672]
40	SINK	[Donald_E_Eastlake][draft-eastlake-kitchen-sink]
41	OPT	[RFC3225][RFC6891]
42	APL	[RFC3123]
43	DS	[RFC4034]
44	SSHFP	[RFC4255]
45	IPSECKEY	[RFC4025]
46	RRSIG	[RFC4034]
47	NSEC	[RFC4034][RFC9077]
48	DNSKEY	[RFC4034]
49	DHCID	[RFC4701]
50	NSEC3	[RFC5155][RFC9077]
51	NSEC3PARAM	[RFC5155]
52	TLSA	[RFC6698]
53	SMIMEA	[RFC8162]
54	Unassigned
55	HIP	[RFC8005]
56	NINFO	[Jim_Reid]
57	RKEY	[Jim_Reid]
58	TALINK	[Wouter_Wijngaards]
59	CDS	[RFC7344]
60	CDNSKEY	[RFC7344]
61	OPENPGPKEY	[RFC7929]
62	CSYNC	[RFC7477]
63	ZONEMD	[RFC8976]
64	SVCB	[RFC-ietf-dnsop-svcb-https-12]
65	HTTPS	[RFC-ietf-dnsop-svcb-https-12]
66-98	Unassigned
99	SPF	[RFC7208]
100	UINFO	[IANA-Reserved]
101	UID	[IANA-Reserved]
102	GID	[IANA-Reserved]
103	UNSPEC	[IANA-Reserved]
104	NID	[RFC6742]
105	L32	[RFC6742]
106	L64	[RFC6742]
107	LP	[RFC6742]
108	EUI48	[RFC7043]
109	EUI64	[RFC7043]
110-248	Unassigned
249	TKEY	[RFC2930]
250	TSIG	[RFC8945]
251	IXFR	[RFC1995]
252	AXFR	[RFC1035][RFC5936]
253	MAILB	[RFC1035]
254	MAILA	[RFC1035]
255	*	[RFC1035][RFC6895][RFC8482]
256	URI	[RFC7553]
257	CAA	[RFC8659]
258	AVC	[Wolfgang_Riedel]
259	DOA	[draft-durand-doa-over-dns]
260	AMTRELAY	[RFC8777]
261-32767	Unassigned
32768	TA	 Deploying DNSSEC Without a Signed Root.  Technical Report 1999-19,
32769	DLV	[RFC8749][RFC4431]
32770-65279	Unassigned
65280-65534	Private use
65535	Reserved
"""
QUERY_TYPE = {
    1      : "A (IPv4)",
    2      : "NS",
    3      : "MD",
    4      : "MF",
    5      : "CNAME",
    6      : "SOA",
    7      : "MB",
    8      : "MG",
    9      : "MR",
    10     : "NULL",
    11     : "WKS",
    12     : "PTR (IP to Domain)",
    13     : "HINFO",
    14     : "MINFO",
    15     : "MX",
    16     : "TXT",
    17     : "RP",
    18     : "AFSDB",
    19     : "X25",
    20     : "ISDN",
    21     : "RT",
    22     : "NSAP",
    23     : "NSAP-PTR",
    24     : "SIG",
    25     : "KEY",
    26     : "PX",
    27     : "GPOS",
    28     : "AAAA (IPv6)",
    29     : "LOC",
    30     : "NXT",
    31     : "EID",
    32     : "NIMLOC",
    33     : "SRV",
    34     : "ATMA",
    35     : "NAPTR",
    36     : "KX",
    37     : "CERT",
    38     : "A6",
    39     : "DNAME",
    40     : "SINK",
    41     : "OPT",
    42     : "APL",
    43     : "DS",
    44     : "SSHFP",
    45     : "IPSECKEY",
    46     : "RRSIG",
    47     : "NSEC",
    48     : "DNSKEY",
    49     : "DHCID",
    50     : "NSEC3",
    51     : "NSEC3PARAM",
    52     : "TLSA",
    53     : "SMIMEA",
    54     : "Unassigned",
    55     : "HIP",
    56     : "NINFO",
    57     : "RKEY",
    58     : "TALINK",
    59     : "CDS",
    60     : "CDNSKEY",
    61     : "OPENPGPKEY",
    62     : "CSYNC",
    63     : "ZONEMD",
    64     : "SVCB",
    65     : "HTTPS",
    99     : "SPF",
    100    : "UINFO",
    101    : "UID",
    102    : "GID",
    103    : "UNSPEC",
    104    : "NID",
    105    : "L32",
    106    : "L64",
    107    : "LP",
    108    : "EUI48",
    109    : "EUI64",
    249    : "TKEY",
    250    : "TSIG",
    251    : "IXFR",
    252    : "AXFR",
    253    : "MAILB",
    254    : "MAILA",
    255    : "ANY",
    256    : "URI",
    257    : "CAA",
    258    : "AVC",
    259    : "DOA",
    260    : "AMTRELAY",
    32768  : "TA",
    32769  : "DLV",
}

QUERY_CLASS = {
    0x01   :  "Class IN",
    0x03   :  "Class CH",
    0x04   :  "Class HS",
    0x8001 :  "Class IN",
}

class MDNSHeader(object):
    """
    Multicast DNS Header format:
    -----------------------------------------------------------------------------------------------
    | Transaction ID | Flags  | Question Count | Answer Count | Authority Count | Addtional Count |
    -----------------------------------------------------------------------------------------------
    |     2bytes     | 2bytes |    2bytes      |     2bytes   |      2bytes     |      2bytes     |   
    -----------------------------------------------------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc6762.txt
    """

    def __init__(self, transaction_id=0x00, flags=0x00, question_count=0x00, 
                 answer_count=0x00, authority_count=0x00, addition_count=0x00):
        """
        @transaction_id
        @flags:    QR(1bit)|Opcode(4bits)|AA(1bit)|TC(1bit)|RD(1bit)|RA(1bit)|Z(1bit)|AD(1bit)|CD(1bit)|RCODE(4bits)
                   QR        0 - query message  1 - response message
                   OPCODE    0 - standard query 1 - inverse query  2 - server status query
                   RCODE     0 - no error   1 - query format error  2 - server unavalable
                             3 - dn not exist  4 - query not be execute  5 - refuse query
        @question_count
        @answer_count
        @authority_count
        @addition_count
        """
        self.transaction_id = transaction_id
        self.flags = flags
        self.question_count = question_count
        self.answer_count = answer_count
        self.authority_count = authority_count
        self.addition_count = addition_count

    def format_flags_output(self, title="\033[32m Flags bits \033[0m", indent=0):
        """
        Format output flags message.
        Flags: QR(1bit)|Opcode(4bits)|AA(1bit)|TC(1bit)|RD(1bit)|RA(1bit)|Z(1bit)|AD(1bit)|CD(1bit)|RCODE(4bits)
        """
        return ""

    def format_outmsg(self, title="\033[32m MDNS Message Header \033[0m", indent=0):
        """
        Format output message.
        @title
        @indent
        """
        if not indent:
            prefix = ""
        else:
            prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "flags":
                attrs[attr] = bin(attrs[attr])

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))

        title_out = prefix + "|  %s  |\n" % title

        outmsg = title_out + \
                prefix + "-"*num_sep + "\n" + \
                prefix + head_out + "\n" + \
                prefix + "-"*num_sep + "\n" + \
                prefix + data_out + "\n" + \
                prefix + "-"*num_sep + "\n"

        return outmsg 

    def format_output(self, stdout, title="\033[32m MDNS Message Header\033[0m", indent=0):
        """
        Output mdns message header.
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))

class Question(object):
    """
    -----------------------------------------
    | Query Name | Query Type | Query Class |
    -----------------------------------------
    |    n       |    2bytes  |   2bytes    |
    -----------------------------------------
    Query Name: one or more LV, end with 00
    """
    def __init__(self, query_name="", query_type=0x00, query_class=0x00):
        """
        @query_name
        @query_type
        @query_class
        """
        self.query_name = query_name
        self.query_type = query_type
        self.query_class = query_class

    def format_outmsg(self, title="\033[32m Question \033[0m", indent=4):
        """
        Format output message.
        @title
        @indent
        """
        if not indent:
            prefix = ""
        else:
            prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "query_type":
                attrs[attr] = "%d-%s" % (attrs[attr], QUERY_TYPE[attrs[attr]])
            elif attr == "query_class":
                attrs[attr] = "%02x-%s" % (attrs[attr], QUERY_CLASS[attrs[attr]])

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))

        title_out = prefix + "|  %s  |\n" % title

        outmsg = title_out + \
                prefix + "-"*num_sep + "\n" + \
                prefix + head_out + "\n" + \
                prefix + "-"*num_sep + "\n" + \
                prefix + data_out + "\n" + \
                prefix + "-"*num_sep + "\n"

        return outmsg 

    def format_output(self, stdout, title="\033[32m Question \033[0m", indent=4):
        """
        Output message.
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))

class ResourceRecord(object):
    """
    Format:
    ----------------------------------------------------------------------
    | Domain Name | RR Type | RR Class | Life Time | RR Length | RR Data |
    ----------------------------------------------------------------------
    |    n        | 2bytes  | 2bytes   |  4bytes   |   2bytes  |    n    |
    ----------------------------------------------------------------------

    Domain Name: one or more LV, end with 00

    RR Data:  type=A and class=IN      --> data=IPv4
              type=AAAA and class=IN   --> data=IPv6
              type=PTR  and class=IN   --> data=domain name

    Reference: https://www.rfc-editor.org/rfc/rfc1035
    """

    def __init__(self, domain_name="", record_type=0x00, record_class=0x00,
                 life_time=0x00, record_length=0x00, record_data=[]):
        """
        @domain_class
        @record_type
        @record_class
        @life_time
        @record_length
        @record_data
        """
        self.domain_name = domain_name
        self.record_type = record_type
        self.record_class = record_class
        self.life_time = life_time
        self.record_length = record_length
        self.record_data = record_data

    def format_outmsg(self, title="\033[32m Resource Record \033[0m", indent=4):
        """
        Format output message.
        @title
        @indent
        """
        if not indent:
            prefix = ""
        else:
            prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "record_type":
                attrs[attr] = "%d-%s" % (attrs[attr], QUERY_TYPE[attrs[attr]])
            elif attr == "record_class":
                attrs[attr] = "%02x-%s" % (attrs[attr], QUERY_CLASS[attrs[attr]])
            elif attr == "record_data":
                if attrs[attr]:
                    if not isinstance(attrs[attr][0], dict):
                        attrs[attr] = attrs[attr][0]

        head_out = "|"
        data_out = "|"
        num_sep = 1
        for attr in attrs.keys():
            if attr == "record_data":
                if attrs[attr]:
                    if isinstance(attrs[attr][0], dict):
                        continue

            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))

        if self.record_data:
            if isinstance(self.record_data[0], dict):
                data_out += "\n" + prefix + "-"*num_sep + "\n"
                ext_num_sep = 0
                for rdata in self.record_data:
                    ext_head_out = "|"
                    ext_data_out = "|"
                    ext_num_sep = 1
                    for key,value in rdata.items():
                        if key == "signature":
                            continue

                        if len(key) > len(str(value)):
                            ext_width = len(key)
                        else:
                            ext_width = len(str(value))

                        ext_num_sep += ext_width+3
                        ext_head_out += " %-*s |" % (ext_width, key.replace('_', ' ').title())
                        ext_data_out += " %-*s |" % (ext_width, str(value))

                    data_out += prefix + "| Record Data Detail |\n" + \
                                prefix + "-"*ext_num_sep + "\n" + \
                                prefix + ext_head_out + "\n" + \
                                prefix + "-"*ext_num_sep + "\n" + \
                                prefix + ext_data_out + "\n" + \
                                prefix + "-"*ext_num_sep
                    if rdata.get("signature", None):
                        sign_length = len(rdata["signature"])
                        sign_num_sep = 100
                        n = sign_length//sign_num_sep if not sign_length%sign_num_sep else (sign_length//sign_num_sep+1)
                        data_out += "\n" + prefix + "| Signature Data |\n" + \
                                    prefix + "-"*sign_num_sep + "\n"
                        for idx in range(n):
                            data_out += prefix + rdata["signature"][idx*sign_num_sep:(idx+1)*sign_num_sep] + "\n"
                        data_out += prefix + "-"*sign_num_sep

        title_out = prefix + "|  %s  |\n" % title

        outmsg = title_out + \
                prefix + "-"*num_sep + "\n" + \
                prefix + head_out + "\n" + \
                prefix + "-"*num_sep + "\n" + \
                prefix + data_out + "\n" 

        if self.record_data:
            if isinstance(self.record_data[0], dict):
                return outmsg 

        outmsg += prefix + "-"*num_sep + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m Resource Record \033[0m", indent=4):
        """
        Output message.
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))

class AnswerResourceRecord(ResourceRecord):
    pass

class AuthorityResourceRecord(ResourceRecord):
    pass

class AdditionalResourceRecord(ResourceRecord):
    pass

class MDNSMessage(object):
    """
    ------------------------------------------------------------------------------------------------------
    | MDNSHeader | Question | AnswerResourceRecord | AuthorityResourceRecord | AadditionResourceRecord |
    ------------------------------------------------------------------------------------------------------
    """
    def __init__(self, mdns_header=None, question=[], answer=[], authority=[], addition=[]):
        """
        @mdns_header
        @question: list object of Question
        @answer: list object of AnswerResourceRecord
        @anthority : list object of AuthorityResourceRecord
        @addition: list object of AdditionalResourceRecord
        """
        self.mdns_header = mdns_header
        self.question = question
        self.answer = answer
        self.authority = authority
        self.addition = addition

    @classmethod
    def create(cls, mdns_data):
        """
        Create object of MDNSMessage.
        @mdns_data
        """
        if not mdns_data or len(mdns_data) < 12:
            raise ParamsError()

        pos = 0

        mdnshdr = MDNSHeader(transaction_id=int(mdns_data[:2].hex(), 16), 
                             flags=int(mdns_data[2:4].hex(), 16), 
                             question_count=int(mdns_data[4:6].hex(), 16), 
                             answer_count=int(mdns_data[6:8].hex(), 16), 
                             authority_count=int(mdns_data[8:10].hex(), 16), 
                             addition_count=int(mdns_data[10:12].hex(), 16))
        pos += 12
        area_data = mdns_data[pos:]

        questions = []
        if mdnshdr.question_count:
            for idx in range(mdnshdr.question_count):
                names = []
                local = True
                while True:
                    flag = area_data[0]
                    if local:
                        pos += 1
                    if flag & 0xC0 == 0xC0:
                        # domain name pointer
                        point_pos = int(area_data[:2].hex(), 16) & 0x3FFF
                        if local:
                            pos += 1
                        area_data = mdns_data[point_pos:]
                        local = False
                        continue
                    elif flag != 0x00:
                        length = flag
                        names.append(convert_hex2ascii(area_data[1:1+length].hex()))
                        area_data = area_data[1+length:]
                        if local:
                            pos += length
                        continue
                    else:
                        break
                
                area_data = mdns_data[pos:]
                domain_name = ".".join(names)
                query_type = int(area_data[:2].hex(), 16)
                query_class = int(area_data[2:4].hex(), 16)

                questions.append(Question(query_name=domain_name, query_type=query_type, query_class=query_class))

                pos += 4
                area_data = mdns_data[pos:]

        answers = []
        if mdnshdr.answer_count:
            for idx in range(mdnshdr.answer_count):
                names = []
                local = True
                while True:
                    flag = area_data[0]
                    if local:
                        pos += 1
                    if flag & 0xC0 == 0xC0:
                        # domain name pointer
                        point_pos = int(area_data[:2].hex(), 16) & 0x3FFF
                        if local:
                            pos += 1
                        area_data = mdns_data[point_pos:]
                        local = False
                        continue
                    elif flag != 0x00:
                        length = flag
                        names.append(convert_hex2ascii(area_data[1:1+length].hex()))
                        area_data = area_data[1+length:]
                        if local:
                            pos += length
                        continue
                    else:
                        break
                
                area_data = mdns_data[pos:]
                domain_name = ".".join(names)
                record_type = int(area_data[:2].hex(), 16)
                record_class = int(area_data[2:4].hex(), 16)
                life_time = int(area_data[4:8].hex(), 16)
                record_length = int(area_data[8:10].hex(), 16)
                record_data = parse_resource_record_data(record_type, record_class, 
                                        area_data[10:10+record_length], mdns_data=mdns_data)

                answers.append(AnswerResourceRecord(domain_name=domain_name, record_type=record_type, 
                                                   record_class=record_class, life_time=life_time,
                                                   record_length=record_length, record_data=record_data))

                pos += 10+record_length
                area_data = mdns_data[pos:]

        authorities = []
        if mdnshdr.authority_count:
            for idx in range(mdnshdr.authority_count):
                names = []
                local = True
                while True:
                    flag = area_data[0]
                    if local:
                        pos += 1
                    if flag & 0xC0 == 0xC0:
                        # domain name pointer
                        point_pos = int(area_data[:2].hex(), 16) & 0x3FFF
                        if local:
                            pos += 1
                        area_data = mdns_data[point_pos:]
                        local = False
                        continue
                    elif flag != 0x00:
                        length = flag
                        names.append(convert_hex2ascii(area_data[1:1+length].hex()))
                        area_data = area_data[1+length:]
                        if local:
                            pos += length
                        continue
                    else:
                        break
                
                area_data = mdns_data[pos:]
                domain_name = ".".join(names)
                record_type = int(area_data[:2].hex(), 16)
                record_class = int(area_data[2:4].hex(), 16)
                life_time = int(area_data[4:8].hex(), 16)
                record_length = int(area_data[8:10].hex(), 16)
                record_data = parse_resource_record_data(record_type, record_class, 
                                        area_data[10:10+record_length], mdns_data=mdns_data)

                authorities.append(AuthorityResourceRecord(domain_name=domain_name, record_type=record_type, 
                                                      record_class=record_class, life_time=life_time,
                                                      record_length=record_length, record_data=record_data))

                pos += 10+record_length
                area_data = mdns_data[pos:]

        additions = []
        if mdnshdr.addition_count:
            for idx in range(mdnshdr.addition_count):
                names = []
                local = True
                while True:
                    flag = area_data[0]
                    if local:
                        pos += 1
                    if flag & 0xC0 == 0xC0:
                        # domain name pointer
                        point_pos = int(area_data[:2].hex(), 16) & 0x3FFF
                        if local:
                            pos += 1
                        area_data = mdns_data[point_pos:]
                        local = False
                        continue
                    elif flag != 0x00:
                        length = flag
                        names.append(convert_hex2ascii(area_data[1:1+length].hex()))
                        area_data = area_data[1+length:]
                        if local:
                            pos += length
                        continue
                    else:
                        break
                
                area_data = mdns_data[pos:]
                domain_name = ".".join(names)
                record_type = int(area_data[:2].hex(), 16)
                record_class = int(area_data[2:4].hex(), 16)
                life_time = int(area_data[4:8].hex(), 16)
                record_length = int(area_data[8:10].hex(), 16)
                record_data = parse_resource_record_data(record_type, record_class, 
                                        area_data[10:10+record_length], mdns_data=mdns_data)

                additions.append(AdditionalResourceRecord(domain_name=domain_name, record_type=record_type, 
                                                       record_class=record_class, life_time=life_time,
                                                       record_length=record_length, record_data=record_data))

                pos += 10+record_length
                area_data = mdns_data[pos:]
                
        mdnsmsg = cls(mdns_header = mdnshdr,
                      question = questions,
                      answer = answers,
                      authority = authorities,
                      addition = additions)
        return mdnsmsg

    def format_output(self, stdout, title="\033[32m Multicast DNS Message \033[0m", indent=0):
        """
        Output format message.
        @stdout
        @title
        @indent
        """
        if self.mdns_header:
            self.mdns_header.format_output(stdout, title="\033[32m Multicast DNS Message Header \033[0m", indent=indent)

        if self.question:
            count = 0
            for question in self.question:
                question.format_output(stdout, title="\033[32m Question [%d] \033[0m"%count, indent=indent)
                count += 1

        if self.answer:
            count = 0
            for answer in self.answer:
                answer.format_output(stdout, title="\033[32m Answer RR [%d] \033[0m"%count, indent=indent)
                count += 1

        if self.authority:
            count = 0
            for authority in self.authority:
                authority.format_output(stdout, title="\033[32m Authority RR [%d] \033[0m"%count, indent=indent)
                count += 1

        if self.addition:
            count = 0
            for addition in self.addition:
                addition.format_output(stdout, title="\033[32m Additional RR [%d] \033[0m"%count, indent=indent)
                count += 1

def parse_resource_record_data(record_type, record_class, record_data, mdns_data=b''):
    """
    Parse the data in resource record.
    @mdns_data
    @record_type
    @record_class
    """
    if not record_type or not record_class or not record_data:
        raise ParamsError()

    if record_type == 0x01:
        # A IPv4
        # https://www.rfc-editor.org/rfc/rfc1035
        return [format_ip2s(record_data, 0x04)]
    elif record_type in [0x02, 0x05, 0x0c]:
        # https://www.rfc-editor.org/rfc/rfc1035
        # 0x0c  PTR  IP to domain name
        # 0x05  CNAME
        # 0x02  NSDNAME
        names = []
        area_data = record_data
        while True:
            flag = area_data[0]
            if flag & 0xC0 == 0xC0:
                # domain name pointer
                point_pos = int(area_data[:2].hex(), 16) & 0x3FFF
                area_data = mdns_data[point_pos:]
                continue
            elif flag != 0x00:
                length = flag
                names.append(convert_hex2ascii(area_data[1:1+length].hex()))
                area_data = area_data[1+length:]
                continue
            else:
                break
        return [".".join(names)]
    elif record_type == 0x1c:
        # https://www.rfc-editor.org/rfc/rfc3596
        # AAAA IPv6
        return [format_ip2s(record_data, 0x06)]
    elif record_type == 0x10:
        # https://www.rfc-editor.org/rfc/rfc1035
        # TXT
        return [convert_hex2ascii(record_data.hex())]
    elif record_type == 0x2e:
        # https://www.rfc-editor.org/rfc/rfc4034
        # RRSIG Resource Record
        type_covered = int(record_data[:2].hex(), 16)
        algorithm = record_data[2]
        labels = record_data[3]
        original_TTL  = int(record_data[4:8].hex(), 16)
        sign_expiration = int(record_data[8:12].hex(), 16)
        sign_inception = int(record_data[12:16].hex(), 16)
        key_tag = record_data[16:18].hex()

        names = []
        area_data = record_data[18:]
        pos = 0
        while True:
            flag = area_data[0]
            pos += 1
            if flag != 0x00:
                length = flag
                names.append(convert_hex2ascii(area_data[1:1+length].hex()))
                pos += length
                area_data = area_data[1+length:]
            else:
                # end
                break
        signer_name = ".".join(names)
        signature = record_data[18+pos:].hex()

        return [{"type_covered":str(type_covered),
                 "algorithm":str(algorithm),
                 "labels":str(labels),
                 "original_TTL":str(original_TTL),
                 "sign_expiration":str(sign_expiration),
                 "sign_inception":str(sign_inception),
                 "key_tag":key_tag,
                 "signer_name":signer_name,
                 "signature":signature}]
    elif record_type == 0x06:
        # SOA
        # https://www.rfc-editor.org/rfc/rfc1035#section-3.3.13
        names = []
        area_data = record_data
        pos = 0
        remote = False
        deep = 1
        while True:
            flag = area_data[0]
            if not remote:
                pos += 1
            if flag & 0xC0 == 0xC0:
                # domain name pointer
                point_pos = int(area_data[:2].hex(), 16) & 0x3FFF
                area_data = mdns_data[point_pos:]
                remote = True
                if deep == 1:
                    pos += 1
                deep += 1
                continue
            elif flag != 0x00:
                length = flag
                names.append(convert_hex2ascii(area_data[1:1+length].hex()))
                if not remote:
                    pos += length
                area_data = area_data[1+length:]
            else:
                # end
                break
        mname = ".".join(names)

        names.clear()
        area_data = record_data[pos:]
        remote = False
        deep = 1
        while True:
            flag = area_data[0]
            if not remote:
                pos += 1
            if flag & 0xC0 == 0xC0:
                # domain name pointer
                point_pos = int(area_data[:2].hex(), 16) & 0x3FFF
                area_data = mdns_data[point_pos:]
                remote = True
                if deep == 1:
                    pos += 1
                deep += 1
                continue
            elif flag != 0x00:
                length = flag
                names.append(convert_hex2ascii(area_data[1:1+length].hex()))
                if not remote:
                    pos += length
                area_data = area_data[1+length:]
            else:
                # end
                break
        rname = ".".join(names)
        area_data = record_data[pos:]

        serial = int(area_data[:4].hex(), 16)
        refresh = int(area_data[4:8].hex(), 16)
        retry = int(area_data[8:12].hex(), 16)
        expire = int(area_data[12:16].hex(), 16)
        minimum = int(area_data[16:20].hex(), 16)

        return [{"mname":mname,
                 "rname":rname,
                 "serial":str(serial),
                 "refresh":str(refresh),
                 "retry":str(retry),
                 "expore":str(expire),
                 "minimum":str(minimum)}]
    elif record_type == 0x21:
        # https://www.rfc-editor.org/rfc/rfc2782
        # 0x21 SRV Record
        # Priority(2bytes) Weight(2bytes) Port(2bytes) Target
        priority = int(record_data[:2].hex(), 16)
        weight = int(record_data[2:4].hex(), 16)
        port = int(record_data[4:6].hex(), 16)

        names = []
        area_data = record_data[6:]
        while True:
            flag = area_data[0]
            if flag & 0xC0 == 0xC0:
                # domain name pointer
                point_pos = int(area_data[:2].hex(), 16) & 0x3FFF
                area_data = mdns_data[point_pos:]
                continue
            elif flag != 0x00:
                length = flag
                names.append(convert_hex2ascii(area_data[1:1+length].hex()))
                area_data = area_data[1+length:]
                continue
            else:
                break
        target = ".".join(names)
        return [{'Priority':str(priority), 'Weight':str(weight), 'Port':str(port), 'Target':target}]
    else:
        return []

