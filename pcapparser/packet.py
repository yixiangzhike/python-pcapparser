#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.error import ParamsError
from pcapparser.utils import format_datetime
from pcapparser.constants import FRAME_IEEE802_LLC_HDR_LEN,PACKET_HDR_LEN,BPDU_MAC
from pcapparser.etherframe import EtherFrameHeader
from pcapparser.protocol import *

__all__ = [
    'PacketHeader',
    'PacketBody',
]

class PacketHeader(object):
    """
    The packet header data is coded by byteorder.
    If 'little', calculate data from right to left.
    If 'big', calculate data from lefto to right.

    packet header format:
    ---------------------------------------------------------
    fields | timestamp(s) | timestamp(us) | caplen | length |
    ---------------------------------------------------------
    bytes  |      4       |       4       |    4   |    4   |
    ---------------------------------------------------------
    """
    def __init__(self, timestamp_s=0x00, timestamp_us=0x00, caplen=0x00, length=0x00, byteorder="little"):
        """
        @timestamp_s     : timestamp seconds
        @timestamp_us    : timestamp microseconds
        @caplen          : data frame length
        @length          : offline data length, <= caplen
        @byteorder       : little or big
        """
        self.timestamp_s = timestamp_s
        self.timestamp_us = timestamp_us
        self.caplen = caplen
        self.length = length
        self.byteorder = byteorder

    @classmethod
    def get_pkt_length(cls, header_data, size=PACKET_HDR_LEN, byteorder="little"):
        """
        Get packet length from header data
        @header_data  : the data of packet header
        @size         : the length of packet header, unused current
        @byteorder    : little or big
        """
        if not header_data or len(header_data) < PACKET_HDR_LEN:
            raise ParamsError()

        if not isinstance(header_data, bytes):
            raise ParamsError()

        pktlength = int.from_bytes(header_data[12:16], byteorder)

        return pktlength


    @classmethod
    def create(cls, header_data, size=PACKET_HDR_LEN, byteorder="little"):
        """
        Create PacketHeader object
        @header_data  : the data of packet header
        @size         : the length of packet header
        @byteorder    : little or big
        """
        if not header_data or size < PACKET_HDR_LEN:
            raise ParamsError()

        if not isinstance(header_data, bytes):
            raise ParamsError()

        pkthdr = cls(timestamp_s=int.from_bytes(header_data[:4], byteorder),
                     timestamp_us=int.from_bytes(header_data[4:8], byteorder),
                     caplen=int.from_bytes(header_data[8:12], byteorder),
                     length=int.from_bytes(header_data[12:16], byteorder))

        return pkthdr

    def format_outmsg(self, title="\033[32m Packet Header \033[0m", indent=4):
        """
        Format output message.
        @title
        @indent
        """
        if not indent:
            prefix = ""
        else:
            prefix = "%*s" % (indent, " ")
        fmt = "| %-19s | %-13s | %-8s | %-8s | %-4s |"
        head_out = prefix + fmt % ("Timestamp(s)", "Timestamp(us)", "Cap Len", "Length", "    ")
        hex_data_out = prefix + fmt % ("%08X" % self.timestamp_s,
                                       "%08X" % self.timestamp_us,
                                       "%08X" % self.caplen,
                                       "%08X" % self.length, "HEX")
        data_out = prefix + fmt % (format_datetime(self.timestamp_s),
                                   str(self.timestamp_us),
                                   str(self.caplen), str(self.length), "DEC")
        sep_num = 68

        outmsg = (prefix + "\n|>>>    %s    |" % title) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (head_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (hex_data_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (data_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n"

        return outmsg
        
    def format_output(self, stdout, title="\033[32m Packet Header \033[0m", indent=4):
        """
        Output message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))


class PacketBody(object):
    """
    packet body format:
    ----------------------------------------------------------------------------------------------------
    |  Ether Frame Header  | Net Header (IP/ARP/STP) |  Trans Header (UDP/TCP/ICMP) | App Data   | crc
    ----------------------------------------------------------------------------------------------------
    """
    def __init__(self, frame_hdr=None, net_hdr=None, trans_hdr=None, app_data=None, crc=0x00):
        """
        @frame_hdr
        @net_hdr
        @trans_hdr
        @app_data
        @crc      : checksum of ether frame data
        """
        self.frame_hdr = frame_hdr
        self.net_hdr = net_hdr
        self.trans_hdr = trans_hdr
        self.app_data = app_data

    @classmethod
    def create(cls, data, size=0x00):
        """
        Create PacketBody object
        @data
        @size
        """
        if not data:
            raise ParamsError("Param data can't be None")

        packet_length = len(data)

        frmhdr = None
        net_hdr = None
        trans_hdr = None
        # create frame header object
        frmhdr_len = EtherFrameHeader.get_header_length(data[:FRAME_IEEE802_LLC_HDR_LEN])
        frmhdr = EtherFrameHeader.create(data, size=frmhdr_len)

        posoffset = frmhdr_len

        if frmhdr.ssap == 0x42 and frmhdr.dsap == 0x42:
            # STP protocol frame
            stp_ver = get_version_from_stp_message(data[frmhdr_len:frmhdr_len+3])
            if stp_ver == 0:
                stpmsg = STPMessage.create(data[frmhdr_len:], size=35)
            elif stp_ver == 2:
                stpmsg = RSTPMessage.create(data[frmhdr_len:], size=35)
            elif stp_ver == 3:
                stpmsg = MSTPMessage.create(data[frmhdr_len:], size=35)

            net_hdr = stpmsg
            posoffset += len(data[frmhdr_len:])
        else:
            if frmhdr.etype == 0x0800:
                # IPv4 protocol frame
                net_hdr = []
                while True:
                    ipv4hdr_len = IPv4FrameHeader.get_header_length(data[posoffset:posoffset+1])
                    ipv4hdr = IPv4FrameHeader.create(data[posoffset:posoffset+ipv4hdr_len], size=ipv4hdr_len)
                    ipv4_total_len = ipv4hdr.get_ipv4_total_length()
                    tmp_hdr = ipv4hdr

                    posoffset += ipv4hdr_len

                    if not ipv4hdr.protocol in [4, 41]:
                        net_hdr = tmp_hdr
                        break
                    else:
                        # Ipv4 in Ip
                        # https://www.rfc-editor.org/rfc/rfc2003.txt
                        net_hdr.append(tmp_hdr)
                        # Ipv6 in Ip
                        # https://www.rfc-editor.org/rfc/rfc3056.txt
                        if ipv4hdr.protocol == 41:
                            posoffset, ipv6_hdr, trans_hdr = switch_ipv6_packet(data, posoffset)
                            net_hdr.append(ipv6_hdr)

                            # remove padding data
                            appdata = data[posoffset:]
                            if not appdata.hex().strip("0"):
                                appdata = b''

                            pktbody = cls(frame_hdr=frmhdr, net_hdr=net_hdr, 
                                          trans_hdr=trans_hdr, app_data=appdata)
                            return pktbody

                if ipv4hdr.protocol == 6:
                    # TCP protocol frame
                    tcp_hdr_len = TCPFrameHeader.get_header_length(data[posoffset:posoffset+20])
                    tcphdr = TCPFrameHeader.create(data[posoffset:posoffset+tcp_hdr_len], size=tcp_hdr_len)
                    trans_hdr = tcphdr
                    posoffset += tcp_hdr_len
                elif ipv4hdr.protocol == 17:
                    # UDP protocol frame
                    udp_hdr_len = 8
                    udphdr = UDPFrameHeader.create(data[posoffset:posoffset+udp_hdr_len], size=udp_hdr_len)
                    trans_hdr = udphdr
                    posoffset += udp_hdr_len
                elif ipv4hdr.protocol == 112:
                    # VRRP protocol
                    vrrp_data = data[posoffset:posoffset-ipv4hdr_len+ipv4_total_len]
                    vrrp = VRRPMessage.create(vrrp_data, 4)
                    trans_hdr = vrrp
                    posoffset += len(vrrp_data)
                elif ipv4hdr.protocol == 2:
                    # IGMP protocol
                    igmp_data = data[posoffset:posoffset-ipv4hdr_len+ipv4_total_len]
                    igmp_type = get_igmp_type(igmp_data[:1])
                    igmp_length = len(igmp_data)
                    if igmp_length == 8:
                        if igmp_type in [0x11,0x12,0x16,0x17]:
                            igmp = IGMPMessage.create(igmp_data[:8])
                        else:
                            raise ParamsError()
                    elif  igmp_length > 8:
                        if igmp_type == 0x11:
                            igmp = IGMPv3QueryMessage(igmp_type=0x11, 
                                                     max_resp_time=int(igmp_data[1:2].hex(), 16), 
                                                     checksum=int(igmp_data[2:4].hex(), 16), 
                                                     group_address=igmp_data[4:8], 
                                                     ext_data=igmp_data[8:])
                            igmp.parse_extension_data()
                        elif igmp_type == 0x22:
                            igmp = IGMPv3ReportMessage(igmp_type=0x22, 
                                                      reserved1=int(igmp_data[1:2].hex(), 16), 
                                                      checksum=int(igmp_data[2:4].hex(), 16),
                                                      reserved2=int(igmp_data[4:6].hex(), 16),
                                                      gr_num=int(igmp_data[6:8].hex(), 16),
                                                      gr_data=igmp_data[8:])
                            igmp.parse_group_record_data()

                    trans_hdr = igmp
                    posoffset += igmp_length
                elif ipv4hdr.protocol == 1:
                    # ICMPV4
                    icmpv4_data = data[posoffset:posoffset-ipv4hdr_len+ipv4_total_len]
                    icmpv4_type, icmpv4_code = get_message_type_and_code_icmpv4(icmpv4_data[:2])
                    icmpv4message = None
                    if icmpv4_type == 0x03:
                        # ICMPv4 Destination Unreachable Message 
                        icmpv4message = ICMPv4DestUnreachableMessage(mtype=icmpv4_type, code=icmpv4_code, 
                                checksum=int(icmpv4_data[2:4].hex(), 16), message_body=icmpv4_data[4:])
                    elif icmpv4_type in [0x00,0x08]:
                        # ICMPv4 Echo request/reply Message 
                        icmpv4message = ICMPv4EchoMessage(mtype=icmpv4_type, code=icmpv4_code, 
                                checksum=int(icmpv4_data[2:4].hex(), 16), message_body=icmpv4_data[4:])
                    elif icmpv4_type == 0x04:
                        # ICMPv4 Source Quench Message 
                        icmpv4message = ICMPv4SourceQuenchMessage(mtype=icmpv4_type, code=icmpv4_code, 
                                checksum=int(icmpv4_data[2:4].hex(), 16), message_body=icmpv4_data[4:])
                    elif icmpv4_type == 0x05:
                        # ICMPv4 Redirect Message 
                        icmpv4message = ICMPv4RedirectMessage(mtype=icmpv4_type, code=icmpv4_code, 
                                checksum=int(icmpv4_data[2:4].hex(), 16), message_body=icmpv4_data[4:])
                    elif icmpv4_type == 0x0b:
                        # ICMPv4 TimeOut Message 
                        icmpv4message = ICMPv4TimeOutMessage(mtype=icmpv4_type, code=icmpv4_code, 
                                checksum=int(icmpv4_data[2:4].hex(), 16), message_body=icmpv4_data[4:])
                    elif icmpv4_type == 0x0c:
                        # ICMPv4 Parameter Problem Message 
                        icmpv4message = ICMPv4ParamProblemMessage(mtype=icmpv4_type, code=icmpv4_code, 
                                checksum=int(icmpv4_data[2:4].hex(), 16), message_body=icmpv4_data[4:])
                    elif icmpv4_type in [0x0d,0x0e]:
                        # ICMPv4 Timestamp request/reply Message 
                        icmpv4message = ICMPv4TimestampMessage(mtype=icmpv4_type, code=icmpv4_code, 
                                checksum=int(icmpv4_data[2:4].hex(), 16), message_body=icmpv4_data[4:])
                    elif icmpv4_type in [0x0f,0x10]:
                        # ICMPv4 Information request/reply Message 
                        icmpv4message = ICMPv4InformationMessage(mtype=icmpv4_type, code=icmpv4_code, 
                                checksum=int(icmpv4_data[2:4].hex(), 16), message_body=icmpv4_data[4:])

                    if icmpv4message:
                        icmpv4message.parse_message_body()    
                        trans_hdr = icmpv4message

                    posoffset += len(icmpv4_data)
            elif frmhdr.etype in [0x0806, 0x8035]:
                # ARP/RARP protocol frame
                arpframe = ARPFrame.create(data[posoffset:posoffset+ARP_FRAME_LEN], size=ARP_FRAME_LEN)
                net_hdr = arpframe

                posoffset += ARP_FRAME_LEN
            elif frmhdr.etype == 0x86dd:
                # IPv6 protocol frame
                posoffset, net_hdr, trans_hdr = switch_ipv6_packet(data, posoffset)

        # remove padding data
        appdata = data[posoffset:]
        if not appdata.hex().strip("0"):
            appdata = b''

        pktbody = cls(frame_hdr=frmhdr, net_hdr=net_hdr, trans_hdr=trans_hdr, app_data=appdata)

        return pktbody

    def format_output(self, stdout, indent=4):
        if self.frame_hdr:
            self.frame_hdr.format_output(stdout, indent=indent)
        if self.net_hdr:
            if isinstance(self.net_hdr, dict):
                for net_hdr in self.net_hdr:
                    net_hdr.format_output(stdout, indent=indent+4)
            else:
                self.net_hdr.format_output(stdout, indent=indent+4)
        if self.trans_hdr:
            self.trans_hdr.format_output(stdout, indent=indent+8)

    def format_app_data(self, stdout, indent=4):
        if self.app_data:
            if indent:
                prefix = "%*s" % (indent, " ")
            else:
                prefix = ""
            num_sep = 100
            data_hex = self.app_data.hex()
            length = len(data_hex)
            out = prefix + "|\033[32m >>> Application Data \033[0m|" + "\n" 
            out += prefix + "-"*num_sep + "\n"
            n = (length//100 + 1) if length%100 else (length//100)
            for idx in range(n):
                out += prefix + data_hex[idx*num_sep:(idx+1)*num_sep] + "\n"
            out += prefix + "-"*num_sep + "\n"

            stdout.write(out)


def switch_ipv6_packet(packet, offset):
    """
    @packet : full packet data
    @offset : begin position of ipv6 frame
    """

    data = packet
    posoffset = offset

    ipv6hdr_payload_len = IPv6FrameHeader.get_payload_length(data[posoffset:posoffset+IPV6_HDR_FIX_LEN])
    ipv6hdr = IPv6FrameHeader.create(data[posoffset:posoffset+ipv6hdr_payload_len+IPV6_HDR_FIX_LEN], size=ipv6hdr_payload_len+IPV6_HDR_FIX_LEN)
    net_hdr = ipv6hdr

    offset = ipv6hdr.get_uplayer_offset()

    posoffset += offset

    if not ipv6hdr.ext_headers:
        next_hdr = ipv6hdr.next_header
    else:
        next_hdr = ipv6hdr.ext_headers[len(ipv6hdr.ext_headers) - 1].next_header

    if next_hdr == 6:
        # TCP protocol frame
        tcp_hdr_len = TCPFrameHeader.get_header_length(data[posoffset:posoffset+20])
        tcphdr = TCPFrameHeader.create(data[posoffset:posoffset+tcp_hdr_len], size=tcp_hdr_len)
        trans_hdr = tcphdr

        posoffset += tcp_hdr_len
    elif next_hdr == 17:
        # UDP protocol frame
        udp_hdr_len = 8
        udphdr = UDPFrameHeader.create(data[posoffset:posoffset+udp_hdr_len], size=udp_hdr_len)
        trans_hdr = udphdr

        posoffset += udp_hdr_len
    elif next_hdr == 112:
        # VRRP protocol
        vrrp_data = data[posoffset:]
        vrrp = VRRPMessage.create(vrrp_data, 6)
        trans_hdr = vrrp
        posoffset += len(vrrp_data)
    elif next_hdr == 58:
        # ICMPv6
        icmpv6_data = data[posoffset:]
        icmpv6_type = icmpv6_data[0]
        icmpv6message = None
        if icmpv6_type == 0x01:
            # ICMPv6 Destination Unreachable Message 
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6DestUnreachableMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type == 0x02:
            # ICMPv6 Packet Too Big Message 
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6PacketTooBigMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type == 0x03:
            # ICMPv6 Time Exceeded Message
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6TimeOutMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type == 0x04:
            # ICMPv6 Parameter Problem Message
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6ParamProblemMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type in [0x80,0x81]:
            # ICMPv6 Echo Request/Respone Message
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6EchoMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type in [0x82,0x83,0x84]:
            # ICMPv6 MLD Query/Report/Done Message 
            icmpv6_code = icmpv6_data[1]
            icmpv6message = MLDv2QRDMessage(mld_type=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), 
                    max_resp_delay=int(icmpv6_data[4:6].hex(), 16),
                    reserved=int(icmpv6_data[6:8].hex(), 16), 
                    multicast_address=icmpv6_data[8:24], 
                    ext_data=icmpv6_data[24:])
        elif icmpv6_type == 0x85:
            # ICMPv6 Router Solicitation Message
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6RSMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type == 0x86:
            # ICMPv6 Router Advertisement Message
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6RAMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type == 0x87:
            # ICMPv6 Neighbor Solicitation Message
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6NSMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type == 0x88:
            # ICMPv6 Neighbor Advertisement Message
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6NAMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type == 0x89:
            # ICMPv6 Redirect Message
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6RedirectMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type in [0x8d, 0x8e]:
            # ICMPv6 Inverse Neighbor Discovery Solicitation/Advertisement Message
            icmpv6_code = icmpv6_data[1]
            icmpv6message = ICMPv6INDMessage(mtype=icmpv6_type, code=icmpv6_code, 
                    checksum=int(icmpv6_data[2:4].hex(), 16), message_body=icmpv6_data[4:])
        elif icmpv6_type == 0x8f:
            # ICMPv6 MLDv2 Report Message
            icmpv6message = MLDv2ReportMessage(mld_type=icmpv6_type, reserved1=icmpv6_data[1],
                    checksum=int(icmpv6_data[2:4].hex(), 16), reserved2=int(icmpv6_data[4:6].hex(), 16),
                    mar_num=int(icmpv6_data[6:8].hex(), 16), mar_data=icmpv6_data[8:])

        if icmpv6message:
            icmpv6message.parse_message_body()
            trans_hdr = icmpv6message

        posoffset += len(icmpv6_data)

    return (posoffset, net_hdr, trans_hdr)
