#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.constants import STP_MESSAGE_FIX_LEN,MSTP_MESSAGE_FIX_LEN,STP_VERSIONS
from pcapparser.utils import conv_format_mac

__all__ = ['get_version_from_stp_message', 'STPMessage', 'RSTPMessage', 'MSTPMessage']

def get_version_from_stp_message(message_data, size=STP_MESSAGE_FIX_LEN):
    """
    Get version from stp message data.
    Version: STP=0, RSTP=2, MSTP=3
    @message_data : Include the first 3 bytes in STP message data at least
    @size         : >= 3, current unused
    """
    if not message_data or len(message_data) < 3:
        raise ParamsError()

    if not isinstance(message_data, bytes):
        raise ParamsError()

    return message_data[2]


class STPMessage(object):
    """
    STP Message format:
    --------------------------------
    | Protocol Identifier | 2bytes |
    --------------------------------
    | Protocol Version    | 1byte  |
    --------------------------------
    | BPDU Type           | 1byte  |
    --------------------------------
    | Flags               | 1byte  |
    --------------------------------
    | Root Identifier     | 8bytes |
    --------------------------------
    | Root Path Cost      | 4bytes |
    --------------------------------
    | Bridge Identifier   | 8bytes |
    --------------------------------
    | Port Identifier     | 2bytes |
    --------------------------------
    | Message Age         | 2bytes |
    --------------------------------
    | Max Age             | 2bytes |
    --------------------------------
    | Hello Time          | 2bytes |
    --------------------------------
    | Forward Delay       | 2bytes |
    --------------------------------

    The format for Root Identifier and Bridge Identifier:
    4bit(high) + 12bit + 6bytes
    4bit  : Priority
    12bit : Bridge System ID extension
    6bytes: MAC

    """

    BPDU_TYPES = {0x00:'Configuration', 0x80:'STP TCN BPDU'}

    def __init__(self, identifier=0x00, version=0x00, bpdu_type=0x00, flags=0x00,
                 root_ident=b'', root_path_cost=0x00, bridge_ident=b'', port_ident=0x00, 
                 message_age=0x00, max_age=0x00, hello_time=0x00, forward_delay=0x00, version1_length=0x00):
        """
        @identifier     : Protocol identifier, STP=0
        @version        : Protocol version 
                          STP=0, RSTP=2, MSTP=3
        @bpdu_type      : BPDU type
                          0x00:STP Configuration BPDU
                          0x02:RSTP BPDU / MSTP BPDU
                          0x80:STP TCP BPDU
        @flags          : b7:TCA  b0:TC
        @root_ident     : Root bridge ID, first 2 bytes bridge priority, and follow 6 bytes MAC
        @root_path_cost : Root path root_path_cost, the time from this port to root bridge
        @bridge_ident   : Sender bridge identifier, first 2 bytes bridge priority, and follow 6 bytes MAC
        @port_ident     : Sender port identifier
        @message_age    : Message age
        @max_age        : Max age
        @hello_time     : The time bettwen two neighboring BPDU message
        @forward_delay  : The time for controling listening and learning state
        @version1_length: Only exist in RSTP and MSTP message
        """
        self.identifier = identifier
        self.version = version
        self.bpdu_type = bpdu_type
        self.flags = flags
        self.root_ident = root_ident
        self.root_path_cost = root_path_cost
        self.bridge_ident = bridge_ident
        self.prot_ident = port_ident
        self.message_age = message_age
        self.max_age = max_age
        self.hello_time = hello_time
        self.forward_delay = forward_delay
        self.version1_length = version1_length

    @classmethod
    def create(cls, message_data, size=STP_MESSAGE_FIX_LEN):
        """
        Create STPMessage object
        @message_data : the data of stp message
        @size        : the length of stp message
        """
        if not message_data or len(message_data) < STP_MESSAGE_FIX_LEN:
            raise ParamsError()

        if not isinstance(message_data, bytes):
            raise ParamsError()

        stpmsg = cls(identifier=int(message_data[:2].hex(), 16),
                     version=message_data[2],
                     bpdu_type=message_data[3],
                     flags=message_data[4],
                     root_ident=message_data[5:13],
                     root_path_cost=int(message_data[13:17].hex(), 16),
                     bridge_ident=message_data[17:25],
                     port_ident=int(message_data[25:27].hex(), 16),
                     message_age=int(message_data[27:29].hex(), 16),
                     max_age=int(message_data[29:31].hex(), 16),
                     hello_time=int(message_data[31:33].hex(), 16),
                     forward_delay=int(message_data[33:35].hex(), 16))

        return stpmsg

    def format_outmsg(self, title="\033[32m STP Message \033[0m", indent=8):
        """
        Format output message
        @title
        @indent
        """
        pass

    def format_vertical_outmsg(self, title="\033[32m STP Message \033[0m", indent=8):
        """
        Format output message in vertical
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        sep_num = 70
        fmt = "| %-30s | %-33s |"
        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "version":
                attrs[attr] = "%d-%s" % (attrs[attr], STP_VERSIONS[attrs[attr]])
            elif attr == "bpdu_type":
                attrs[attr] = "%d-%s" % (attrs[attr], self.BPDU_TYPES[attrs[attr]])
            elif attr in ["root_ident","bridge_ident"]:
                priority = int(attrs[attr][:2].hex(), 16) & 0xF000
                sys_id_ext = int(attrs[attr][:2].hex(), 16) & 0x0FFF
                mac      = conv_format_mac(attrs[attr][2:])
                attrs[attr] = "%d/%d/%s" % (priority, sys_id_ext, mac)
            elif attr == "flags":
                attrs[attr] = bin(attrs[attr])

        # STP has no field version1_length
        if self.version == 0:
            attrs.pop('version1_length')

        outmsg = (prefix + "|>>>   %s   |" % title) + "\n"  + (prefix + "-"*sep_num) + "\n"
        for attr in attrs.keys():
            outmsg += prefix + fmt % (attr.replace('_'," ").title(), str(attrs[attr])) + "\n"

        outmsg += (prefix + "-"*sep_num) + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m STP Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_vertical_outmsg(title=title, indent=indent))


class RSTPMessage(STPMessage):
    """
    Protocol version : 2
    BPDU type : 0x02
    Flags:
        bit7 : TCA
        bit6 : Agreement
        bit5 : Forwarding
        bit4 : Learning
    bit3-bit2: 00 unknown
               01 root port
               10 alternate / backup
               11 custom port
        bit1 : Proposal
        bit0 : TC
    """

    BPDU_TYPES = {0x00:'Configuration', 0x80:'STP TCN BPDU', 0x02:'RSTP BPDU'}
  

class MSTIConfigMessage(object):
    """
    MSTI Config Message(zero or more, total length: n*16bytes):
    -------------------------------------------
    | MSTI Flags                   | 1byte    |
    | MSTI Regional Root Ident     | 8bytes   |
    | MSTI Internal Root Path Cost | 4bytes   |
    | MSTI Bridge Priority         | 1byte    |
    | MSTI Port Priority           | 1byte    |
    | MSTI Remaining Hops          | 1byte    |
    -------------------------------------------
    """
    def __init__(self, msti_flags=0x00, msti_regional_root_ident=b'', 
                 msti_internal_root_path_cost=0x00, msti_bridge_priority=0x00,
                 msti_port_priority=0x00, msti_remaining_hops=0x00):
        """
        @msti_flags
        @msti_regional_root_ident
        @msti_internal_root_path_cost
        @msti_bridge_priority
        @msti_port_priority
        @msti_remaining_hops
        """
        self.msti_flags = msti_flags
        self.msti_regional_root_ident = msti_regional_root_ident
        self.msti_internal_root_path_cost = msti_internal_root_path_cost
        self.msti_bridge_priority = msti_bridge_priority
        self.msti_port_priority = msti_port_priority
        self.msti_remaining_hops = msti_remaining_hops
        
    def format_vertical_outmsg(self, title="\033[32m MSTI Config Message \033[0m", indent=8):
        """
        Format output message in vertical
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        sep_num = 70
        fmt = "| %-30s | %-33s |"
        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "msti_regional_root_ident":
                priority = int(attrs[attr][:2].hex(), 16) & 0xF000
                sys_id_ext = int(attrs[attr][:2].hex(), 16) & 0x0FFF
                mac      = conv_format_mac(attrs[attr][2:])
                attrs[attr] = "%d/%d/%s" % (priority, sys_id_ext, mac)
            elif attr == "msti_flags":
                attrs[attr] = bin(self.msti_flags)

        outmsg = (prefix + "|>>>   %s   |" % title) + "\n"  + (prefix + "-"*sep_num) + "\n"
        for attr in attrs.keys():
            outmsg += prefix + fmt % (attr.replace('_'," ").title(), str(attrs[attr])) + "\n"

        outmsg += (prefix + "-"*sep_num) + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m MSTI Config Message \033[0m", indent=8):
        """

        """
        stdout.write(self.format_vertical_outmsg(title=title, indent=indent))


class MSTPMessage(STPMessage):
    """
    MSTP Message format:
    ------------------------------------------
    | Protocol Identifier           | 2bytes |
    | Protocol Version              | 1byte  |
    | BPDU Type                     | 1byte  |
    | CIST Flags                    | 1byte  |
    | CIST Root Identifier          | 8bytes |
    | CIST Root Path Cost           | 4bytes |
    | CIST Bridge Identifier        | 8bytes |
    | CIST Port Identifier          | 2bytes |
    | Message Age                   | 2bytes |
    | Max Age                       | 2bytes |
    | Hello Time                    | 2bytes |
    | Forward Delay                 | 2bytes |
    ------------------------------------------
    | Version1 Length               | 1byte  |
    | Version3 Length               | 2byte  |
    | MST Config Identifier         | 51bytes|
    | CIST Internal Root Path Cost  | 4bytes |
    | CIST Bridge Identifier        | 8bytes |
    | CIST Remaining Hops           | 1bytes |
    | MSTI Config Message           | 16bytes|103-39+Version3 Length|
    -----------------------------------------------------------------

    The first 35 bytes same as RSTP Message.
    Version1 Length only in RSTP and MSTP, value: 0.

    CIST Flags:
        bit7 : TCA
        bit6 : Agreement
        bit5 : Forwarding
        bit4 : Learning
    bit3-bit2: 00 unknown
               01 root port
               10 alternate / backup
               11 custom port
        bit1 : Proposal
        bit0 : TC

    MST Config Identifier:
    -------------------------------------------------
    | MST Config Format Selector  |  1byte   | 0x00
    | MST Config Name             |  32bytes |
    | MST Revision Level          |  2bytes  |
    | MST Config Digest           |  16bytes |
    -------------------------------------------------

    MSTI Config Message(zero or more, total length: n*16bytes):
    -------------------------------------------
    | MSTI Flags                   | 1byte    |
    | MSTI Regional Root Ident     | 8bytes   |
    | MSTI Internal Root Path Cost | 4bytes   |
    | MSTI Bridge Priority         | 1byte    |
    | MSTI Port Priority           | 1byte    |
    | MSTI Remaining Hops          | 1byte    |
    -------------------------------------------

    MSTI Flags same as CIST Flags:
        bit7 : TCA
        bit6 : Agreement
        bit5 : Forwarding
        bit4 : Learning
    bit3-bit2: 00 unknown
               01 root port
               10 alternate / backup
               11 custom port
        bit1 : Proposal
        bit0 : TC

    Reference: https://zhuanlan.zhihu.com/p/575547198
    """

    BPDU_TYPES = {0x00:'Configuration', 0x80:'STP TCN BPDU', 0x02:'MSTP BPDU'}

    def __init__(self, identifier=0x00, version=0x00, bpdu_type=0x00, flags=0x00,
                 root_ident=b'', root_path_cost=0x00, bridge_ident=b'', port_ident=0x00, 
                 message_age=0x00, max_age=0x00, hello_time=0x00, forward_delay=0x00, 
                 version1_length=0x00, version3_length=0x00, mst_config_format_selector=0x00,
                 mst_config_name=b'', mst_revision_level=0x00, mst_config_digest=b'',
                 cist_internal_root_path_cost=0x00, cist_bridge_ident=b'', cist_remaining_hops=0x00,
                 msti_config_messages=[]):
        super().__init__(identifier=identifier,
                        version=version,
                        bpdu_type=bpdu_type,
                        flags=flags,
                        root_ident=root_ident,
                        root_path_cost=root_path_cost,
                        bridge_ident=bridge_ident,
                        port_ident=port_ident,
                        message_age=message_age,
                        max_age=max_age,
                        hello_time=hello_time,
                        forward_delay=forward_delay,
                        version1_length=version1_length)
        self.version3_length = version3_length
        self.mst_config_format_selector = mst_config_format_selector
        self.mst_config_name = mst_config_name
        self.mst_revision_level = mst_revision_level
        self.mst_config_digest = mst_config_digest
        self.cist_internal_root_path_cost = cist_internal_root_path_cost
        self.cist_bridge_ident = cist_bridge_ident
        self.cist_remaining_hops = cist_remaining_hops
        self.msti_config_messages = msti_config_messages

    @classmethod
    def create(cls, message_data, size=MSTP_MESSAGE_FIX_LEN):
        """
        Create MSTPMessage object
        @message_data
        @size
        """
        if not message_data or len(message_data) < MSTP_MESSAGE_FIX_LEN:
            raise ParamsError()

        if not isinstance(message_data, bytes):
            raise ParamsError()

        messages = []
        if len(message_data) > MSTP_MESSAGE_FIX_LEN:
            msti_conf_msg_data = message_data[MSTP_MESSAGE_FIX_LEN:]
            pos = 0
            length = len(msti_conf_msg_data)
            while True:
                msticonfmsg = MSTIConfigMessage(msti_flags=msti_conf_msg_data[0], 
                        msti_regional_root_ident=msti_conf_msg_data[1:9],
                        msti_internal_root_path_cost=msti_conf_msg_data[9:13], 
                        msti_bridge_priority=msti_conf_msg_data[13],
                        msti_port_priority=msti_conf_msg_data[14], 
                        msti_remaining_hops=msti_conf_msg_data[15])
                messages.append(msticonfmsg)
                pos += 16
                # end data
                if pos >= length:
                    break

                msti_conf_msg_data = msti_conf_msg_data[16:]

        mstpmsg = cls(identifier=int(message_data[:2].hex(), 16),
                     version=message_data[2],
                     bpdu_type=message_data[3],
                     flags=message_data[4],
                     root_ident=message_data[5:13],
                     root_path_cost=int(message_data[13:17].hex(), 16),
                     bridge_ident=message_data[17:25],
                     port_ident=int(message_data[25:27].hex(), 16),
                     message_age=int(message_data[27:29].hex(), 16),
                     max_age=int(message_data[29:31].hex(), 16),
                     hello_time=int(message_data[31:33].hex(), 16),
                     forward_delay=int(message_data[33:35].hex(), 16),
                     version1_length=message_data[35],
                     version3_length=int(message_data[36:38].hex(), 16),
                     mst_config_format_selector=message_data[38],
                     mst_config_name=message_data[39:71],
                     mst_revision_level=int(message_data[71:73].hex(), 16),
                     mst_config_digest=message_data[73:89],
                     cist_internal_root_path_cost=int(message_data[89:93].hex(), 16),
                     cist_bridge_ident=message_data[93:101],
                     cist_remaining_hops=message_data[101],
                     msti_config_messages=messages)
                     
        return mstpmsg

    def format_vertical_outmsg(self, title="\033[32m MSTP Message \033[0m", indent=8):
        """
        Format output message in vertical
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        attrs = deepcopy(self.__dict__)
        for attr in attrs.keys():
            if attr == "version":
                attrs[attr] = "%d-%s" % (attrs[attr], STP_VERSIONS[attrs[attr]])
            elif attr == "bpdu_type":
                attrs[attr] = "%d-%s" % (attrs[attr], self.BPDU_TYPES[attrs[attr]])
            elif attr == "flags":
                attrs[attr] = bin(attrs[attr])
            elif attr in ["root_ident", "bridge_ident", "cist_bridge_ident"]:
                priority = int(attrs[attr][:2].hex(), 16) & 0xF000
                sys_id_ext = int(attrs[attr][:2].hex(), 16) & 0x0FFF
                mac      = conv_format_mac(attrs[attr][2:])
                attrs[attr] = "%d/%d/%s" % (priority, sys_id_ext, mac)
            elif attr == "mst_config_digest":
                attrs[attr] = attrs[attr].hex()
            elif attr == "mst_config_name":
                attrs[attr] = attrs[attr].strip(b'\x00').decode()

        if len(attrs['mst_config_name']) > 32:
            fmt = "| %-30s | %-64s |"
            sep_num = 101
        else:
            fmt = "| %-30s | %-33s |"
            sep_num = 70

        outmsg = (prefix + "|>>>   %s   |" % title) + "\n"  + (prefix + "-"*sep_num) + "\n"
        for attr in attrs.keys():
            if attr == "msti_config_messages":
                continue
            outmsg += prefix + fmt % (attr.replace('_'," ").title(), str(attrs[attr])) + "\n"

        outmsg += (prefix + "-"*sep_num) + "\n"

        count = 0
        for message in self.msti_config_messages:
            outmsg += message.format_vertical_outmsg(title="\033[32m  MSTI Config Message [%d]\033[0m" % count, indent=8+2)
            count += 1

        return outmsg


#------------------------------------------------------------------------
if __name__ == '__main__':
    pass
