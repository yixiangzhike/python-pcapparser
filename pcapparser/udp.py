#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.error import ParamsError
from pcapparser.constants import UDP_HDR_FIX_LEN

class UDPFrameHeader(object):
    """
    UDP Frame Header format:
    ---------------------------------------------------
    fields | S.PORT  | D.PORT  | Length  |  Checksum  |
    ---------------------------------------------------
    bytes  |   2     |    2    |    2    |     2      |
    ---------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc768.txt
    """
    def __init__(self, sport=0x00, dport=0x00, length=0x00, checksum=0x00):
        """
        @sport    : Source PORT
        @dport    : Destination PORT
        @length   : The length of udp header and data, min 8bytes
        @checksum : Checksum, udp header and data
        """
        self.sport = sport
        self.dport = dport
        self.length = length
        self.checksum = checksum

    @classmethod
    def create(cls, header_data, size=UDP_HDR_FIX_LEN):
        """
        Create UDPFrameHeader object
        @header_data : the data of udp header
        @size        : the length of udp frame header
        """
        if not header_data or size < UDP_HDR_FIX_LEN:
            raise ParamsError()

        if not isinstance(header_data, bytes):
            raise ParamsError()

        udphdr = cls(sport=int(header_data[:2].hex(), 16),
                     dport=int(header_data[2:4].hex(), 16),
                     length=int(header_data[4:6].hex(), 16),
                     checksum=int(header_data[6:8].hex(), 16))

        return udphdr

    def format_outmsg(self, title="\033[32m UDP Header \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-6s | %-6s | %-6s | %-8s |"
        head_out = prefix + fmt % ("S.PORT", "D.PORT", "Length", "Checksum")
        data_out = prefix + fmt % (str(self.sport), str(self.dport), str(self.length), str(self.checksum))
        sep_num = 40

        outmsg = (prefix + "|>>>   %s    |" % title) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (head_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (data_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m UDP Header \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        

class UDPFrame(object):
    """
    UDP frame format:
    ---------------------------
    | UDP Header | Frame Data |
    ---------------------------
    |   8bytes   |    n       |
    ---------------------------
    """
    def __init__(self, frmhdr=None, frmdata=None):
        """
        @frmhdr : The object of UDPFrameHeader
        @frmdata : UDP frame data
        """
        self.frmhdr = frmhdr
        self.frmdata = frmdata

#------------------------------------------------------------------------
if __name__ == '__main__':
    pass
