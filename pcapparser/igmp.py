#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.error import ParamsError
from pcapparser.constants import IGMP_BASE_LEN,IGMP_TYPES,IGMP_GROUP_RECORD_TYPES
from pcapparser.utils import format_ip2s

__all__=['get_igmp_type', 'IGMPMessage', 'IGMPv3QueryMessage', 'IGMPv3ReportMessage']

def get_igmp_type(igmp_data):
    """
    Get IGMP message type from the data
    """
    if not igmp_data:
        raise ParamsError()

    if not isinstance(igmp_data, bytes):
        raise paramsError()

    return int(igmp_data[:1].hex(), 16)


class IGMPMessage(object):
    """
    IGMPv1/IGMPv2 Message format:
    -----------------------------------------------------------
    fields | Type  | Max Resp Time | Checksum | Group Address |
    -----------------------------------------------------------
    bytes  |   1   |      1        |      2   |       4       |
    -----------------------------------------------------------

    IGMP only used in IPv4 protocol.
    The destination IP address in IPv4 header identifies the IGMP receiver.

    Reference: https://www.rfc-editor.org/rfc/rfc2236.txt
    """
    def __init__(self, igmp_type=0x00, max_resp_time=0x00, checksum=0x00, group_address=b''):
        """
        @igmp_type
        @max_resp_time
        @checksum
        @group_address
        """
        self.igmp_type  = igmp_type
        self.max_resp_time = max_resp_time
        self.checksum = checksum
        self.group_address = group_address

        if self.igmp_type == 0x12:
            self.version = 1
        else:
            self.version = 2

    @classmethod
    def create(cls, data, size=IGMP_BASE_LEN):
        """
        Create IGMPMessage object
        @data : the data of IGMP
        @size        : the length of IGMP frame 
        """
        if not data or size < IGMP_BASE_LEN:
            raise ParamsError()

        if not isinstance(data, bytes):
            raise ParamsError()

        igmp = cls(igmp_type=int(data[:1].hex(), 16),
                   max_resp_time=int(data[1:2].hex(), 16),
                   checksum=int(data[2:4].hex(), 16),
                   group_address=data[4:8])

        return igmp

    def format_outmsg(self, title="\033[32m IGMP Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-18s | %-13s | %-8s | %-15s | %-7s |"
        head_out = prefix + fmt % ("Type", "Max Resp Time", "Checksum", "Group Address", "Version")
        data_out = prefix + fmt % ("0x%x-%s"%(self.igmp_type, IGMP_TYPES[self.igmp_type]), 
                                   str(self.max_resp_time), 
                                   str(self.checksum), 
                                   format_ip2s(self.group_address, 4), 
                                   str(self.version))
        sep_num = 77

        outmsg = (prefix + "|>>>   %s    |" % title) + "\n" \
                  + (prefix + "-"*sep_num) + "\n" \
                  + (head_out) + "\n" \
                  + (prefix + "-"*sep_num) + "\n" \
                  + (data_out) + "\n" \
                  + (prefix + "-"*sep_num) + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m IGMP Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        

class IGMPv3QueryMessage(IGMPMessage):
    """
    IGMPv3 Query Message format:
    -----------------------------------------------------------------------------------------------
    | Type  | Max Resp Time | Checksum | Group Address | Resv | S  | QRV | QQIC | Number | S.ADDR |
    -----------------------------------------------------------------------------------------------
    | 1byte |      1byte    |  2bytes  |   4bytes      | 4bit |1bit|3bit | 1byte| 2bytes |    n   |
    -----------------------------------------------------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc3376.txt
    """
    def __init__(self, igmp_type=0x11, max_resp_time=0x00, checksum=0x00, group_address=b'', ext_data=b''):
        """
        @igmp_type
        @max_resp_time
        @checksum
        @group_address
        @ext_data
        """
        super().__init__(igmp_type=igmp_type, max_resp_time=max_resp_time, 
                checksum=checksum, group_address=group_address)
        self.version = 3
        self.ext_data = ext_data
        self.resv = 0x00
        self.s = 0x00
        self.qrv = 0x00
        self.qqic = 0x00
        self.number = 0x00
        self.saddrs = []

    def parse_extension_data(self):
        """
        Parse fields from extension data.
        """
        if not self.ext_data:
            return

        if not isinstance(self.ext_data, bytes):
            raise ParamsError()

        self.resv = int(self.ext_data[:1].hex(), 16) >> 4 & 0x0F
        self.s = int(self.ext_data[:1].hex(), 16) >> 3 & 0x01
        self.qrv = int(self.ext_data[:1].hex(), 16) & 0x07
        self.qqic = int(self.ext_data[1:2].hex(), 16)
        self.number = int(self.ext_data[2:4].hex(), 16)

        for idx in range(self.number):
            self.saddrs.append(self.ext_data[4+idx*4:4+idx*4+4])

    @classmethod
    def create(cls, igmp_data):
        pass

    def format_outmsg(self, title="\033[32m IGMP Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-18s | %-13s | %-8s | %-15s | %-4s | %-1s | %-3s | %-4s | %-6s | %-7s |"
        head_out = prefix + fmt % ("Type", "Max Resp Time", "Checksum", 
                                   "Group Address", "Resv", "S", "QRV",
                                   "QQIC", "Number", "Version")
        data_out = prefix + fmt % ("0x%x-%s"%(self.igmp_type, IGMP_TYPES[self.igmp_type]), 
                                   str(self.max_resp_time), 
                                   str(self.checksum), 
                                   format_ip2s(self.group_address, 4), 
                                   str(self.resv), 
                                   str(self.s), 
                                   str(self.qrv), 
                                   str(self.qqic), 
                                   str(self.number), 
                                   str(self.version))
        sep_num = 110

        outmsg = (prefix + "|>>>   %s    |" % title) + "\n" \
                  + (prefix + "-"*sep_num) + "\n" \
                  + (head_out) + "\n" \
                  + (prefix + "-"*sep_num) + "\n" \
                  + (data_out) + "\n" \
                  + (prefix + "-"*sep_num) + "\n"

        return outmsg + self.format_saddrs(indent=indent+4)


    def format_saddrs(self, indent=16):
        prefix = "%*s" % (indent, " ")
        idx = 0
        outmsg = ""
        for address in self.saddrs:
            if not idx:
                outmsg += prefix + "| \033[32m Source Address \033[0m |" + "\n"
                outmsg += prefix + "-"*17 + "\n"
            outmsg += prefix + "| " + format_ip2s(address, 4) + " |\n"
            idx += 1
        if outmsg:
            outmsg += prefix + "-"*17 + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m IGMP Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))

class GroupRecord(object):
    """
    Group Record in IGMPv3 Report Message.
    format:
    -------------------------------------------------------------------------------------------
    | Record Type | Aux DataLen | Num S.Addr | MultiAddr | S.Addr1 | S.Addr2 | ... | Aux Data |
    -------------------------------------------------------------------------------------------
    |   1byte     |  1byte      |  2bytes    |    4bytes |  4bytes |  4bytes | ... |    n     | 
    -------------------------------------------------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc3376.txt
    """
    def __init__(self, record_type=0x00, aux_datalen=0x00, num_saddr=0x00,
                 multi_addr=b'', saddr_data=b'', aux_data=b''):
        """
        @record_type
        @aux_datalen
        @mum_saddr
        @multi_addr
        @saddr_data
        @aux_data
        """
        self.record_type = record_type
        self.aux_datalen = aux_datalen
        self.num_saddr = num_saddr
        self.multi_addr = multi_addr
        self.saddr_data = saddr_data
        self.aux_data = aux_data
        self.saddrs = []

    def parse_source_address(self):
        """
        Parse every source address from self.saddr_data
        """
        for count in range(self.num_saddr):
            self.saddrs.append(self.saddr_data[4*count:4*count+4])

    @classmethod
    def get_group_record_length(cls, gr_data):
        """
        Get group record length from IGMP data.
        @gr_data
        """
        if not gr_data or len(gr_data) < 4:
            raise ParamsError()

        if not isinstance(gr_data, bytes):
            raise ParamsError()

        aux_data_len = int(gr_data[1:2].hex(), 16)
        saddr_data_len = 4* int(gr_data[2:4].hex(), 16)

        return (1+1+2+4+saddr_data_len+aux_data_len)

    @classmethod
    def create(cls, gr_data, size=0x00):
        """
        Create GroupRecord object.
        @gr_data
        @size
        """
        if not gr_data:
            raise ParamsError()
        if not isinstance(gr_data, bytes):
            raise ParamsError()

        aux_data_len = int(gr_data[1:2].hex(), 16)
        saddr_data_len = 4* int(gr_data[2:4].hex(), 16)

        group_record = cls(record_type=int(gr_data[:1].hex(), 16),
                           aux_datalen=aux_data_len,
                           num_saddr=int(gr_data[2:4].hex(), 16),
                           multi_addr=gr_data[4:8],
                           saddr_data=gr_data[8:8+saddr_data_len],
                           aux_data=gr_data[8+saddr_data_len:8+saddr_data_len+aux_data_len])

        return group_record

    def format_outmsg(self, title="\033[32m Group Record \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-26s | %-11s | %-11s | %-15s | %-20s |"
        head_out = prefix + fmt % ("Record Type", "Aux DataLen", "Num S.Addr", "MultiAddr", "Aux Data")
        data_out = prefix + fmt % ("%d-%s" % (self.record_type, IGMP_GROUP_RECORD_TYPES[self.record_type]), 
                                   str(self.aux_datalen),
                                   str(self.num_saddr), format_ip2s(self.multi_addr, 4), 
                                   self.aux_data.hex() if self.aux_data else "")
        sep_num = 99

        outmsg = (prefix + "|  %s  |" % title) + "\n" \
                  + (prefix + "-"*sep_num) + "\n" \
                  + (head_out) + "\n" \
                  + (prefix + "-"*sep_num) + "\n" \
                  + (data_out) + "\n" \
                  + (prefix + "-"*sep_num) + "\n"

        return outmsg + self.format_saddrs(indent=indent+4)


    def format_saddrs(self, indent=16):
        prefix = "%*s" % (indent, " ")
        idx = 0
        outmsg = ""
        for address in self.saddrs:
            if not idx:
                outmsg += prefix + "| \033[32m Source Address \033[0m |" + "\n"
                outmsg += prefix + "-"*17 + "\n"
            outmsg += prefix + "| " + format_ip2s(address, 4) + " |\n"
            idx += 1

        if outmsg:
            outmsg += prefix + "-"*17 + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m Group Record \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))


class IGMPv3ReportMessage(object):
    """
    IGMPv3 Report Message format:
    ---------------------------------------------------------------------
    | Type  | Reserved | Checksum | Reserved | Num GR | GR1 | GR2 | ... |
    ---------------------------------------------------------------------
    | 1byte | 1byte    |  2bytes  |  2bytes  |  2byte | n   |  n  | ... |
    ---------------------------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc3376.txt
    """
    def __init__(self, igmp_type=0x22, reserved1=0x00, checksum=0x00, reserved2=0x00, gr_num=0x00, gr_data=b''):
        """
        @igmp_type
        @reserved1
        @checksum
        @reserved2
        @gr_num
        @gr_data
        """
        self.version = 3
        self.igmp_type = igmp_type
        self.reserved1 = reserved1
        self.checksum = checksum
        self.reserved2 = reserved2
        self.gr_num = gr_num
        self.gr_data = gr_data
        self.group_records = []

    def parse_group_record_data(self):
        """
        Parse group record
        """
        if not self.gr_data:
            raise ParamsError()

        if not isinstance(self.gr_data, bytes):
            raise ParamsError()

        data = self.gr_data
        while True:
            if not data:
                break
            gr_length = GroupRecord.get_group_record_length(data)
            gr = GroupRecord.create(data[:gr_length], size=gr_length)
            gr.parse_source_address()
            self.group_records.append(gr)
            data = data[gr_length:]

    @classmethod
    def create(cls, igmp_data):
        pass

    def format_outmsg(self, title="\033[32m IGMP Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-18s | %-8s | %-8s | %-8s | %-19s |"
        head_out = prefix + fmt % ("Type", "Reserved", "Checksum", "Reserved", "Num of group record")
        data_out = prefix + fmt % ("0x%x-%s"%(self.igmp_type, IGMP_TYPES[self.igmp_type]), 
                                   str(self.reserved1), 
                                   str(self.checksum), 
                                   str(self.reserved2), 
                                   str(self.gr_num))
        sep_num = 77

        outmsg = (prefix + "|>>>   %s    |" % title) + "\n" \
                  + (prefix + "-"*sep_num) + "\n" \
                  + (head_out) + "\n" \
                  + (prefix + "-"*sep_num) + "\n" \
                  + (data_out) + "\n" \
                  + (prefix + "-"*sep_num) + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m IGMP Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        if self.group_records:
            count = 0
            for gr in self.group_records:
                gr.format_output(stdout, title="\033[32m Group Record [%d] \033[0m" % count, indent=indent)
                count += 1

#------------------------------------------------------------------------
if __name__ == '__main__':
    pass
