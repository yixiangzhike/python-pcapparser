#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

import os
from pcapparser.error import DataFileError
from pcapparser.constants import PCAP_MAGIC_LITTLE,PCAP_MAGIC_BIG,PCAPNG_MAGIC, \
        PCAPNG_BYTEORDER_LITTLE,PCAPNG_BYTEORDER_BIG

class DataFile(object):
    """
    Support file xxx.pcap and xxx.pcapng.

    The magic number in the file xxx.pcap:
    0xa1b2c3d4  (big-endian) 
    0xd4c3b2a1  (little-endian).

    The magic number in the file xxx.pcapng 0x0a0d0d0a, 
    the byteorder:
    0x4d3c2b1a  (little-endian) 
    0x1a2b3c4d  (big-endian)
    """
    def __init__(self, path):
        """
        @path : the path of xxx.pcap and xxx.pcapng
        """
        self.path = path

    def get_magic_number(self):
        """
        Get the magic number from file, the magic number is the first 4bytes in the file.
        """
        fileobj = open(self.path, mode="rb")
        magic = int(fileobj.read(4).hex(), 16)
        fileobj.close()

        return magic
    
    def set_file_format(self):
        """
        Set the format field for DataFile object.
        The format: pcap or pcapng
        """
        magic = self.get_magic_number()
        if magic in [PCAP_MAGIC_LITTLE,PCAP_MAGIC_BIG]:
            self.format = "pcap"

        if magic in [PCAPNG_MAGIC]:
            self.format = "pcapng"

    def set_file_endian(self):
        """
        Set the endian field for DataFile object.
        The endian: big-endian or little-endian
        """
        magic = self.get_magic_number()
        if magic == PCAP_MAGIC_BIG:
            self.endian = "big"
        elif magic == PCAP_MAGIC_LITTLE:
            self.endian = "little"
        elif magic in [PCAPNG_MAGIC]:
            # get pcapng file byteorder
            fileobj = open(self.path, mode="rb")
            byteorder = int(fileobj.read(12)[8:12].hex(), 16)
            fileobj.close()
            if byteorder == PCAPNG_BYTEORDER_LITTLE:
                self.endian = "little"
            elif byteorder == PCAPNG_BYTEORDER_BIG:
                self.endian = "big"
            else:
                self.endian = "unknown"
        else:
            self.endian = "unknown"

        if self.endian == "unknown":
            raise DataFileError()

    def check_file(self):
        """
        Check file exist and right format.
        """
        if not os.path.exists(self.path):
            return False

        fileobj = open(self.path, mode="rb")
        magic = int(fileobj.read(4).hex(), 16)
        if not magic in [PCAP_MAGIC_LITTLE,PCAP_MAGIC_BIG,PCAPNG_MAGIC]:
            fileobj.close()
            return False

        if magic in [PCAPNG_MAGIC]:
            # check byteorder
            byteorder = int(fileobj.read(8)[4:8].hex(), 16)
            if not byteorder in [PCAPNG_BYTEORDER_LITTLE, PCAPNG_BYTEORDER_BIG]:
                fileobj.close()
                return False

        fileobj.close()

        return True
    
    def debug_output(self, debug=False):
        """
        Output the field value for debug.
        """
        if not debug:
            return ""
        print("DataFile:field:path   [%s]" % self.path)
        print("DataFile:field:format [%s]" % self.format)
        print("DataFile:field:endian [%s]" % self.endian)
