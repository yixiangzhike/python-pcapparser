#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from copy import deepcopy

from pcapparser.error import ParamsError
from pcapparser.constants import ICMPV4_MESSAGE_TYPES
from pcapparser.utils import format_ip2s,format_datetime
from pcapparser.option import *


__all__ = [
        'get_message_type_and_code_icmpv4', 
        'ICMPv4DestUnreachableMessage',
        'ICMPv4SourceQuenchMessage',
        'ICMPv4TimeOutMessage',
        'ICMPv4ParamProblemMessage',
        'ICMPv4EchoMessage',
        'ICMPv4RedirectMessage',
        'ICMPv4TimeStampMessage',
        'ICMPv4InformationMessage',
]

def get_message_type_and_code_icmpv4(icmpv4_data):
    """
    Get message type and code from icmpv4 message data
    @icmpv4_data : min size 2, include type and code
    """
    if not icmpv4_data or len(icmpv4_data) < 2:
        raise ParamsError()

    if not isinstance(icmpv4_data, bytes):
        raise ParamsError()

    return (icmpv4_data[0], icmpv4_data[1])

class ICMPv4BaseMessage(object):
    """
    ICMPv4 base message format:
    ------------------------------------------------
    fields | Type | Code | Checksum | Message Body |
    ------------------------------------------------
    bytes  |  1   |  1   |    2     |     n        |
    ------------------------------------------------

    Two type message:
    1. Error Reporting Message 
    2. Query Message

    Type:
        0   Echo Reply
        3   Destination Unreachable
        4   Source Quench
        5   Redirect
        8   Echo
        11  Time Exceeded
        12  Parameter Problem
        13  Timestamp
        14  Timestamp Reply
        15  Information Request
        16  Information Reply

    Reference: https://www.rfc-editor.org/rfc/rfc792.txt
    """

    EXCLUDE_VARS = ["message_body"]

    def __init__(self, mtype=0x00, code=0x00, checksum=0x00, message_body=b''):
        """
        @mtype        : Error Reporting Message, Query Message
        @code         : An additional level of message granularity
        @checksum     : Checksum, udp header and data
        @message_body : Remaining fields in different messages
        """
        self.mtype = mtype
        self.code = code
        self.checksum = checksum
        self.message_body = message_body

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        pass

    def format_outmsg(self, title="\033[32m ICMPv4 Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        head_out = "|"
        data_out = "|"
        num_sep = 0
        for attr in attrs.keys():
            if attr in self.EXCLUDE_VARS:
                continue
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))
        title_out = prefix + "|>>>  %s  |\n" % title
        width = len(ICMPV4_MESSAGE_TYPES[str(self.mtype)+str(self.code)])
        if width < len("Description"):
            width = len("Description")
        head_out = prefix + head_out + " %-*s |\n" % (width, "Description")
        data_out = prefix + data_out + " %-*s |\n" % (width, ICMPV4_MESSAGE_TYPES[str(self.mtype)+str(self.code)])
        num_sep += width+4

        if attrs.get('ihd8', None):
            data_out += prefix + "-"*num_sep + "\n"
            data_out += prefix + "| IP Header + First 8bytes of Datagram |" + "\n"
            data_out += prefix + "-"*num_sep + "\n"

            if isinstance(self.ihd8, bytes):
                ihd8_hex = self.ihd8.hex()
            else:
                ihd8_hex = self.ihd8
            length = len(ihd8_hex)
            n = (length // num_sep + 1) if length%num_sep else (length // num_sep)
            for idx in range(n):
                data_out += prefix + ihd8_hex[idx*num_sep:(idx+1)*num_sep] + "\n"

        return title_out + prefix + "-"*num_sep + "\n" + head_out + \
                prefix + "-"*num_sep + "\n" + data_out + prefix + "-"*num_sep + "\n"

    def format_output(self, stdout, title="\033[32m ICMPv4 Message \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))


class ICMPv4DestUnreachableMessage(ICMPv4BaseMessage):
    """
    ICMPv4 destination unreachable  message format:
    ---------------------------------------------------------------------------
    fields | Type | Code | Checksum | Unused  | IH + First 8bytes of Datagram |
    ---------------------------------------------------------------------------
    bytes  |  1   |  1   |    2     |   4     |                n              |
    ---------------------------------------------------------------------------

    Type      3
    Code
              0 = net unreachable;
              1 = host unreachable;
              2 = protocol unreachable;
              3 = port unreachable;
              4 = fragmentation needed and DF set;
              5 = source route failed.
    Checksum  The checksum is the 16-bit ones's complement of the one's
              complement sum of the ICMP message starting with the ICMP Type.
              For computing the checksum , the checksum field should be zero.
              This checksum may be replaced in the future.
    
    Internet Header + 64 bits of Data Datagram
              The internet header plus the first 64 bits of the original

    Reference: https://www.rfc-editor.org/rfc/rfc792.txt
    """

    EXCLUDE_VARS = ["message_body", "ihd8"]

    def __init__(self, mtype=0x03, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.unused = int(self.message_body[:4].hex(), 16)
            self.ihd8 = self.message_body[4:]

class ICMPv4SourceQuenchMessage(ICMPv4BaseMessage):
    """
    ICMPv4 source quench message format:
    ---------------------------------------------------------------------------
    fields | Type | Code | Checksum | Unused  | IH + First 8bytes of Datagram |
    ---------------------------------------------------------------------------
    bytes  |  1   |  1   |    2     |   4     |                n              |
    ---------------------------------------------------------------------------

    Type      4
    Code      0
    Checksum  The checksum is the 16-bit ones's complement of the one's
              complement sum of the ICMP message starting with the ICMP Type.
              For computing the checksum , the checksum field should be zero.
              This checksum may be replaced in the future.
    
    Internet Header + 64 bits of Data Datagram
              The internet header plus the first 64 bits of the original
              datagram's data.  This data is used by the host to match the
              message to the appropriate process.  If a higher level protocol
              uses port numbers, they are assumed to be in the first 64 data
              bits of the original datagram's data.

    Reference: https://www.rfc-editor.org/rfc/rfc792.txt
    """

    EXCLUDE_VARS = ["message_body", "ihd8"]

    def __init__(self, mtype=0x04, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.unused = int(self.message_body[:4].hex(), 16)
            self.ihd8 = self.message_body[4:]

class ICMPv4TimeOutMessage(ICMPv4BaseMessage):
    """
    ICMPv4 timeout message format:
    ---------------------------------------------------------------------------
    fields | Type | Code | Checksum | Unused  | IH + First 8bytes of Datagram |
    ---------------------------------------------------------------------------
    bytes  |  1   |  1   |    2     |   4     |                n              |
    ---------------------------------------------------------------------------

    Type        11
    Code
                0 = time to live exceeded in transit;
                1 = fragment reassembly time exceeded.
    Checksum    The checksum is the 16-bit ones's complement of the one's
                complement sum of the ICMP message starting with the ICMP Type.
                For computing the checksum , the checksum field should be zero.
                This checksum may be replaced in the future.
    
    Internet Header + 64 bits of Data Datagram
                The internet header plus the first 64 bits of the original
                datagram's data.  This data is used by the host to match the
                message to the appropriate process.  If a higher level protocol
                uses port numbers, they are assumed to be in the first 64 data
                bits of the original datagram's data.

    Reference: https://www.rfc-editor.org/rfc/rfc792.txt
    """

    EXCLUDE_VARS = ["message_body", "ihd8"]

    def __init__(self, mtype=0x0b, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.unused = int(self.message_body[:4].hex(), 16)
            self.ihd8 = self.message_body[4:]


class ICMPv4ParamProblemMessage(ICMPv4BaseMessage):
    """
    ICMPv4 parameter problem message format:
    -------------------------------------------------------------------------------------
    fields | Type | Code | Checksum | Pointer | Unused  | IH + First 8bytes of Datagram |
    -------------------------------------------------------------------------------------
    bytes  |  1   |  1   |    2     |   1     |   3     |                  n            |
    -------------------------------------------------------------------------------------

    Type       12
    Code
               0 = pointer indicates the error.
    
    Checksum   The checksum is the 16-bit ones's complement of the one's
               complement sum of the ICMP message starting with the ICMP Type.
               For computing the checksum , the checksum field should be zero.
               This checksum may be replaced in the future.
    Pointer    If code = 0, identifies the octet where an error was detected.
    
    Internet Header + 64 bits of Data Datagram
               The internet header plus the first 64 bits of the original
               datagram's data.  This data is used by the host to match the
               message to the appropriate process.  If a higher level protocol
               uses port numbers, they are assumed to be in the first 64 data
               bits of the original datagram's data.

    Reference: https://www.rfc-editor.org/rfc/rfc792.txt
    """

    EXCLUDE_VARS = ["message_body", "ihd8"]

    def __init__(self, mtype=0x0c, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            # Pointer Identifies the octet offset within the 
            # invoking packet where the error was detected. 
            self.pointer = self.message_body[4]
            self.unused = int(self.message_body[5:8].hex(), 16)
            self.ihd8 = self.message_body[8:]


class ICMPv4EchoMessage(ICMPv4BaseMessage):
    """
    ICMPv4 echo request or echo response message format:
    -------------------------------------------------------------------------
    fields | Type | Code | Checksum | Identifier | Sequence Number |  Data  |
    -------------------------------------------------------------------------
    bytes  |  1   |  1   |    2     |      2     |       2         |   n    |
    -------------------------------------------------------------------------

    Type
                      8 for echo message;
                      0 for echo reply message.
    Code              0
    Checksum          The checksum is the 16-bit ones's complement of the one's
                      complement sum of the ICMP message starting with the ICMP Type.
                      For computing the checksum , the checksum field should be zero.
                      If the total length is odd, the received data is padded with one
                      octet of zeros for computing the checksum.  This checksum may be
                      replaced in the future.
    
    Identifier        If code = 0, an identifier to aid in matching echos and replies,
                      may be zero.
    
    Sequence Number   If code = 0, a sequence number to aid in matching echos and
                      replies, may be zero.

    Reference: https://www.rfc-editor.org/rfc/rfc792.txt
    """

    EXCLUDE_VARS = ["message_body", "data"]

    def __init__(self, mtype=0x08, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.identifier = int(self.message_body[:2].hex(), 16)
            self.seq_number = int(self.message_body[2:4].hex(), 16)
            self.data = self.message_body[4:].hex()

    def format_outmsg(self, title="\033[32m ICMPv4 Message \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")

        attrs = deepcopy(self.__dict__)
        head_out = "|"
        data_out = "|"
        num_sep = 0
        for attr in attrs.keys():
            if attr in self.EXCLUDE_VARS:
                continue
            if len(attr) > len(str(attrs[attr])):
                width = len(attr)
            else:
                width = len(str(attrs[attr]))
            num_sep += width+3
            head_out += " %-*s |" % (width, attr.replace('_', ' ').title())
            data_out += " %-*s |" % (width, str(attrs[attr]))
        title_out = prefix + "|>>>  %s  |\n" % title
        width = len(ICMPV4_MESSAGE_TYPES[str(self.mtype)+str(self.code)])
        if width < len("Description"):
            width = len("Description")
        head_out = prefix + head_out + " %-*s |\n" % (width, "Description")
        data_out = prefix + data_out + " %-*s |\n" % (width, ICMPV4_MESSAGE_TYPES[str(self.mtype)+str(self.code)])
        num_sep += width+4

        data_out += prefix + "-"*num_sep + "\n"
        data_out += prefix + "| Data |" + "\n"
        data_out += prefix + "-"*num_sep + "\n"

        length = len(self.data)
        n = (length // num_sep + 1) if length%num_sep else (length // num_sep)
        for idx in range(n):
            data_out += prefix + self.data[idx*num_sep:(idx+1)*num_sep] + "\n"

        return title_out + prefix + "-"*num_sep + "\n" + head_out + \
                prefix + "-"*num_sep + "\n" + data_out + prefix + "-"*num_sep + "\n"

class ICMPv4RedirectMessage(ICMPv4BaseMessage):
    """
    ICMPv4 redirect message format:
    -------------------------------------------------------------------------------------------
    fields| Type | Code | Checksum | Gateway Internet Address | IH + First 8bytes of Datagram |
    -------------------------------------------------------------------------------------------
    bytes |  1   |  1   |    2     |          4               |              n                |
    -------------------------------------------------------------------------------------------

    Type            5
    Code
                    0 = Redirect datagrams for the Network.
                    1 = Redirect datagrams for the Host.
                    2 = Redirect datagrams for the Type of Service and Network.
                    3 = Redirect datagrams for the Type of Service and Host.
    Checksum        The checksum is the 16-bit ones's complement of the one's
                    complement sum of the ICMP message starting with the ICMP Type.
                    For computing the checksum , the checksum field should be zero.
                    This checksum may be replaced in the future.
    
    Gateway Internet Address
                    Address of the gateway to which traffic for the network specified
                    in the internet destination network field of the original
                    datagram's data should be sent.
    
    Internet Header + 64 bits of Data Datagram
                    The internet header plus the first 64 bits of the original
                    datagram's data.  This data is used by the host to match the
                    message to the appropriate process.  If a higher level protocol
                    uses port numbers, they are assumed to be in the first 64 data
                    bits of the original datagram's data.

    Reference: https://www.rfc-editor.org/rfc/rfc792.txt
    """

    EXCLUDE_VARS = ["message_body", "ihd8"]

    def __init__(self, mtype=0x05, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.gateway_address = format_ip2s(self.message_body[:4], 0x04)
            self.ihd8 = self.message_body[4:]


class ICMPv4TimeStampMessage(ICMPv4BaseMessage):
    """
    ICMPv4 timestamp request/response message format:
    --------------------------------------------------------------------------------------------------------
    |Type|Code|Checksum|Identifier|Sequence Number|Originate Timestamp|Receive Timestamp|Transmit Timestamp|
    --------------------------------------------------------------------------------------------------------
    | 1  | 1  |   2    |    2     |      2        |         4         |      4          |       4          |
    --------------------------------------------------------------------------------------------------------

    Type
                     13 for timestamp message;
                     14 for timestamp reply message.
    
    Code             0
    Checksum         The checksum is the 16-bit ones's complement of the one's
                     complement sum of the ICMP message starting with the ICMP Type.
                     For computing the checksum , the checksum field should be zero.
                     This checksum may be replaced in the future.
    Identifier       If code = 0, an identifier to aid in matching timestamp and
                     replies, may be zero.
    Sequence Number  If code = 0, a sequence number to aid in matching timestamp and
                     replies, may be zero.
    
    Description
                     The data received (a timestamp) in the message is returned in the
                     reply together with an additional timestamp.  The timestamp is 32
                     bits of milliseconds since midnight UT.  One use of these
                     timestamps is described by Mills.
                     
                     The Originate Timestamp is the time the sender last touched the
                     message before sending it, the Receive Timestamp is the time the
                     echoer first touched it on receipt, and the Transmit Timestamp is
                     the time the echoer last touched the message on sending it.

    Reference: https://www.rfc-editor.org/rfc/rfc792.txt
    """
    def __init__(self, mtype=0x0d, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.identifier = int(self.message_body[:2].hex(), 16)
            self.seq_number = int(self.message_body[2:4].hex(), 16)
            self.orig_timestamp = format_datetime(int(self.message_body[4:8].hex(), 16)//1000)
            self.recv_timestamp = format_datetime(int(self.message_body[8:12].hex(), 16)//1000)
            self.trans_timestamp = format_datetime(int(self.message_body[12:16].hex(), 16)//1000)


class ICMPv4InformationMessage(ICMPv4BaseMessage):
    """
    ICMPv4 information request/response message format:
    -----------------------------------------------
    |Type|Code|Checksum|Identifier|Sequence Number|
    -----------------------------------------------
    | 1  | 1  |   2    |    2     |      2        |
    -----------------------------------------------

    Type
                 15 for information request message;
                 16 for information reply message.
    Code         0
    Checksum     The checksum is the 16-bit ones's complement of the one's
                 complement sum of the ICMP message starting with the ICMP Type.
                 For computing the checksum , the checksum field should be zero.
                 This checksum may be replaced in the future.
    Identifier   If code = 0, an identifier to aid in matching request and replies,
                 may be zero.
    Sequence Number     
                 If code = 0, a sequence number to aid in matching request and
                 replies, may be zero.

    Reference: https://www.rfc-editor.org/rfc/rfc792.txt
    """
    def __init__(self, mtype=0x0F, code=0x00, checksum=0x00, message_body=b''):
        super().__init__(mtype=mtype, code=code, checksum=checksum, message_body=message_body)

    def parse_message_body(self):
        """
        Get other fields from body data
        """
        if self.message_body:
            self.identifier = int(self.message_body[:2].hex(), 16)
            self.seq_number = int(self.message_body[2:4].hex(), 16)

#------------------------------------------------------------------------
if __name__ == '__main__':
    pass
