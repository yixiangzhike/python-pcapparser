#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.error import ParamsError
from pcapparser.utils import format_ip2s
from pcapparser.constants import IP_HDR_FIX_LEN,IPV6_HDR_FIX_LEN,PROTOCOL_TYPES,IPV6_EXT_HEADER_TYPES
from pcapparser.option import Option
from pcapparser.ext_header import *


class IPOption(Option):
    """
    IPOptions field used in IPv4 Header and IPv6 Header.
    (only hop-by-hop optins header and destination options header)

    There are two cases for the format of an option:
        Case 1: A single byte of option 
        Case 2: Option-type (1byte), Option-length(1byte), Option-data

    Options format(TLV):
    ----------------------------------------------------
    | Option Type/Code | Options Length | Options Data |
    ----------------------------------------------------
    |       1byte      |       1byte    |       n      |
    ----------------------------------------------------
    case 1 : Single-byte options, type/code is 0(End of option) or 1(No operation)
    case 2 : Multiple-byte options

    IPv4 Options Type:
    b7     : Copy
             0 Copy only in first fragment
             1 Copy into all fragments
    b5-b6  : Class
             00 Datagram control
             01 Reserved
             10 Debugging and management
             11 Reserved
    b0-b4  : Number
             00000 End of Option
             00001 No operation
             00011 Loose source route
             00100 Timestamp
             00111 Recode route
             01001 Strict source route

    Ipv6 Options Type:
    b6-b7  : Action
             00 Skip this option
             01 Discard datagram, no more action
             10 Discard datagram and send ICMP message
             11 Discard datagram send ICMP message if not multicast
    b5     : Change
             0 Dose not change in transit
             1 May be changed in transit
    b0-b4  : Type
             00000 Pad1
             00001 PadN
             00010 Jumbo payload

    IPv4 Options Data Len include type,length,value
    IPv6 Options Data Len only the length of value
    """
    def __init__(self, opt_type=0x00, opt_length=0x00, opt_value=b'', protocol="ipv4"):
        """
        @opt_type
        @opt_length
        @opt_value
        @protocol
        """
        super().__init__(opt_type=opt_type, opt_length=opt_length, opt_value=opt_value, protocol=protocol)

class IPv4Option(IPOption):
    def __init__(self, opt_type=0x00, opt_length=0x00, opt_value=b'', protocol="ipv4"):
        """
        @opt_type
        @opt_length
        @opt_value
        @protocol
        """
        super().__init__(opt_type=opt_type, opt_length=opt_length, opt_value=opt_value, protocol="ipv4")


class IPv4FrameHeader(object):
    """
    Ipv4 Frame Header format:
    --------------------------------------------------------------------------------------------------------------
    Version| IHL  | Tos |Total Len|Ident |Flags|FragOffset|TTL  |Protocol|Checksum|S.IP   |D.IP  |options|padding|
    --------------------------------------------------------------------------------------------------------------
    4bits  |4bits |1byte|  2bytes |2bytes|3bits|   13bits |1byte|1byte   |2bytes  |4bystes|4bytes|   n   |  n    |
    --------------------------------------------------------------------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc791.txt
    """
    def __init__(self, version=0x00, hlen=0x00, tos=0x00, 
                total_len=0x00, ident=0x00, flag=0x00, fragment_offset=0x00,  
                ttl=0x00, protocol=0x00, checksum=0x00, sip=b'', dip=b'', 
                options=[], padding=b''):
        """
        @version    : ipv4 = 0x04
        @hlen       : Total header length, unit: 4bytes
        @tos        : Type of Service
        @total_len  : Total length of header and data, max length: 65535bytes
        @ident      : All fragments the same identification
        @flag       : Fragment flag, 3bits 
                      bit0: unused, 0
                      bit1-DF: 1-dont't fragment 0-fragment
                      bit2-MF: 1-more fragment  0-last fragment
        @fragment_offset : Fragment offset, the postion of per fragment in source message, unit: 8bytes
        @ttl        : Time to live, max count of router
        @protocol   : 1-ICMP  2-IGMP  6-TCP  17-UDP  41-IPv6  89-OSPF  136-UDPLITE
        @checksum   : Header checksum
        @sip        : Source IP address
        @dip        : Destination IP address
        @options    : option field, TLV format, max 40bytes
        @padding    : padding data, full 0
        """
        self.version = version
        self.hlen = hlen
        self.tos = tos
        self.total_len = total_len
        self.ident = ident
        self.flag = flag
        self.fragment_offset = fragment_offset
        self.ttl = ttl
        self.protocol = protocol
        self.checksum = checksum
        self.sip = sip
        self.dip = dip
        self.options = options
        self.padding = padding

    @classmethod
    def get_header_length(cls, header_data, size=IP_HDR_FIX_LEN):
        """
        Get header length from header data
        @header_data
        @size           : unused
        """
        if not header_data:
            raise ParamsError("Param header_data can not be None")

        if not isinstance(header_data, bytes):
            raise ParamsError()

        headerlen = (int(header_data[:1].hex(), 16) & 0x0F) << 2

        return headerlen

    @classmethod
    def create(cls, header_data, size=IP_HDR_FIX_LEN):
        """
        Create IPv4FrameHeader object
        @header_data  : the data of IPv4 header
        @size         : the length of IPv4 header
        """
        if not header_data:
            raise ParamsError("Param header_data can not be None")

        if not isinstance(header_data, bytes):
            raise ParamsError()

        hdrlen = cls.get_header_length(header_data[:1])
        opts = []
        if hdrlen > IP_HDR_FIX_LEN:
            opts_bytes = header_data[IP_HDR_FIX_LEN:hdrlen]
            pos = 0
            while True:
                opt_length = IPv4Option.get_option_length(opts_bytes[:2], protocol="ipv4")
                option = IPv4Option.create(opts_bytes[:opt_length], size=opt_length, protocol="ipv4")

                opts.append(option)

                # end of options list
                if not int(opts_bytes[:1].hex(), 16):
                    break

                pos += opt_length
                if hdrlen - IP_HDR_FIX_LEN <= pos:
                    break
                opts_bytes = opts_bytes[opt_length:]

        ipv4hdr = cls(version=(int(header_data[:1].hex(), 16) >> 4) & 0x0F,
                      hlen=hdrlen,
                      tos=int(header_data[1:2].hex(), 16),
                      total_len=int(header_data[2:4].hex(), 16),
                      ident=int(header_data[4:6].hex(), 16),
                      flag=(int(header_data[6:7].hex(), 16) >> 5) & 0x0F,
                      fragment_offset=int(header_data[6:8].hex(), 16) & 0x1FFF,
                      ttl=int(header_data[8:9].hex(), 16),
                      protocol=int(header_data[9:10].hex(), 16),
                      checksum=int(header_data[10:12].hex(), 16),
                      sip=header_data[12:16],
                      dip=header_data[16:20],
                      options=opts)

        return ipv4hdr

    def get_ipv4_total_length(self):

        return self.total_len

    def format_ips(self):
        # format IP address to be separated by '.'
        dip_show = format_ip2s(self.dip, ip_version=self.version)
        sip_show = format_ip2s(self.sip, ip_version=self.version)

        return (sip_show, dip_show)

    def format_outmsg(self, title="\033[32m IPv4 Header \033[0m", indent=8):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-7s | %-10s | %-3s | %-9s | %-5s | %-5s | %-15s | %-3s | %-8s | %-8s |"
        head_out = prefix + fmt % ("Version", "Header Len", "Tos", "Total Len", 
                                   "Ident", "Flags", "Fragment offset", "TTL",
                                   "Protocol", "Checksum")
        data_out = prefix + fmt % (str(self.version),
                                   str(self.hlen),
                                   str(self.tos),
                                   str(self.total_len),
                                   "%04X" % self.ident,
                                   str(self.flag),
                                   str(self.fragment_offset),
                                   str(self.ttl),
                                   "%d-%s" % (self.protocol, PROTOCOL_TYPES[self.protocol]),
                                   str(self.checksum))
        sep_num = 104
        (sip, dip) = self.format_ips()
        outmsg = (prefix + "|>>>  %s | %s ---> %s |" % (title, sip, dip)) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (head_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (data_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n"

        return outmsg
        
    def format_output(self, stdout, title="\033[32m IPv4 Header \033[0m", indent=8):
        """
        Output format message
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        if self.options:
            count = 0
            for option in self.options:
                # option.format_output(stdout, titile="\033[32m Option [%d] \033[0m"%count, indent=indent)
                if not count:
                    stdout.write(option.format_headmsg(indent=indent))
                stdout.write(option.format_bodymsg(indent=indent))
                count += 1



class IPv4Frame(object):
    """
    ----------------------------
    | IPv4 Header | Frame Data |
    ----------------------------
    """
    def __init__(self, frmhdr=None, frmdata=None):
        """
        @frmhdr  : The object of IPv4FrameHeader.
        @frmdata : Data.
        """
        self.frmhdr = frmhdr
        self.frmdata = frmdata



class IPv6FrameHeader(object):
    """
    Ipv6 Frame Header format:
    ---------------------------------------------------------------------------------------------------------------
    | Version | Traffic class | Flow Label | Payload Len | Next header | Hop limit | S.ADDR | D.ADDR | Ext header |
    ---------------------------------------------------------------------------------------------------------------
    | 4bits   |   8bits       |   20bits   |   2bytes    |    1byte    |   1byte   |16bytes | 16bytes|    n       |
    ---------------------------------------------------------------------------------------------------------------
    
    Reference: https://www.rfc-editor.org/rfc/rfc2460.txt

    Extension Headers order:
        0      Hop-by-Hop Options header
        60     Destination Options header
        43     Routing header
        44     Fragment header
        51     Authentication header
        50     Encapsulating Security Payload header
        60     Destination Options header
        59     No next header
               upper-layer header:
                     6  - TCP
                     17 - UDP
                     58 - ICMPv6
    """
    def __init__(self, version=0x00, traffic_class=0x00, flow_label=0x00,
                payload_len=0x00, next_header=0x00, hop_limit=0x00, saddr=b'', daddr=b'', ext_headers=[]):
        """
        @version        : ipv6 = 0x06
        @traffic_class  : Like the TOS in ipv4
        @flow_label     : Traffic Label,All datagrams belonging to
                          the same stream have the same flow label
        @payload_len    : The length of ext headers and data
        @next_header    : Next header, which indicates the information type 
                          of the extended header following the IPv6 basic header
        @hop_limit      : Hop limit, which defines the maximum number of hops that 
                          an IPv6 packet can pass through
        @saddr          : Source IPv6 address
        @daddr          : Destination IPv6 address
        @ext_headers    : More extension headers
        """
        self.version = version
        self.traffic_class = traffic_class
        self.flow_label = flow_label
        self.payload_len = payload_len
        self.next_header = next_header
        self.hop_limit = hop_limit
        self.saddr = saddr
        self.daddr = daddr
        self.ext_headers = ext_headers

    @classmethod
    def get_payload_length(self, header_data, size=IPV6_HDR_FIX_LEN):
        """
        Get payload length from base header data
        @header_data : base header data in ipv6 header
        @size        : min IPV6_HDR_FIX_LEN
        """
        if not header_data:
            raise ParamsError("Param header_data can not be None")

        if not isinstance(header_data, bytes):
            raise ParamsError()

        if len(header_data) < IPV6_HDR_FIX_LEN:
            raise ParamsError()

        return int(header_data[4:6].hex(), 16)

    @classmethod
    def create(cls, header_data, size=IPV6_HDR_FIX_LEN):
        """
        Create IPv6FrameHeader object
        @header_data  : the data of IPv6 header
        @size         : the length of IPv6 header
        """
        if not header_data:
            raise ParamsError("Param header_data can not be None")

        if not isinstance(header_data, bytes):
            raise ParamsError()

        # extension headers
        ext_headers=[]
        next_hdr=int(header_data[6:7].hex(), 16)
        current_hdr = next_hdr
        if not current_hdr in [6,17,58]:
            ext_data = header_data[40:]
            extheader=None
            while True:
                if current_hdr in [6,17,58]:
                    break

                next_hdr = int(ext_data[:1].hex(), 16)
                if current_hdr == 0:
                    # Hop-by-Hop extension header
                    ext_length = int(ext_data[1:2].hex(), 16)
                    real_length = 8 * (int(ext_data[1:2].hex(), 16) + 1)
                    ext_header_data = ext_data[2:real_length]
                    extheader = HBHExtHeader(current_header=current_hdr,
                                            next_header=next_hdr,
                                            header_length=ext_length,
                                            header_data=ext_header_data)
                elif current_hdr == 60:
                    # Destination Options extension header
                    ext_length = int(ext_data[1:2].hex(), 16)
                    real_length = 8 * (int(ext_data[1:2].hex(), 16) + 1)
                    ext_header_data = ext_data[2:real_length]
                    extheader = DestOptExtHeader(current_header=current_hdr,
                                                next_header=next_hdr,
                                                header_length=ext_length,
                                                header_data=ext_header_data)
                else:
                    pass

                extheader.parse_extension_header_data()
                ext_headers.append(extheader)
                current_hdr = next_hdr

        ipv6hdr = cls(version=(int(header_data[:1].hex(), 16) >> 4) & 0x0F,
                      traffic_class=(int(header_data[:2].hex(), 16) >> 4) & 0xFF,
                      flow_label=int(header_data[1:4].hex(), 16) & 0x0FFFFF,
                      payload_len=cls.get_payload_length(header_data),
                      next_header=int(header_data[6:7].hex(), 16),
                      hop_limit=int(header_data[7:8].hex(), 16),
                      saddr=header_data[8:24],
                      daddr=header_data[24:40],
                      ext_headers=ext_headers)

        return ipv6hdr

    def get_uplayer_offset(self):
        """
        Get the data offset in IPv6 frame
        """
        if not self.ext_headers:
            return IPV6_HDR_FIX_LEN

        offset = 0
        for exthdr in self.ext_headers:
            offset += 8 * (exthdr.header_length + 1)

        return offset + IPV6_HDR_FIX_LEN

    def format_ips(self):
        # format IP address to be separated by '.'
        dip_show = format_ip2s(self.daddr, ip_version=self.version)
        sip_show = format_ip2s(self.saddr, ip_version=self.version)

        return (sip_show, dip_show)

    def format_outmsg(self, title="\033[32m IPv6 Header \033[0m", indent=8):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-7s | %-13s | %-10s | %-11s | %-20s | %-9s |"
        head_out = prefix + fmt % ("Version", "Traffic Class", "Flow Label", "Payload Len", 
                          "Next Header", "Hop Limit")
        data_out = prefix + fmt % (str(self.version),
                                  str(self.traffic_class),
                                  "%04X" % self.flow_label,
                                  str(self.payload_len),
                                  "%d-%s" % (self.next_header, IPV6_EXT_HEADER_TYPES[self.next_header].split()[0]),
                                  str(self.hop_limit))

        sep_num = 90
        sip, dip = self.format_ips()
        outmsg = prefix + "|>>> %s | %s --> %s |" % (title, sip, dip) + "\n" \
                + prefix + "-"*sep_num + "\n" \
                + head_out + "\n" \
                + prefix + "-"*sep_num + "\n" \
                + data_out + "\n" \
                + prefix + "-"*sep_num + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m IPv6 Header \033[0m", indent=8):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        if self.ext_headers:
            count = 0
            for ext_header in self.ext_headers:
                ext_header.format_output(stdout, title="\033[32m Extension Header [%d] \033[0m" % count, indent=indent)
                count += 1

class IPv6Frame(object):
    """
    ----------------------------
    | IPv6 Header | Frame Data |
    ----------------------------
    """
    def __init__(self, frmhdr=None, frmdata=None):
        """
        @frmhdr  : The object of IPv6FrameHeader.
        @frmdata : Data.
        """
        self.frmhdr = frmhdr
        self.frmdata = frmdata

#------------------------------------------------------------------------
if __name__ == '__main__':
    pass
