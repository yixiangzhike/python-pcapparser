#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

import re
import time
import socket
import datetime
from sys import version

from pcapparser.error import ParamsError
from pcapparser.constants import HEX_CHARS

# check hex string
def check_hex_chars(hex_chars=""):
    """
    Check chars is all hex char.
    @hex_chars : hex string
    """
    if not hex_chars:
        return False

    for char in list(hex_chars):
        if not str(char).upper() in list(HEX_CHARS):
            return False

    return True

# format MAC
def conv_format_mac(mac=None):
    """
    @mac
    """
    if isinstance(mac, bytes):
        if version.split()[0] >= '3.8':
            return mac.hex(":").upper()
        else:
            mac_hex = mac.hex().upper()

    if isinstance(mac, int):
        mac_hex = "%012X" % mac

    if isinstance(mac, str):
        if not check_hex_chars(hex_chars=mac):
            raise ParamsError("Param mac must be hex string")
        mac_hex = mac

    return ":".join(re.findall(r'.{2}', mac_hex))

# format IP
def format_ip2s(ip, ip_version):
    """
    Format ip to string
    @ip
    @ip_version
    """
    if not ip_version in [4,6]:
        raise ParamsError("Param ip_version error")

    if not isinstance(ip, bytes):
        raise ParamsError()

    if ip_version == 4:
        return socket.inet_ntop(socket.AF_INET, ip)
    else:
        return socket.inet_ntop(socket.AF_INET6, ip)

# format datetime
def format_datetime(seconds):
    if isinstance(seconds, int):
        ldt = time.localtime(seconds)

    if isinstance(seconds, str):
        if seconds[:2] in ['0x', '0X']:
            if not check_hex_chars(hex_chars=seconds[:2]):
                return None
        else:
            if not check_hex_chars(hex_chars=seconds):
                return None
        ldt = time.localtime(int(seconds, 16))

    return time.strftime("%Y-%m-%d %H:%M:%S", ldt)

# format datetime string
def format_str_datetime(datetime_str):
    """
    Format datetime string to number-string.
    Drop the separater chars in datetime string.
    @datetime_str : YYYY/MM/DD HH:MM:SS
                    YYYY-MM-DD HH:MM:SS
    """
    dt_string = datetime_str
    for ch in ['/','-',' ',':']:
        dt_string = dt_string.replace(ch, '')

    return dt_string

# convert datetime string to seconds
def convert_timestamp(datetime_str):
    """
    Convert datetime string to seconds
    @datetime_str : YYYY/MM/DD HH:MM:SS
                    YYYY-MM-DD HH:MM:SS
                    YYYYMMDDHHMMSS
    """

    dtstr = format_str_datetime(datetime_str)

    if not len(dtstr) in [8,14]:
        raise ParamsError("Datetime format: YYYYMMDD or YYYYMMDDHHMMSS")

    if not dtstr.isdigit():
        raise ParamsError("Datetime format: YYYYMMDD or YYYYMMDDHHMMSS")

    seconds = 0
    elements = re.findall(r'\d{2}', dtstr)
    if len(dtstr) == 8:
        seconds = datetime.datetime(int(elements[0]+elements[1]), int(elements[2]), int(elements[3])).timestamp()
    elif len(dtstr) == 14:
        seconds = datetime.datetime(int(elements[0]+elements[1]), int(elements[2]), int(elements[3]),
                                    int(elements[4]), int(elements[5]), int(elements[6])).timestamp()

    return seconds
    
# Convert hex string to ascii
def convert_hex2ascii(hex_str):
    """
    Convert hex to ascii
    @hex_str
    """
    if not hex_str:
        return ""

    if not check_hex_chars(hex_chars=hex_str):
        raise ParamsError()

    ascii_str = ""
    for i in range(0, len(hex_str), 2):
        ascii_str += chr(int(hex_str[i:i+2], 16))

    return ascii_str

#------------------------------------------------------------------------------
if __name__ == '__main__':
    mac_hex="68f728fd85a8"
    print(conv_format_mac(mac_hex))
    print(conv_format_mac(bytes.fromhex(mac_hex)))

    dt = "2023/09/01"
    dt1= "2023-09-01 21:15:00"
    dt2= "20230902094000"

    print(convert_timestamp(dt))
    print(convert_timestamp(dt1))
    print(convert_timestamp(dt2))

    hex_str = "696e2d61646472"
    print(convert_hex2ascii(hex_str))

