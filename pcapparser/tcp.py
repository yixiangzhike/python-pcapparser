#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.error import ParamsError
from pcapparser.constants import TCP_HDR_FIX_LEN
from pcapparser.option import Option


class TCPOption(Option):
    """
    TCPOption format(TLV):
    ------------------------------------------------
    | Option Type  | Options Length | Options Data |
    ------------------------------------------------
    |   1byte      |       1byte    |       n      |
    ------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc9293.txt
    """
    def __init__(self, opt_type=0x00, opt_length=0x00, opt_value=b'', protocol="tcp"):
        """
        @opt_type
        @opt_length
        @opt_value
        @protocol
        """
        super().__init__(opt_type=opt_type, opt_length=opt_length, opt_value=opt_value, protocol="tcp")

class TCPFrameHeader(object):
    """
    TCP Frame Header format:
    -------------------------------------------------------------------------------------------
    |S.PORT|D.PORT|SEQ-NUM|ACK-NUM|H-Length|Reserve|Control|Win size|Checksum|Urgent|Options  |
    -------------------------------------------------------------------------------------------
    |2bytes|2bytes|4bytes |4bytes |4bits   |6bits  | 6bits | 2bytes |2bytes| 2bytes |0-40bytes|
    -------------------------------------------------------------------------------------------

    Reference: https://www.rfc-editor.org/rfc/rfc9293.txt
    """
    def __init__(self, sport=0x00, dport=0x00, seqnum=0x00, 
                acknum=0x00, hlen=0x00, reserve=0x00, control=0x00, 
                winsize=0x00, checksum=0x00, urgent=0x00, options=[]):
        """
        @sport   : Source PORT
        @dport   : Destination PORT
        @seqnum  : Sequence Number, the seq-number of first byte in current message
        @acknum  : Acknowledgment Number, enable if ack-flag is 1 
        @hlen    : Header Length, data offset, include options length, unit: 4bytes
        @reserve : All 0 now
        @control : One bit per field, URG|ACK|PSH|RST|SYN|FIN
        @winsize : Window size, TCP stream size control, start with acknum value, max 65535bytes
        @checksum: Checksum, include header and data
        @urgent  : Only enable when URG flag set 1, the size of urgent data which is front at data
        @options : Option fields, max length 40bytes
        """
        self.sport = sport
        self.dport = dport
        self.seqnum = seqnum
        self.acknum = acknum
        self.hlen = hlen
        self.reserve = reserve
        self.control = control
        self.winsize = winsize
        self.checksum = checksum
        self.urgent = urgent
        self.options = options

    @classmethod
    def get_header_length(cls, header_data):
        """
        Get header length from header data
        @header_data
        """
        if not header_data:
            raise ParamsError("Param header_data can not be None")

        if not isinstance(header_data, bytes):
            raise ParamsError()

        if len(header_data) < 12:
            raise ParamsError("Length of param header_data error")

        hlen = ((int(header_data[12:13].hex(), 16) >> 4) & 0x0F) << 2

        return hlen


    @classmethod
    def create(cls, header_data, size=TCP_HDR_FIX_LEN):
        """
        Create TCPFrameHeader object
        @header_data : the data of tcp frame header
        @size        : the length of tcp frame header
        """
        if not header_data or size < TCP_HDR_FIX_LEN:
            raise ParamsError("Param header_data or size error")

        if not isinstance(header_data, bytes):
            raise ParamsError()

        header_length = cls.get_header_length(header_data)
        opts = []
        if header_length > TCP_HDR_FIX_LEN:
            opts_bytes = header_data[TCP_HDR_FIX_LEN:header_length]
            pos = 0
            while True:
                opt_length = TCPOption.get_option_length(opts_bytes[:2], protocol="tcp")
                option = TCPOption.create(opts_bytes[:opt_length], size=opt_length, protocol="tcp")
        
                opts.append(option)
        
                # end of options list
                if not int(opts_bytes[:1].hex(), 16):
                    break
        
                pos += opt_length
                if header_length - TCP_HDR_FIX_LEN <= pos:
                    break
                opts_bytes = opts_bytes[opt_length:]

        tcphdr = cls(sport=int(header_data[:2].hex(), 16),
                     dport=int(header_data[2:4].hex(), 16),
                     seqnum=int(header_data[4:8].hex(), 16),
                     acknum=int(header_data[8:12].hex(), 16),
                     hlen=header_length,
                     reserve=(int(header_data[12:14].hex(), 16) >> 6) & 0x3F,
                     control=int(header_data[12:14].hex(), 16) & 0x3F,
                     winsize=int(header_data[14:16].hex(), 16),
                     checksum=int(header_data[16:18].hex(), 16),
                     urgent=int(header_data[18:20].hex(), 16),
                     options=opts)

        return tcphdr

    def format_outmsg(self, title="\033[32m TCP Header \033[0m", indent=12):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "| %-6s | %-6s | %-10s | %-10s | %-5s| %-7s | %-8s | %-8s | %-8s | %-6s |"
        head_out = prefix + fmt % ("S.PORT", "D.PORT", "SEQ-NUM", "ACK-NUM", "HLen", 
                                   "Reserve", "Control", "Win size", "Checksum", "Urgent")
        data_out = prefix + fmt % (str(self.sport), str(self.dport), str(self.seqnum), str(self.acknum), 
                                   str(self.hlen), str(self.reserve), str(bin(self.control)), 
                                   str(self.winsize), str(self.checksum), str(self.urgent))
        sep_num = 104

        outmsg = (prefix + "|>>>  %s  | CTRL: 0b111111 - URG|ACK|PSH|RST|SYN|FIN  |" % title) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (head_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (data_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m TCP Header \033[0m", indent=12):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        if self.options:
            count = 0
            for option in self.options:
                # option.format_output(stdout, title="\033[32m Option [%d] \033[0m"%count, indent=indent)
                if not count:
                    stdout.write(option.format_headmsg(indent=indent))
                stdout.write(option.format_bodymsg(indent=indent))
                count += 1


class TCPFrame(object):
    """
    TCP frame format:
    -----------------------------
    | Frame Header | Frame Data |
    -----------------------------
    |  20-60bytes  |    n       |
    -----------------------------
    """
    def __init__(self, frmhdr=None, frmdata=None):
        """
        @frmhdr  : The object of EtherFrameHeader.
        @frmdata : Ether frame data.
        """
        self.frmhdr = frmhdr
        self.frmdata = frmdata

#------------------------------------------------------------------------
if __name__ == '__main__':
    pass
