#!/usr/bin/env python3

"""
Copyright (c) 2023-08-10 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

# pcap file magic and byteorder
PCAP_MAGIC_LITTLE = 0xd4c3b2a1
PCAP_MAGIC_BIG    = 0xa1b2c3d4
# pcapng file magic
PCAPNG_MAGIC = 0x0a0d0d0a
# pcapng file byteorder
PCAPNG_BYTEORDER_LITTLE = 0x4d3c2b1a
PCAPNG_BYTEORDER_BIG    = 0x1a2b3c4d

# hex string charset
HEX_CHARS="0123456789ABCDEF"

# pcap file header size
PCAP_FILE_HDR_LEN = 24

# packet header size
PACKET_HDR_LEN = 16

# ether frame header size
FRAME_ETHII_HDR_LEN = 14
FRAME_IEEE802_LLC_HDR_LEN = 17
FRAME_IEEE802_SNAP_HDR_LEN = 22

# ether frame class
FRAME_ETHERNETII = 0
FRAME_IEEE802_LLC = 1
FRAME_IEEE802_SNAP = 2

# IP frame header size
IP_HDR_FIX_LEN = 20
IPV6_HDR_FIX_LEN = 40

# TCP frame header size
TCP_HDR_FIX_LEN = 20

# UDP frame header size
UDP_HDR_FIX_LEN = 8

# IGMP base length
IGMP_BASE_LEN = 8

# MLD base length
MLD_BASE_LEN = 24

# ARP frame size
ARP_FRAME_LEN = 28

# STP protocol MAC
BPDU_MAC = 0x0180c2000000

# STP message size
STP_MESSAGE_FIX_LEN = 35
MSTP_MESSAGE_FIX_LEN = 102


# linkt types
LINK_TYPES = {
    0         :        'BSD loopback devices, except for later OpenBSD',
    1         :        'Ethernet, and Linux loopback devices',
    6         :        '802.5 Token Ring',
    7         :        'ARCnet',
    8         :        'SLIP',
    9         :        'PPP',
    10        :        'FDDI', 
    100       :        'LLC/SNAP-encapsulated ATM', 
    101       :        'raw IP, with no link',
    102       :        'BSD/OS SLIP',
    103       :        'BSD/OS PPP',
    104       :        'Cisco HDLC',
    105       :        '802.11',
    108       :        'later OpenBSD loopback devices (with the AF_value in network byte order)',
    113       :        'special Linux cooked capture',
    114       :        'LocalTalk'
}


# ethernet types
# https://www.iana.org/assignments/ieee-802-numbers/ieee-802-numbers.xhtml
ETHER_TYPES = {
    0x0800      : 'Internet Protocol (IP)',
    0x0801      : 'X.75 Internet',
    0x0805      : 'X.25 Level 3',
    0x0806      : 'Address Resolution Protocol (ARP)',
    0x0808      : 'Frame Relay ARP',
    0x8000      : 'IS-IS',
    0x8035      : 'Reverse Address Resolution Protocol (RARP)',
    0x8137      : 'Novell NetWare IPX/SPX (old)',
    0x8138      : 'Novell, Inc.',
    0x8100      : 'IEEE Std 802.1Q - Customer VLAN Tag Type',
    0x814C      : 'SNMP over Ethernet',
    0x86DD      : 'IP Protocol version 6 (IPv6)',
    0x8808      : 'IEEE Std 802.3 - Ethernet Passive Optical Network (EPON)',
    0x880B      : 'Point-to-Point Protocol (PPP)',
    0x880C      : 'General Switch Management Protocol (GSMP)',
    0x8847      : 'MPLS (multiprotocol label switching) label stack - unicast',
    0x8848      : 'MPLS (multiprotocol label switching) label stack - multicast',
    0x8863      : 'PPP over Ethernet (PPPoE) Discovery Stage',
    0x8864      : 'PPP over Ethernet (PPPoE) Session Stage',
    0x888E      : 'IEEE Std 802.1X - Port-based network access control',
    0x88A8      : 'IEEE Std 802.1Q - Service VLAN tag identifier (S-Tag)',
    0x88B7      : 'IEEE Std 802 - OUI Extended Ethertype', 
    0x88C7      : 'IEEE Std 802.11 - Pre-Authentication (802.11i)',
    0x88CC      : 'IEEE Std 802.1AB - Link Layer Discovery Protocol (LLDP)',
    0x88E5      : 'IEEE Std 802.1AE - Media Access Control Security', 
    0x88F5      : 'IEEE Std 802.1Q - Multiple VLAN Registration Protocol (MVRP)', 
    0x88F6      : 'IEEE Std 802.1Q - Multiple Multicast Registration Protocol (MMRP)',
    0x9998      : 'Loopback Detection',
}


# protocol types
# https://www.iana.org/assignments/ieee-802-numbers/ieee-802-numbers.xhtml
PROTOCOL_TYPES = {
    0    :  'Reserved',
    1    :  'ICMP',
    2    :  'IGMP',
    3    :  'GGP',
    4    :  'IP in IP',
    6    :  'TCP',
    17   :  'UDP',
    20   :  'HMP',
    27   :  'RDP',
    41   :  'IPv6 in IP',
    46   :  'RSVP',
    47   :  'GRE',
    50   :  'ESP',
    51   :  'AH',
    54   :  'NARP',
    58   :  'IPv6-ICMP',
    59   :  'IPv6-NoNxt',
    60   :  'IPv6-Opts',
    89   :  'OSPF',
    103  :  'PIM',
    112  :  'VRRP',
    115  :  'L2TP',
    124  :  'ISIS over IPv4',
    126  :  'CRTP',
    127  :  'CRUDP',
    132  :  'SCTP',
    136  :  'UDPLite',
}

# IPv4 options types
IPV4_OPTIONS_TYPES = {
    0   : 'EOL',
    1   : 'NOP',
    7   : 'Record Route',
    68  : 'Timestamp',
    130 : 'Security',
    131 : 'LSR',
    137 : 'SSR',
    148 : 'Router Alert',
}

# IPv6 options types
IPV6_OPTIONS_TYPES = {
    0x00 : 'Pad1',
    0x01 : 'PadN',
    0x05 : 'Router Alert',
    0xc2 : 'Jumbo Palyload',
    0xc9 : 'Home Address',
}

# arp hardware types
# https://www.iana.org/assignments/arp-parameters/arp-parameters.xhtml
ARP_HARDWARE_TYPES = {
    0     : "Reserved",
    1     : "Ethernet (10Mb)",
    2     : "Experimental Ethernet (3Mb)",
    3     : "Amateur Radio AX.25",
    4     : "Proteon ProNET Token Ring",
    5     : "Chaos",
    6     : "IEEE 802 Networks",
    7     : "ARCNET",
    8     : "Hyperchannel",
    9     : "Lanstar",
    10    : "Autonet Short Address",
    11    : "LocalTalk",
    12    : "LocalNet (IBM PCNet or SYTEK LocalNET)",
    13    : "Ultra link",
    14    : "SMDS",
    15    : "Frame Relay",
    16    : "Asynchronous Transmission Mode (ATM)",
    17    : "HDLC",
    18    : "Fibre Channel",
    19    : "Asynchronous Transmission Mode (ATM)",
    20    : "Serial Line",
    21    : "Asynchronous Transmission Mode (ATM)",
    22    : "MIL-STD-188-220",
    23    : "Metricom",
    24    : "IEEE 1394.1995",
    25    : "MAPOS",
    26    : "Twinaxial",
    27    : "EUI-64",
    28    : "HIPARP",
    29    : "IP and ARP over ISO 7816-3",
    30    : "ARPSec",
    31    : "IPsec tunnel",
    32    : "InfiniBand (TM)",
    33    : "TIA-102 Project 25 Common Air Interface (CAI)",
    34    : "Wiegand Interface",
    35    : "Pure IP",
    36    : "HW_EXP1",
    37    : "HFI",
    38    : "Unified Bus (UB)",
    255   : "Unassigned",
    256   : "HW_EXP2",
    257   : "AEthernet",
}

# tcp options types
TCP_OPTIONS_TYPES = {
    0    : 'EOL', 
    1    : 'NOP',
    2    : 'MSS',
    3    : 'WSOPT',
    4    : 'SACK-Premitted',
    5    : 'SACK',
    8    : 'TSPOT',
    19   : 'TCP-MD5',
    28   : 'UTO',
    29   : 'TCP-AO',
    253  : 'Experimental',
    254  : 'Experimental',
}

# icmpv6 options types
"""
1       Source Link-layer Address               [RFC4861]
2       Target Link-layer Address               [RFC4861]
3       Prefix Information                      [RFC4861]
4       Redirected Header                       [RFC4861]
5       MTU                                     [RFC4861]
6       NBMA Shortcut Limit Option              [RFC2491]
7       Advertisement Interval Option           [RFC3775]
8       Home Agent Information Option           [RFC3775]
9       Source Address List                     [RFC3122]
10      Target Address List                     [RFC3122]
11      CGA option                              [RFC3971]
12      RSA Signature option                    [RFC3971]
13      Timestamp option                        [RFC3971]
14      Nonce option                            [RFC3971]
15      Trust Anchor option                     [RFC3971]
16      Certificate option                      [RFC3971]
17      IP Address/Prefix Option                [RFC5568]
18      New Router Prefix Information Option    [RFC4068]
19      Link-layer Address Option               [RFC5568]
20      Neighbor Advertisement Acknowledgment   [RFC5568]
21-22   Unassigned
23      MAP Option                              [RFC4140]
24      Route Information Option                [RFC4191]
25      Recursive DNS Server Option             [RFC5006]
26      RA Flags Extension Option               [RFC5175]
27      Handover Key Request Option             [RFC5269]
28      Handover Key Reply Option               [RFC5269]
29      Handover Assist Information Option      [RFC5271]
30      Mobile Node Identifier Option           [RFC5271]
31      DNS Search List Option                  [RFC-ietf-6man-dns-options-bis-08.txt]
32-137  Unassigned
138     CARD Request option                     [RFC4065]
139     CARD Reply option                       [RFC4065]
140-252 Unassigned
253     RFC3692-style Experiment 1 (*)          [RFC4727]
254     RFC3692-style Experiment 2 (*)          [RFC4727]
"""
ICMPV6_OPTIONS_TYPES = {
    1     : 'Source Link-Layer Address',
    2     : 'Target Link-Layer Address',
    3     : 'Prefix Information',
    4     : 'Redirected Header',
    5     : 'MTU',
    6     : 'NBMA Shortcut Limit Option',
    7     : 'Advertisement Interval Option',
    8     : 'Home Agent Information Option',
    9     : 'Source Address List',
    10    : 'Target Address List',
    11    : 'CGA option',
    12    : 'RSA Signature option',
    13    : 'Timestamp option',
    14    : 'Nonce option',
    15    : 'Trust Anchor option',
    16    : 'Certificate option',
    17    : 'IP Address/Prefix Option',
    18    : 'New Router Prefix Information Option',
    19    : 'Link-layer Address Option',
    20    : 'Neighbor Advertisement Acknowledgment',
    23    : 'MAP Option',
    24    : 'Route Information Option',
    25    : 'Recursive DNS Server Option',
    26    : 'RA Flags Extension Option',
    27    : 'Handover Key Request Option',
    28    : 'Handover Key Reply Option',
    29    : 'Handover Assist Information Option',
    30    : 'Mobile Node Identifier Option',
    31    : 'DNS Search List Option',
    138   : 'CARD Request option',
    139   : 'CARD Reply option',
    253   : 'RFC3692-style Experiment 1',
    254   : 'RFC3692-style Experiment 2',
}

# IPv6 extension header types
IPV6_EXT_HEADER_TYPES = {
    0   :   'Hop-by-Hop Options header',
    60  :   'Destination Options header',
    43  :   'Routing header',
    44  :   'Fragment header',
    51  :   'Authentication header',
    50  :   'Encapsulating Security Payload header',
    60  :   'Destination Options header',
    6   :   'TCP',
    17  :   'UDP',
    58  :   'ICMPv6',
    59  :   'No next header',
}

# IGMP types
IGMP_TYPES = {
    0x11  : 'Query IGMP',
    0x12  : 'Report IGMPv1',
    0x16  : 'Report IGMPv2',
    0x17  : 'Leave Group',
    0x22  : 'Report IGMPv3',
}

# IGMP group record types
# https://www.rfc-editor.org/rfc/rfc3376#section-4.2.12
IGMP_GROUP_RECORD_TYPES = {
    1    :   'MODE_IS_INCLUDE',
    2    :   'MODE_IS_EXCLUDE',
    3    :   'CHANGE_TO_INCLUDE_MODE',
    4    :   'CHANGE_TO_EXCLUDE_MODE',
    5    :   'ALLOW_NEW_SOURCES',
    6    :   'BLOCK_OLD_SOURCES',
}

# STP version
STP_VERSIONS = {
        0  : 'STP',
        2  : 'RSTP',
        3  : 'MSTP',
}

# ICMPv6 message types
# typecode : description
ICMPV6_MESSAGE_TYPES = {
    '10'   :   'No route to destination',
    '11'   :   'Communication with destination administratively prohibited',
    '12'   :   'Beyond scope of source address',
    '13'   :   'Address unreachable',
    '14'   :   'Port unreachable',
    '15'   :   'Source address failed ingress/egress policy',
    '16'   :   'Reject route to destination',
    '20'   :   'Packet Too Big',
    '30'   :   'Hop limit timeout in transit',
    '31'   :   'Fragment reassembly timeout',
    '40'   :   'Erroneous header field encountered',
    '41'   :   'Unrecognized Next Header type encountered',
    '42'   :   'Unrecognized IPv6 option encountered',
    '1000' :   'Private reserved',
    '1010' :   'Private reserved',
    '1270' :   'ICMPv6 error message extension reserved',
    '1280' :   'Echo request',
    '1290' :   'Echo reply',
    '1300' :   'Multicast Listener Query',
    '1310' :   'Multicast Listener Report',
    '1320' :   'Multicast Listener Done',
    '1330' :   'Router Solicitation',
    '1340' :   'Router Advertisement',
    '1350' :   'Neighbor Solicitation',
    '1360' :   'Neighbor Advertisement',
    '1370' :   'Redirect Message',
    '1410' :   'Inverse ND Solicitation',
    '1420' :   'Inverse ND Advertisement',
    '1430' :   'MLDv2 (Multicast Listener Report Message v2)',
    '2000' :   'Private reserved',
    '2010' :   'Private reserved',
    '2550' :   'ICMPv6 information message extension reserved',
}

# ICMPv4 message types
# typecode : description
ICMPV4_MESSAGE_TYPES = {
    '00'    :   "Echo Reply",
    '30'    :   "Network Unreachable",
    '31'    :   "Host Unreachable",
    '32'    :   "Protocol Unreachable",
    '33'    :   "Port Unreachable",
    '34'    :   "Fragmentation needed but no frag.",
    '35'    :   "Source routing failed",
    '36'    :   "Destination network unknown",
    '37'    :   "Destination host unknown",
    '38'    :   "Source host isolated (obsolete)",
    '39'    :   "Destination network administratively prohibited",
    '310'   :   "Destination host administratively prohibited",
    '311'   :   "Network unreachable for TOS",
    '312'   :   "Host unreachable for TOS",
    '313'   :   "Communication administratively prohibited by filtering",
    '314'   :   "Host precedence violation",
    '315'   :   "Precedence cutoff in effect",
    '40'    :   "Source quench",
    '50'    :   "Redirect for network",
    '51'    :   "Redirect for host",
    '52'    :   "Redirect for TOS and network",
    '53'    :   "Redirect for TOS and host",
    '80'    :   "Echo request",
    '90'    :   "Router advertisement",
    '100'   :   "Route solicitation",
    '110'   :   "TTL equals 0 during transit",
    '111'   :   "TTL equals 0 during reassembly",
    '120'   :   "IP header bad (catchall error)",
    '121'   :   "Required options missing",
    '130'   :   "Timestamp request (obsolete)",
    '140'   :   "Timestamp reply (obsolete)",
    '150'   :   "Information request (obsolete)",
    '160'   :   "Information reply (obsolete)",
    '170'   :   "Address mask request",
    '180'   :   "Address mask reply",
}
