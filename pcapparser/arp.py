#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.error import ParamsError
from pcapparser.utils import format_ip2s
from pcapparser.constants import ARP_FRAME_LEN,ARP_HARDWARE_TYPES

class ARPFrame(object):
    """
    ARP protocol only used in IPv4.
    Ethernet Type : 0x0806

    ARP Frame format:
    --------------------------------------------------------------------------------------------------------
    | HardWare Type | Protocol Type | HardWare Size | Protocol Size | Opcode | S.MAC | S.IP | D.MAC | D.IP |
    --------------------------------------------------------------------------------------------------------
    |    2bytes     |     2bytes    |     1bytes    |      1byte    |  2byte | 6bytes|4bytes|6bytes |4bytes|
    --------------------------------------------------------------------------------------------------------

    Opcode: 1-ARP Request   2-ARP Response  3-RARP Request   4-RARP Response

    Reference: https://www.rfc-editor.org/rfc/rfc6747.txt
    """
    def __init__(self, hwtype=0x00, ptotype=0x00, hwsize=0x00, ptosize=0x00,
                opcode=0x00, smac=b'', sip=b'', dmac=b'', dip=b''):
        """
        @hwtype  : HardWare Type
        @ptotype : Protocol Type
        @hwsize  : HardWare address bytes
        @ptosize : Protocol address bytes
        @opcode  : 1-ARP Request   2-ARP Response  3-RARP Request   4-RARP Response
        @smac    : Source MAC
        @sip     : Source IP
        @dmac    : Destination MAC
        @dip     : Destination IP
        """
        self.hwtype = hwtype
        self.ptotype = ptotype
        self.hwsize = hwsize
        self.ptosize = ptosize
        self.opcode = opcode
        self.smac = smac
        self.sip = sip
        self.dmac = dmac
        self.dip = dip

    @classmethod
    def create(cls, arpdata, size=ARP_FRAME_LEN):
        """
        Create ARPFrame object
        @arpdata: the data of frame 
        @size: the length of frame data
        """
        if not arpdata or size < ARP_FRAME_LEN:
            raise ParamsError()

        if not isinstance(arpdata, bytes):
            raise ParamsError()

        arpfrm = cls(hwtype=int(arpdata[:2].hex(), 16),
                     ptotype=int(arpdata[2:4].hex(), 16),
                     hwsize=int(arpdata[4:5].hex(), 16),
                     ptosize=int(arpdata[5:6].hex(), 16),
                     opcode=int(arpdata[6:8].hex(), 16),
                     smac=arpdata[8:14],
                     sip=arpdata[14:18],
                     dmac=arpdata[18:24],
                     dip=arpdata[24:28])

        return arpfrm

    def format_macs(self):
        # format mac address to be separated by ':'
        fmt = "%02X:%02X:%02X:%02X:%02X:%02X"

        return (fmt % tuple(self.smac), fmt % tuple(self.dmac))

    def format_ips(self):
        # format IP address to be separated by '.'
        dip_show = format_ip2s(self.dip, ip_version=0x04)
        sip_show = format_ip2s(self.sip, ip_version=0x04)

        return (sip_show, dip_show)

    def format_outmsg(self, title="\033[32m ARP Frame Header \033[0m", indent=4):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        fmt = "|%-6s|%-7s|%-6s|%-7s|%-2s|%-17s|%-15s|%-17s|%-15s|"
        head_out = prefix + fmt % ("HWType", "PtoType", "HWSize", "PtoSize", "OP", "S.MAC", "S.IP", "D.MAC", "D.IP")
        (smac, dmac) = self.format_macs()
        if self.ptotype == 0x0800:
            # IPv4 address
            (sip, dip) = self.format_ips()
        else:
            (sip, dip) = ("%08X" % self.sip, "%08X" % self.dip)
        data_out = prefix + fmt % ("%04X" % self.hwtype, 
                                   "%04X" % self.ptotype,
                                   str(self.hwsize),
                                   str(self.ptosize),
                                   str(self.opcode),
                                   smac, sip, dmac, dip)
        sep_num = 102

        if self.opcode == 1:
            opmsg = "ARP Request"
        elif self.opcode == 2:
            opmsg = "ARP Respone"
        elif self.opcode == 3:
            opmsg = "RARP Request"
        elif self.opcode == 4:
            opmsg = "RARP Response"
        else:
            opmsg = ""

        outmsg = (prefix + "|>>>  %s  | %s | %d-%s" % (title, opmsg, self.hwtype, ARP_HARDWARE_TYPES[self.hwtype])) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (head_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n" \
        + (data_out) + "\n" \
        + (prefix + "-"*sep_num) + "\n"

        return outmsg

    def format_output(self, stdout, title="\033[32m ARP Frame Header \033[0m", indent=4):
        """
        Output format message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))
        

class RARPFrame(ARPFrame):
    """
    RARP Frame same as ARP Frame.
    Ethernet Type: 0x8035 
    """
    pass


#------------------------------------------------------------------------
if __name__ == '__main__':
    pass
