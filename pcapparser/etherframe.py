#!/usr/bin/env python3

"""
Copyright (c) 2023 yixiangzhike
python-pcapparser is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2. 
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2 
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
See the Mulan PSL v2 for more details.

@Author: yixiangzhike <yixiangzhike007@163.com>
"""

from pcapparser.error import ParamsError
from pcapparser.constants import ETHER_TYPES,FRAME_IEEE802_SNAP_HDR_LEN,FRAME_IEEE802_SNAP, \
        FRAME_IEEE802_LLC,FRAME_IEEE802_LLC_HDR_LEN,FRAME_ETHERNETII,FRAME_ETHII_HDR_LEN

class EtherFrameHeader(object):
    """
    There are tow format for ether frame header.

    1.Ethernet II
    ---------------------------------------
    fields | D.MAC  | S.MAC  | Ether Type |
    ---------------------------------------
    bytes  | 6      | 6      |  2         |
    ---------------------------------------

    2.IEEE802.3
    --------------------------------------------------------------------------------
    fields | D.MAC  | S.MAC  | Length  |        LLC        |        SNAP           |
    --------------------------------------------------------------------------------
           | D.MAC  | S.MAC  | Length  |D.SAP|S.SAP|Control| Org Code | Ether Type |
    --------------------------------------------------------------------------------
    bytes  | 6      | 6      |  2      |  1  |  1  |   1   | 3        |  2         |
    --------------------------------------------------------------------------------

    Length : The length of ether frame data.
    DSAP:    Destination Service Access Point.
    SSAP:    Source Service Access Point.

    IF DSAP=0xAA SSAP=0xAA, the field SNAP exist, or else SNAP is None
       DSAP=0x42 SSAP=0x42, spanning tree protocol(STP)

    Ether Type <  0x05DC, use IEEE 802.3
    Ether Type >= 0x0600, use Ethernet II
    """
    def __init__(self, dmac=b'', smac=b'', etype=0x00, 
                length=0x00, dsap=0x00, ssap=0x00, 
                control=0x00, orgcode=b''):
        """
        @dmac    : Destionation MAC
        @smac    : Source MAC
        @etype   : Ether Type
        @length  : The length of frame data with fromat IEEE 802
        @dsap    : Destination Service Access Point
        @ssap    : Source Service Access Point
        @control :
        @orgcode : Org Code
        """
        self.dmac = dmac
        self.smac = smac
        self.etype = etype
        self.length = length
        self.dsap = dsap
        self.ssap = ssap
        self.control = control
        self.orgcode = orgcode

        """
        format  : 0-Ethernet II 
                  1-Ethernet IEEE802.3 Frame LLC
                  2-Ethernet IEEE802.3 Frame SNAP
        hdrlen  : 14 or 17 or 22
        """
        if self.etype < 0x05dc:
            if self.dsap == 0xaa and self.ssap == 0xaa:
                self.format = FRAME_IEEE802_SNAP
                self.hdrlen = FRAME_IEEE802_SNAP_HDR_LEN
            else:
                self.format = FRAME_IEEE802_LLC
                self.hdrlen = FRAME_IEEE802_LLC_HDR_LEN
        elif self.etype >= 0x0600:
            self.format = FRAME_ETHERNETII
            self.hdrlen = FRAME_ETHII_HDR_LEN
        else:
            self.format = ""
            self.hdrlen = 0

    @classmethod
    def get_header_length(cls, header_data, size=FRAME_ETHII_HDR_LEN):
        """
        Get header length from header_data
        @header_data
        @size            : unused
        """
        if not header_data:
            raise ParamsError("Param header_data can't be None")

        if not isinstance(header_data, bytes):
            raise ParamsError()

        if len(header_data) < FRAME_ETHII_HDR_LEN:
            raise ParamsError("Length of param header_data error")
        etype = int(header_data[12:14].hex(), 16)

        if etype >= 0x0600:
            return FRAME_ETHII_HDR_LEN

        if etype < 0x05dc:
            dsap = header_data[15]
            ssap = header_data[16]
            if dsap == 0xaa and ssap == 0xaa:
                return FRAME_IEEE802_SNAP_HDR_LEN
            else:
                return FRAME_IEEE802_LLC_HDR_LEN

        return 0

    @classmethod
    def create(cls, header_data, size=FRAME_ETHII_HDR_LEN):
        """
        Create EtherFrameHeader object
        @header_data  : the data of ether frame header
        @size         : the length of ehter frame header
        """
        if not header_data or size < FRAME_ETHII_HDR_LEN:
            raise ParamsError("Param header_data or size error")

        if not isinstance(header_data, bytes):
            raise ParamsError()

        header_length = cls.get_header_length(header_data)

        if header_length == FRAME_ETHII_HDR_LEN:
            frmhdr = cls(dmac=header_data[:6],
                         smac=header_data[6:12],
                         etype=int(header_data[12:14].hex(), 16))

        if header_length == FRAME_IEEE802_LLC_HDR_LEN:
            frmhdr = cls(dmac=header_data[:6],
                         smac=header_data[6:12],
                         length=int(header_data[12:14].hex(), 16),
                         dsap=header_data[14],
                         ssap=header_data[15],
                         control=header_data[16])

        if header_length == FRAME_IEEE802_SNAP_HDR_LEN:
            frmhdr = cls(dmac=header_data[:6],
                         smac=header_data[6:12],
                         length=int(header_data[12:14].hex(), 16),
                         dsap=header_data[14],
                         ssap=header_data[15],
                         control=header_data[16],
                         orgcode=header_data[17:20],
                         etype=int(header_data[20:22], 16))

        return frmhdr

    def format_macs(self):
        """
        Format mac address to be separated by ':'
        """
        mac_fmt = "%02X:%02X:%02X:%02X:%02X:%02X"
        dmac_show = mac_fmt % tuple(self.dmac)
        smac_show = mac_fmt % tuple(self.smac)

        return (smac_show, dmac_show)

    def format_outmsg(self, title="\033[32m Ethernet Frame Header \033[0m", indent=4):
        """
        Format output message
        @title
        @indent
        """
        prefix = "%*s" % (indent, " ")
        if self.format == FRAME_ETHERNETII:
            fmt = "| %-17s | %-17s | %-50s |"
            head_out = prefix + fmt % ("D.MAC", "S.MAC", "Ether Type")
            data_out = prefix + fmt % (self.format_macs()[1], 
                                       self.format_macs()[0],
                                       "%04X - %s" % (self.etype, ETHER_TYPES[self.etype]))
            sep_num = 95

        if self.format == FRAME_IEEE802_LLC:
            fmt = "| %-17s | %-17s | %-6s | %-5s | %-5s | %-5s |"
            head_out = prefix + fmt % ("D.MAC", "S.MAC", "Length", "D.SAP", "S.SAP", "CTRL")
            data_out = prefix + fmt % (self.format_macs()[1], 
                                       self.format_macs()[0],
                                       str(self.length),
                                       str(self.dsap),
                                       str(self.ssap),
                                       str(self.control))
            sep_num = 74

        if self.format == FRAME_IEEE802_SNAP:
            fmt = "| %-17s | %-17s | %-6s | %-5s | %-5s | %-5s | %-8s | %-50s |"
            head_out = prefix + fmt % ("D.MAC", "S.MAC", "Length", "D.SAP", "S.SAP", "CTRL", "Org Code", "Ether Type")
            data_out = prefix + fmt % (self.format_macs()[1], 
                                       self.format_macs()[0],
                                       str(self.length),
                                       str(self.dsap),
                                       str(self.ssap),
                                       str(self.control),
                                       self.orgcode.hex(),
                                       "%04X - %s" % (self.etype, ETHER_TYPES[self.etype]))
            sep_num = 140

        outmsg = (prefix + "|>>>   %s    |" % title) + "\n" \
                + (prefix + "-"*sep_num) + "\n" \
                + (head_out) + "\n" \
                + (prefix + "-"*sep_num) + "\n" \
                + (data_out) + "\n" \
                + (prefix + "-"*sep_num) + "\n"

        return outmsg 

    def format_output(self, stdout, title="\033[32m Ethernet Frame Header \033[0m", indent=4):
        """
        Output message
        @stdout
        @title
        @indent
        """
        stdout.write(self.format_outmsg(title=title, indent=indent))


class EtherFrame(object):
    """
    ether frame format:
    -------------------------------------
    | Frame Header | Frame Data | crc   |
    -------------------------------------
    |14/17/22bytes |    n       | 4bytes|
    -------------------------------------
    """
    def __init__(self, efrmhdr=None, frmdata=None, crc=None):
        """
        @efrmhdr : The object of EtherFrameHeader.
        @frmdata : Ether frame dataes.
        @crc     : Checksum for etherframe data
        """
        self.efrmhdr = efrmhdr
        self.frmdata = frmdata
        self.crc = crc

