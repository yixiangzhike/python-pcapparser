# python-pcapparser

#### Description
A python tool for parsing pcap file which generated by tcpdump.

#### Software Architecture
LINUX

#### Installation

1.  download source code from https://gitee.com/yixiangzhike/python-pcapparser
```
    git clone https://gitee.com/yixiangzhike/python-pcapparser.git
```
2.  install python-pcapparser to local standard python site-packages
```
    cd python-pcapparser
    python3 setup.py build install
```
    or
```
    python3 setup.py build install --single-version-externally-managed --root=/
```
3.  main script file pcap-parse will be installed into PATH

#### Instructions

1.  use command to get usage
```
    pcap-parse -h/--help
```
2.  parse all packets data default
```
    pcap-parse xx.pcap
```
3.  you can parse custom line packet data by option -n
```
    pcap-parse -n 12 xx.pcap
```
4.  you can give a output file to save the results
```
    pcap-parse -o outfile xx.pcap
```

#### Examples
```
$ pcap-parse tests/full.pcap -H
 -----------------------------------------------------------------------------------
 |   Pcap File Header   |  Link Type: 1-Ethernet, and Linux loopback devices 
 -----------------------------------------------------------------------------------
 | Magic    | Major | Minor | Thiszone | Sigfigs  | Snap Length | Link Type |      |
 -----------------------------------------------------------------------------------
 | D4C3B2A1 | 0002  | 0004  | 00000000 | 00000000 | 00040000    | 00000001  | HEX  |
 -----------------------------------------------------------------------------------
 | D4C3B2A1 | 2     | 4     | 0        | 0        | 262144      | 1         | DEC  |
 -----------------------------------------------------------------------------------

$ pcap-parse tests/full.pcap -n 2
 -----------------------------------------------------------------------------------
 |   Pcap File Header   |  Link Type: 1-Ethernet, and Linux loopback devices 
 -----------------------------------------------------------------------------------
 | Magic    | Major | Minor | Thiszone | Sigfigs  | Snap Length | Link Type |      |
 -----------------------------------------------------------------------------------
 | D4C3B2A1 | 0002  | 0004  | 00000000 | 00000000 | 00040000    | 00000001  | HEX  |
 -----------------------------------------------------------------------------------
 | D4C3B2A1 | 2     | 4     | 0        | 0        | 262144      | 1         | DEC  |
 -----------------------------------------------------------------------------------
 |>>>     Packet Header [1]     |
 --------------------------------------------------------------------
 | Timestamp(s)        | Timestamp(us) | Cap Len  | Length   |      |
 --------------------------------------------------------------------
 | 64D6DB5F            | 000266CE      | 0000002A | 0000002A | HEX  |
 --------------------------------------------------------------------
 | 2023-08-12 09:07:43 | 157390        | 42       | 42       | DEC  |
 --------------------------------------------------------------------
    |>>>    Ethernet Frame Header     |
    -------------------------------------------------------
    | D.MAC             | S.MAC             | Ether Type |
    -------------------------------------------------------
    | FF:FF:FF:FF:FF:FF | 28:73:8D:41:EE:D4 | 0806       |
    -------------------------------------------------------
        |>>>   ARP Frame Header   | ARP Request |
        ------------------------------------------------------------------------------------------------------
        |HWType|PtoType|HWSize|PtoSize|OP|S.MAC            |S.IP           |D.MAC            |D.IP           |
        ------------------------------------------------------------------------------------------------------
        |0001  |0800   |6     |4      |1 |28:73:8D:41:EE:D4|192.168.1.11   |FF:FF:FF:FF:FF:FF|192.168.1.31   |
        ------------------------------------------------------------------------------------------------------
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
